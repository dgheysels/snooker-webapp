clubkamp:
 - close season
    * current season: active=false
    * rangschikking van iedere speler opslaan --> update tnmt_player table: kolom met plaats
 - nieuw seizoen enkel als er geen active seizoenen zijn

spelers:
 - interclub info
 - statistieken + palmares
    * clubkampioenschap: 
         jaar - eindpositie - HB
         head-to-head
    * interclub
         jaar - reeks - kampioen/vice-kampioen