
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="dt" uri="/WEB-INF/tags/taglibs-datetime.tld"%>

<c:forEach var="notice" items="${recentNotices}">
	<div class="panel news">
		<p style="color: #AAAAAA; margin-bottom: 0px; margin-top: 0px;">
			<dt:format pattern="dd/MM/yyyy"><c:out value="${notice.date.time}"/></dt:format>
		</p>
		<h1><c:out value="${notice.title}" /></h1>
		<c:out value="${notice.contents}" escapeXml="false" />
		
<!--		<h1>Website "The New Crucible" online</h1>-->
<!--		Na enkele maanden van hard zwoegen en overuren kloppen, staat de eerste versie van de website online.<br>-->
<!--		De bedoeling van deze website is momenteel om:-->
<!--		<ul>-->
<!--			<li>onze club meer in de schijnwerpers te zetten,</li>-->
<!--			<li>communicatie te verzorgen naar alle leden: verslagen van bestuursvergaderingen, evenementen en andere nieuwsfeiten zullen op de website gepubliceerd worden,</li>-->
<!--			<li>informatie op te vragen van alle leden zoals contactgegevens, interclub, ...</li>-->
<!--			<li>resultaten te bekijken van het clubkampioenschap, Davy's Cup en eventueel andere kampioenschappen,</li>-->
<!--			<li>ook een meerwaarde te geven voor onze sponsors.</li>-->
<!--		</li>-->
	</div>
</c:forEach>


