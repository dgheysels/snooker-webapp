<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="dt" uri="/WEB-INF/tags/taglibs-datetime.tld"%>

<div class="panel">
	
	<table width="100%">
		<tr>			
			<c:forEach items="${tournament.singleMatchPeriods}" var="period" varStatus="loopStatus">
				<c:choose>
					<c:when test="${series == 'A'}">			
						<c:set var="matches" value="${period.matchesA}" scope="page" />			
					</c:when>
					<c:otherwise>		
						<c:set var="matches" value="${period.matchesB}" scope="page" />
					</c:otherwise>
				</c:choose>
				<td valign="top" align="center">
					<div align="center" style="border: 2px solid black">
						<!-- match table header -->
						<table width="100%" cellspacing="0">											
							<tr class="tableheader">
								<td colspan="3" style="border-bottom: 2px solid black">
									<h1>Best-of-7 match <c:out value="${period.name}" /></h1>
								</td>
							</tr>
				
							<tr class="bold" align="left">
								<td width="75%" style="border-bottom: 2px solid black"><dt:format pattern="dd/MM/yyyy"><c:out value="${period.fromDate.time}" /></dt:format> - <dt:format pattern="dd/MM/yyyy"><c:out value="${period.toDate.time}" /></dt:format></td>
								<td width="12%" style="border-left: 1px solid black; border-bottom: 2px solid black; border-right: 1px solid black">SC</td>
								<td width="13%" style="border-bottom: 2px solid black">HB</td>
							</tr>
						</table>
						<!--  match table results -->
						<c:forEach items="${matches}" var="singleMatch">						
							<br />
							<table width="100%" cellspacing="0" style="border-top: 1px solid black; border-bottom: 1px solid black">
								<tr align="left">									
									<td width="75%" style="border-bottom: 1px solid black"><c:out value="${singleMatch.playerA}" /></td>
									<td width="12%" style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black">
										<c:choose>
											<c:when test="${singleMatch.scoreA < 0}">
												<c:out value="FF" />
											</c:when>
											<c:otherwise>
												<c:out value="${singleMatch.scoreA}" />
											</c:otherwise>
										</c:choose>
									</td>
									<td width="13%" style="border-bottom: 1px solid black;">
										<c:set var="breakA" value="${singleMatch.highestBreakPlayerA.value}" />
											<c:if test="${breakA != 0}">
												<c:out value="${breakA}" />
											</c:if>
									</td>
								</tr>
								<tr align="left">
									<td width="75%"><c:out value="${singleMatch.playerB}" /></td>
									<td width="12%" style="border-left: 1px solid black; border-right: 1px solid black">
										<c:choose>
											<c:when test="${singleMatch.scoreB < 0}">
												<c:out value="FF" />
											</c:when>
											<c:otherwise>
												<c:out value="${singleMatch.scoreB}" />
											</c:otherwise>
										</c:choose>
									</td>
									<td width="13%">
										<c:set var="breakB" value="${singleMatch.highestBreakPlayerB.value}" />
										<c:if test="${breakB != 0}">
											<c:out value="${breakB}" />
										</c:if>
									</td>
								</tr>
							</table>															
						</c:forEach>
					</div>
				</td>
				
				<c:if test="${loopStatus.count % 3 == 0}">
					</tr>
					<tr>
				</c:if>
			</c:forEach>
		</tr>
	</table>
	
</div>

