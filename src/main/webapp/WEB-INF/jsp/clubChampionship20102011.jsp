<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="panel">	
	<table width="100%">		
		<tr class="title">
			<td colspan="2"><b>Informatie clubkampioenschap</b></td>		
		</tr>
		<tr>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/reglement.pdf" target="_blank">reglement</a></b></p>
			</td>
		</tr>
	</table>
</div>