<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="dt" uri="/WEB-INF/tags/taglibs-datetime.tld" %>

<div class="panel">
	<form:form method="POST" commandName="tournament">
	
		<table width="100%">
			<tr class="title">
				<td colspan="2">Algemeen</td>
			</tr>
			<tr>
				<td width="15%">Seizoen</td>
				<td><form:input path="season" /></td>
			</tr>
			<tr>
				<td>Naam</td>
				<td><form:input path="name" /></td>
			</tr>
		</table>
		<table width="100%">		
			<tr class="title">
				<td valign="top" colspan="6">Spelers</td>
			</tr>
			<tr>
				<td width="15%"></td>
				<c:forEach var="player" items="${players}" varStatus="loopStatus">									
					
					<td>
						<input type="checkbox" name="subscribers" value="${player.id}"/><c:out value="${player.name}" />
					</td>
					<td>
						<select name="${player.id}">
							<option value="A">A</option>
							<option value="B">B</option>
						</select>
					</td>
					
					<c:if test="${loopStatus.count % 2 == 0}">
						</tr>
						<tr>
							<td width="15%"></td>
					</c:if>
				</c:forEach>
			</tr>
		</table>
		
		<table width="100%">
			<tr class="title">
			 	<td width="15%" valign="top" colspan="5">Best-of-7 match</td>
			</tr>
			<tr>
				<td width="15%"></td>		 		
				<td>naam</td>
				<td>van</td>
				<td>tot</td>	
			</tr>
			<tr>
				<td width="15%"></td>
				<td><input type="text" name="smName" /></td>
				<td><input type="text" size="8" name="smFromDate" /></td>
				<td><input type="text" size="8" name="smToDate" /></td>															
				<td>
					<input type="submit" value="add" name="addPeriod" />
				</td>
			</tr>
			<c:forEach items="${tournament.singleMatchPeriods}" var="singleMatchPeriod">
				<tr>
					<td width="15%"></td>
					<td><c:out value="${singleMatchPeriod.name}" /></td>
					<td><dt:format pattern="dd/MM/yyyy"><c:out value="${singleMatchPeriod.fromDate.time}" /></dt:format> </td>
					<td><dt:format pattern="dd/MM/yyyy"><c:out value="${singleMatchPeriod.toDate.time}" /></dt:format></td>																											
				</tr>
			</c:forEach>
		</table>
		
		<table width="100%">
			<tr class="title">
			 	<td width="15%" valign="top" colspan="4">Handicap tornooi</td>
			</tr>
			<tr>
			 	<td width="15%">naam</td>
			 	<td><input type="text" size="10" name="htName" /></td>
			</tr>
			<tr>
				<td>van</td>
			 	<td><input type="text" size="8" name="htFromDate" /></td>
			 	<td>tot</td>
				<td><input type="text" size="8" name="htToDate" /></td>
			</tr>
			<tr>
			 	<td>pouleronde ?</td>
			 	<td><input type="checkbox" name="htWithPoules" value="1" checked /></td>
			</tr>
			<tr>
				<td>puntenverdeling</td>
				<td colspan="3"><input type="text" size="3" name="htPointsFrame" /> punten per gewonnen frame</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="htPointsLoser16F" /> punten voor verliezende 16e finalist</td>
			</tr>
			
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="htPointsLoser8F" /> punten voor verliezende achtste finalist</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="htPointsLoser4F" /> punten voor verliezende kwart finalist</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="htPointsLoser2F" /> punten voor verliezende halve finalist</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="htPointsLoserF" /> punten voor runner up</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="htPointsWinnerF" /> punten voor winnaar </td>
			</tr>
		</table>
		
		<table width="100%">
			<tr class="title">
			 	<td width="15%" valign="top" colspan="4">Eindtornooi</td>
			</tr>
			<tr>
			 	<td width="15%">naam</td>
			 	<td><input type="text" size="10" name="etName" /></td>
			</tr>
			<tr>
				<td>van</td>
			 	<td><input type="text" size="8" name="etFromDate" /></td>
			 	<td>tot</td>
				<td><input type="text" size="8" name="etToDate" /></td>
			</tr>
			<tr>
			 	<td>pouleronde ?</td>
			 	<td><input type="checkbox" name="etWithPoules" value="1" checked /></td>
			</tr>
			<tr>
				<td>puntenverdeling</td>
				<td colspan="3"><input type="text" size="3" name="etPointsFrame" /> punten per gewonnen frame</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="etPointsLoser16F" /> punten voor verliezende 16e finalist</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="etPointsLoser8F" /> punten voor verliezende achtste finalist</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="etPointsLoser4F" /> punten voor verliezende kwart finalist</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="etPointsLoser2F" /> punten voor verliezende halve finalist</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="etPointsLoserF" /> punten voor runner up</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3"><input type="text" size="3" name="etPointsWinnerF" /> punten voor winnaar </td>
			</tr>
		</table>

		<br />
		<hr />	
		<input type="submit" value="save" name="saveTournament" />
							
		
	</form:form>
</div>