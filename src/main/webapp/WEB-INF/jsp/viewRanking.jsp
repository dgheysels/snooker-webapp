<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="json" uri="/WEB-INF/tags/json.tld" %>
	
<c:set var="highestBreaksOverall" value="${ranking.highestBreaks}"></c:set>

<div class="panel">
	<p align="right">
		<c:url value="printRanking.htm" var="printRankingURL">
			<c:param name="tournamentId" value="${tournament.id}"></c:param>
		</c:url>
		<a href=<c:out value="${printRankingURL}" /> target="_blank">
			<img src="images/icons/printer.png" />
		</a>
	</p>
	<table cellspacing="0" width="100%">
		
		<tr class="bold">					
			<td style="border-bottom: 1px solid black" />
			<td style="border-bottom: 1px solid black" />
			<td style="border-bottom: 1px solid black" />
				<c:forEach var="tournamentEvent" items="${ranking.tournament.tournamentEvents}">
				<td align="left" style="border-bottom: 1px solid black"><c:out value="${tournamentEvent.name}" /></td>
			</c:forEach>

			<td align="left" style="border-bottom: 1px solid black">MW</td>

			<c:forEach var="tournamentType" items="${tournamentTypes}">
				<td align="left" style="border-bottom: 1px solid black"><c:out value="${tournamentType.breakname}" /></td>
			</c:forEach>
			
			<td align="left" style="border-bottom: 1px solid black">Ptn</td>
		</tr>
		<c:forEach var="rankingEntry" items="${ranking.entries}"  varStatus="loopStatus">
			<c:url value="disqualify.htm" var="disqualifyURL">
				<c:param name="tournamentId" value="${tournament.id}"></c:param>
				<c:param name="playerId" value="${rankingEntry.player.id}"></c:param>
			</c:url>
			<tr>
				<td><c:out value="${loopStatus.count}" /></td>
				<td><c:out value="${rankingEntry.player}" /></td>				
				<td style="border-right: 1px solid black"><c:out value="${rankingEntry.grade}" /></td>
				<c:forEach var="tournamentEvent" items="${tournament.tournamentEvents}">						
					<c:choose>
						<c:when test="${rankingEntry.disqualified}">
							<td style="color: #dd2424;">
								<c:out value="-" />
							</td>
						</c:when>
						<c:when test="${rankingEntry.rankingPoints[tournamentEvent] == -1}">
							<td style="color: #dd2424;">
								<c:out value="FF" />
							</td>
						</c:when>
						<c:when test="${rankingEntry.rankingPoints[tournamentEvent] < -1}">
							<td style="color: #dd2424;">
								<c:out value="${-rankingEntry.rankingPoints[tournamentEvent]}"/> (FF)
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<c:out value="${rankingEntry.rankingPoints[tournamentEvent]}" />
							</td>
						</c:otherwise>
					</c:choose>					
				</c:forEach>
				<td style="border-left: 1px solid black"><c:out value="${rankingEntry.singleMatchWins}" /></td>
				
				<c:forEach var="tournamentType" items="${tournamentTypes}">
					<td class=<c:if test="${rankingEntry.highestBreaks[tournamentType] == highestBreaksOverall[tournamentType]}">bold</c:if>>
						<c:if test="${rankingEntry.highestBreaks[tournamentType] != 0}">
							<c:out value="${rankingEntry.highestBreaks[tournamentType]}" />
						</c:if>
					</td>

				</c:forEach>
					<c:choose>
						<c:when test="${rankingEntry.disqualified}">
							<td style="border-left: 1px solid black; color: #dd2424;">
								<c:out value="DSQ" />	
							</td>
						</c:when>
						<c:otherwise>
							<td style="border-left: 1px solid black">
								<b><c:out value="${rankingEntry.total}" /></b>
							</td>
						</c:otherwise>
					</c:choose>
				<td>
					<security:authorize ifAnyGranted="ROLE_ADMIN, ROLE_SUPERUSER">
						<c:if test="${!rankingEntry.disqualified}">					
							<a href=<c:out value="${disqualifyURL}" />><img src="images/icons/remove.png" width="10px" /></a>					
						</c:if>
					</security:authorize>
				</td>
			</tr>					
		</c:forEach>				
	</table>
</div>