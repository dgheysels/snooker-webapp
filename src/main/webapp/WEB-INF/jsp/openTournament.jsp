<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="panel">	
	<table width="100%">		
		<tr class="title">
			<td colspan="2"><b>Informatie clubkampioenschap</b></td>		
		</tr>
		<tr>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20092010/reglement.pdf" target="_blank">reglement</a></b></p>
			</td>
		</tr>
		<tr>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20092010/reeksA.pdf" target="_blank">gedetailleerde overzicht wedstrijden reeks A</a></b></p>
			</td>
		</tr>
		<tr>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20092010/reeksB.pdf" target="_blank">gedetailleerde overzicht wedstrijden reeks B</a></b></p>
			</td>
		</tr>
		<tr>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20092010/eindronde.pdf" target="_blank">tabel & planning eindronde clubkampioenschap</a></b></p>
			</td>
		</tr>
	</table>
	
</div>

