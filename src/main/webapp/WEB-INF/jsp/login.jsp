<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

	
<div class="panel">
	<form method="post" action="<c:url value='j_spring_security_check'/>">
		<table>
			<tr>
				<td>Gebruikersnaam</td>
				<td><input type="text" name="j_username" size="15" /></td>					
			</tr>
			<tr>
				<td>Paswoord</td>
				<td><input type="password" name="j_password" size="15" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<c:if test="${not empty param.error}"><span class="error">Onjuiste gebruikersnaam of paswoord</span></c:if>
				</td>					
				<td><input type="submit" value="login" /></td>
			</tr>
		</table>
	</form>
</div>
	
