<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<ul>
	<li>
		<c:url var="calendarURL" value="http://www.thenewcrucible.be/docs/ck/20112012/kalender.pdf" />
		<h2><a href=<c:out value="${calendarURL}"/>><img src="images/icons/calendar.png" width="20px" height="20px" /> kalender</a></h2>			
	</li>
</ul>
