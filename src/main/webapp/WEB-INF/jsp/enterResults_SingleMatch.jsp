<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="dt" uri="/WEB-INF/tags/taglibs-datetime.tld" %>


	<div class="panel">
	
		
		<c:url value="/enterResults.htm" var="submitSingleMatchURL">
			<c:param name="tournamentId" value="${tournament.id}" />
			<c:param name="periodId" value="${period.id}" />
		</c:url>
		
		<form method="POST" action=<c:out value="${submitSingleMatchURL}"/>>
			<b>
			Ingave resultaten voor <c:out value="${period.name}" /><br />
			<dt:format pattern="dd/MM/yyyy"><c:out value="${period.fromDate.time}"/></dt:format> - <dt:format pattern="dd/MM/yyyy"><c:out value="${period.toDate.time}"/></dt:format>
			</b>
			<br /><br />
			<table>
				<tr>
					<td>
						<b>datum</b>
					</td>
					<td>
						<input type="text" name="date" />  
					</td>
				</tr>
				<tr>									
					<td>
						<b>resultaat</b>
					</td>
					<td>
						<select name="playerA">
							<c:forEach items="${tournament.subscribers}" var="subscription">
								<option value="${subscription.player.id}"><c:out value="${subscription.player.name}" /></option>
							</c:forEach>	
						</select>
					</td>
					<td>
						<input type="text" name="scoreA"  size="2" />						
					</td>
					<td>-</td>
					<td>
						<input type="text" name="scoreB"  size="2" />
					</td>
					<td>
						<select name="playerB">
							<c:forEach items="${tournament.subscribers}" var="subscription">
								<option value="${subscription.player.id}"><c:out value="${subscription.player.name}" /></option>
							</c:forEach>	
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<b>breaks</b>
					</td>
					<td colspan="2" align="right">
						<input type="text" name="breaksPlayerA" />
					</td>
					<td />
					<td colspan="2" align="left">
						<input type="text" name="breaksPlayerB" />
					</td>
				</tr>
				<tr>
					<td />
					<td />
					<td />
					<td />
					<td />	
					<td />															
					<td>
						<input type="submit" value="toevoegen" name="addSingleMatch" />
					</td>														
				</tr>			                          
			</table>																										
		 		 
			 <br />
			 <b>
			 Overzicht v/d matches
			 </b>
			 <br /><br />
			 Reeks A<br />
			 <table>
				 <c:forEach items="${period.matchesA}" var="singleMatchA">
					 	<tr>
					 		<td><dt:format pattern="dd/MM/yyyy"><c:out value="${singleMatchA.date.time}"/></dt:format></td>
					 		<td><c:out value="${singleMatchA.playerA}" /></td>
					 		<td><c:out value="${singleMatchA.scoreA}" /></td>
					 		<td>-</td>
					 		<td><c:out value="${singleMatchA.scoreB}" /></td>
					 		<td><c:out value="${singleMatchA.playerB}" /></td>			 		
					 	</tr>
						
				 </c:forEach>
			 </table>
			 <br /><br />
			 Reeks B<br />
			 <table>
				 <c:forEach items="${period.matchesB}" var="singleMatchB">

					 	<tr>
					 		<td><dt:format pattern="dd/MM/yyyy"><c:out value="${singleMatchB.date.time}"/></dt:format></td>
					 		<td><c:out value="${singleMatchB.playerA}" /></td>
					 		<td><c:out value="${singleMatchB.scoreA}" /></td>
					 		<td>-</td>
					 		<td><c:out value="${singleMatchB.scoreB}" /></td>
					 		<td><c:out value="${singleMatchB.playerB}" /></td>			 		
					 	</tr>
						
				 </c:forEach>
			 </table>
			 <br />
			 <hr />
			 <input type="submit" value="Opslaan" name="savePeriod" />
			 <input type="submit" value="Opslaan & beŽindigen" name="finishPeriod" />
			 
		 </form>          					
	</div>
