<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="panel">
	<form:form method="POST" commandName="meeting" enctype="multipart/form-data"> 
		<table width="100%">
			<tr class="tableheader">
				<td colspan="2"><b>Algemene informatie</b></td>
			</tr>
			<tr>
				<td width="20%">Datum</td>
				<td><form:input path="date" maxlength="12" size="12" /></td>
			</tr>
			<tr>
				<td>Titel</td>
				<td><form:input path="name" size="25"  /></td>
			</tr>				
			<tr>
				<td>Verslag</td>
				<td>
					<input type="file" name="report" />
				</td>
			</tr>	
		</table>

		<hr />			
		<input type="submit" value="save" name="saveMeeting" />		

	</form:form>
</div>
