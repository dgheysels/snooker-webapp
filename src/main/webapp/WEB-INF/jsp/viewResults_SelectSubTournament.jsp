<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
	
<script>
	function selectRadio(radio) {
		radio.checked = true;
	}
</script>
	
<div class="panel">

	<c:url value="/viewResults.htm" var="selectRoundURL">
		<c:param name="tournamentId" value="${tournament.id}" />
	</c:url>
	<form method="POST" action=<c:out value="${selectRoundURL}"/>>
		<table width="100%">
			<tr>
				<td width="20%">
					<input type="radio" id="round" name="round_or_match" value="round" <c:if test="${param.round_or_match == 'round'}">checked</c:if>>		
					Sub-tornooi
				</td>
				<td width="20%">
					<select name="subTournamentId" onclick="selectRadio(round);">
						<c:forEach items="${tournament.subTournaments}" var="subTournament">
							<option value="${subTournament.id}" <c:if test="${param.subTournamentId == subTournament.id}">selected="selected"</c:if>>   
								<c:out value="${subTournament.name}"/>												
							</option>
						</c:forEach>						
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<input type="radio" id="singleMatch" name="round_or_match" value="match" <c:if test="${param.round_or_match == 'match'}">checked</c:if>>
					Best-of-7 match
				</td>
				<td>
					<select name="series" onclick="selectRadio(singleMatch);">
						<option value="A" <c:if test="${param.series == 'A'}">selected="selected"</c:if>>A reeks</option>
						<option value="B" <c:if test="${param.series == 'B'}">selected="selected"</c:if>>B reeks</option>
					</select>
				</td>
			</tr>
			<tr>
				<td />
				<td />
				<td>
					<input type="submit" value="selecteer" name="selectRoundViewResults" />
				</td>
			</tr>
		</table>		
	</form>
</div>
<hr />
		
	