<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<ul>
	<li>
		<c:url var="viewRankingURL" value="viewRanking.htm">
			<c:param name="tournamentId" value="${tournament.id}" />
		</c:url>
		<c:choose>
			<c:when test="${param.menu == 0}">
				<h2><img src="images/icons/rankingtable.png" width="20px" height="20px" /> klassement</h2>
			</c:when>
			<c:otherwise>
				<h2><a href=<c:out value="${viewRankingURL}"/>><img src="images/icons/rankingtable.png" width="20px" height="20px" /> klassement</a></h2>
			</c:otherwise>
		</c:choose>	
	</li>
	<li>
		<c:url var="viewResultsURL" value="viewResults.htm">
			<c:param name="tournamentId" value="${tournament.id}" />
		</c:url>
		<c:choose>
			<c:when test="${param.menu == 1}">
				<h2><img src="images/icons/resultsview.png" width="20px" height="20px" /> resultaten</h2>
			</c:when>
			<c:otherwise>
				<h2><a href=<c:out value="${viewResultsURL}"/>><img src="images/icons/resultsview.png" width="20px" height="20px" /> resultaten</a></h2>
			</c:otherwise>
		</c:choose>
		
	</li>
	<security:authorize ifAnyGranted="ROLE_SUPERUSER">
		<li>
			<c:url var="enterResultsURL" value="enterResults.htm">
				<c:param name="tournamentId" value="${tournament.id}" />
			</c:url>					
			<c:choose>
				<c:when test="${param.menu == 2}">
					<h2><img src="images/icons/resultsinput.png" width="20px" height="20px" /> ingave resultaten</h2>
				</c:when>
				<c:otherwise>
					<h2><a href=<c:out value="${enterResultsURL}"/>><img src="images/icons/resultsinput.png" width="20px" height="20px" /> ingave resultaten</a></h2>	
				</c:otherwise>
			</c:choose>						
		</li>
		<li>
			<c:url var="closeTournamentURL" value="closeTournament.htm">
				<c:param name="tournamentId" value="${tournament.id}" />
			</c:url>					
			<c:choose>
				<c:when test="${param.menu == 3}">
					<h2><img src="images/icons/ckclose.png" width="20px" height="20px" /> seizoen afsluiten</h2>
				</c:when>
				<c:otherwise>
					<h2><a href=<c:out value="${closeTournamentURL}"/>><img src="images/icons/ckclose.png" width="20px" height="20px" /> seizoen afsluiten</a></h2>	
				</c:otherwise>
			</c:choose>						
		</li>
	</security:authorize>
</ul>
