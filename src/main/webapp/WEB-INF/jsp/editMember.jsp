<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="panel">

	<form:form method="POST" commandName="player" enctype="multipart/form-data"> 
		
		<table width="100%">
			<tr>
				<c:url value="getImage.htm" var="imageURL">
					<c:param name="playerId" value="${player.id}" />
				</c:url>
				<td width="20%"><img src=<c:out value="${imageURL}" /> height="128" /></td>
				<td align="left"><h1><c:out value="${player.name}" /></h1></td>					
			</tr>
			<tr>
				<td>Wijzig foto</td>
				<td><input type="file" name="photo" /></td>
			</tr>
			<tr></tr>
			
			
		</table>
		
		<table width="100%">				
			<tr class="title">
				<td colspan="2"><b>Contactgegevens</b></td>			
			</tr>	
			<tr>
				<td width="20%" valign="top">Adres</td>
				<td><form:input path="address.street" maxlength="25" size="25" /> <form:input path="address.number" maxlength="4" size="4"/><br>
				    <form:input path="address.postcode" maxlength="4" size="4" /> <form:input path="address.city" maxlength="20" size="20"/>
				</td>
			</tr>
			<tr>
				<td>Telnr</td>
				<td><form:input path="telnr" maxlength="12" size="12"/></td>
			</tr>
			<tr>
				<td>E-mail</td>
				<td><form:input path="email" maxlength="40" size="40"/></td>
			</tr>
		</table>
		
		<table width="100%">
			<tr class="title">
				<td colspan="2"><b>Interclub</b></td>
			</tr>
			<tr>
				<td width="20%">Interclub-team</td>
				<td>
					<form:select path="interclubTeam">
						<form:option value="-1" label="" />
						<form:options items="${interclubTeams}" itemLabel="name"  />
					</form:select>
				</td>
			</tr>
			<tr>
				<td>Rang binnen team</td>
				<td>
					<form:select path="interclubRank">
						<form:option value="-1" label="" />
							<form:option value="0" label="captain" />
							<form:option value="1" label="2nd player" />
							<form:option value="2" label="3rd player" />						
<!--						<form:options items="${interclubRanks}"  itemValue="idx" itemLabel="description" />								-->
					</form:select>										
				</td>
			</tr>			
		</table>
		
		<table width="100%">
		<tr class="title">
				<td colspan="2"><b>Diverse Informatie</b></td>			
			</tr>
			<tr>
				<td width="20%">Bestuurslid</td>
				<td>
					<form:checkbox path="management" value="false" />
					<form:input  path="managementPosition" maxlength="25" size="25" />
				</td>
			</tr>
			<security:authorize ifAnyGranted="ROLE_SUPERUSER">
				<tr>
					<td>role</td>
					<td><form:select path="role" items="${roles}" itemLabel="name" /></td>
				</tr>
			</security:authorize>
		</table>
		
		<table width="100%">
			<tr class="title">
				<td colspan="2"><b>Wachtwoord</b></td>
			</tr>
			<tr>
				<td width="20%">Nieuw wachtwoord</td>
				<td>
					<input type="checkbox" name="regeneratePwd" value="true" />
				</td>				
			</tr>	
		</table>
		
<!--			<tr>				-->
<!--				<td>snookerclub</td>-->
<!--				<td><form:select path="snookerclub" items="${snookerclubs}" itemLabel="name" /></td>-->
<!--			</tr>-->
			
			
			
			
			
		<hr />			
		
		<input type="submit" value="save" name="savePlayer" />		
	
	</form:form>
</div>

