<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="dt" uri="/WEB-INF/tags/taglibs-datetime.tld"%>

<div class="panel">
	<table width="100%">						
		<c:forEach var="member" items="${management}" varStatus="s">
			<c:url value="getImage.htm" var="imageURL">
				<c:param name="playerId" value="${member.id}" />
			</c:url>
			<c:url value="viewMember.htm" var="viewURL">
				<c:param name="playerId" value="${member.id}" />
			</c:url>
			<tr>				
				<td width="48px" align="center"><img src=<c:out value="${imageURL}"/> width="32" height="32" /></td>
				<td><a href=<c:out value="${viewURL}"/>><c:out value="${member.name}"/></a></td>
				<td><c:out value="${member.managementPosition}"/></td>								
			</tr>
			
		</c:forEach>						
	</table>
</div>	

<security:authorize ifAnyGranted="ROLE_PLAYER, ROLE_ADMIN, ROLE_SUPERUSER">
	
	<hr />
	<div class="panel">
		<table width="100%" cellspacing="0">
			<tr class="title">
				<td width="15%"><b>Date</b></td>
				<td><b>Description</b></td>
				<td width="32px" />
			</tr>
									
			<c:forEach items="${meetings}" var="meeting">
				<c:url value="getReport.htm" var="downloadURL">
					<c:param name="meetingId" value="${meeting.id}" />
				</c:url>
				<tr>
					<td><dt:format pattern="dd/MM/yyyy"><c:out value="${meeting.date.time}"/></dt:format></td>
					<td><c:out value="${meeting.name}"/></td>
					<td><a href=<c:out value="${downloadURL}"/> target="_blank"><img src="images/icons/download.png" /></a></td>
					<td />											
				</tr>	
			</c:forEach>
	
			<security:authorize ifAnyGranted="ROLE_ADMIN, ROLE_SUPERUSER">
				<c:url value="newMeeting.htm" var="newMeetingURL"></c:url>
				<tr />			
				<tr>
					<td />
					<td />
					<td colspan="3"><a href=<c:out value="${newMeetingURL}" />><img src="images/icons/add.png" /></a></td>
					<td />
				</tr>
			</security:authorize>
			
		</table>
	</div>
</security:authorize>

