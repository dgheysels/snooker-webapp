<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<div class="panel">
	<p>	
	Sinds 2006 organiseert Davy Den Blauwen ieder jaar de Davy's Cup.  Dit is een uniek invitatietornooi in Belgi� die plaats vindt in The New Crucible.<br>
	Inmiddels is dit tornooi aan zijn 4de editie toe en start in november/december en loopt tot eind juni.  De prijzenpot is al opgelopen tot een aardige <b>5000 euro</b>.
	</p>
	<p>
	Hetgeen wat de Davy's Cup zo uniek maakt zijn de langere wedstrijden.  Er wordt steeds naar een <b>best-of-19</b> gespeeld, en dit over twee sessies die op een verschillende dag plaatsvinden.<br>
	Na de bikkelharde gevechten aan de tafel kan er nog gezellig nagepraat worden aan de toog.<br>
	Na de finale wordt door de uitbater van The New Crucible een <b>gratis</b> warm gerecht voorgeschoteld.
	</p>
	<p>
	Meer informatie omtrent inschrijving e.d. kan je <a href="http://www.thenewcrucible.be/docs/davyscup/uitnodiging.pdf" target="_blank"><u><b>hier</b></u></a> terugvinden.
	</p>
</div>
	
