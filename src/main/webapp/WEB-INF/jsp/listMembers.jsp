<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>




<div class="panel">
	<table width="100%">
	
		<tr>			
			<c:forEach var="player" items="${players}" varStatus="loopStatus">
				<c:url value="getImage.htm" var="imageURL">
					<c:param name="playerId" value="${player.id}" />
				</c:url>
				<c:url value="/viewMember.htm" var="viewURL">
					<c:param name="playerId" value="${player.id}" />
				</c:url>	
				
				<security:authorize ifAnyGranted="ROLE_PLAYER, ROLE_ADMIN, ROLE_SUPERUSER">
					<c:url value="/editMember.htm" var="editURL">
						<c:param name="playerId" value="${player.id}" />
					</c:url>
					<c:url value="/deleteMember.htm" var="deleteURL">
						<c:param name="playerId" value="${player.id}" />
					</c:url>
				</security:authorize>
				
				
				
				<td width="48px" align="center"><img src=<c:out value="${imageURL}"/> height="32" /></td>
				<td><c:out value="${player.name}"/></td>
					
				<td width="16px"><a href=<c:out value="${viewURL}"/>><img src="images/icons/view.png" width="12px" height="12px" /></a></td>
				<security:authorize ifAnyGranted="ROLE_ADMIN, ROLE_SUPERUSER">
					<td width="16px"><a href=<c:out value="${editURL}"/>><img src="images/icons/edit.png" width="12px" height="12px" /></a></td>
					<td width="16px"><a href=<c:out value="${deleteURL}"/>><img src="images/icons/remove.png" width="12px" height="12px" /></a></td>														
				</security:authorize>

				<c:if test="${loopStatus.count % 2 == 0}">
					</tr>
					<tr>
				</c:if>
				
			</c:forEach>
		</tr>	
							
	</table>
</div>
