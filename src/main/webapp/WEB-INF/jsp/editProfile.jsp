<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script src="js/nicEdit.js"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>


<div class="panel">

	<form:form method="POST" commandName="user" enctype="multipart/form-data"> 
		<c:url value="getImage.htm" var="imageURL">
			<c:param name="playerId" value="${user.id}" />
		</c:url>
		<table width="100%">
			<tr>
				<td width="20%"><img src=<c:out value="${imageURL}"/> height="128" /></td>
				<td align="left"><h1><c:out value="${user.name}" /></h1></td>					
			</tr>
			<tr></tr>
			<tr class="title">
				<td colspan="2"><p>Contactgegevens</p></td>			
			</tr>			
			<tr>
				<td valign="top">Adres</td>
				<td><form:input path="address.street" maxlength="25" size="25" /> <form:input path="address.number" maxlength="4" size="4"/><br>
				    <form:input path="address.postcode" maxlength="4" size="4" /> <form:input path="address.city" maxlength="20" size="20"/>
				</td>
			</tr>
			<tr>
				<td>Telnr</td>
				<td><form:input path="telnr" maxlength="12" size="12"/></td>
			</tr>
			<tr>
				<td>E-mail</td>
				<td><form:input path="email" maxlength="40" size="40"/></td>
			</tr>

			<tr class="title">
				<td colspan="2"><p>Wachtwoord wijzigen</p></td>
			<tr>
			<tr>
				<td>Huidig wachtwoord</td>
				<td><input type="password" name="currentPwd" maxlength="15" size="15" /></td>
			</tr>
			<tr>
				<td>Nieuw wachtwoord</td>
				<td><input type="password" name="newPwd" maxlength="15" size="15" /></td>
			</tr>
			<tr>
				<td>Bevestig nieuw wachtwoord</td>
				<td><input type="password" name="newPwd2" maxlength="15" size="15" /></td>
			</tr>
			
			<tr class="title">
				<td colspan="2"><p>Over jezelf</p></td>
			</tr>
			<tr>
				<td>Verslaafd aan snooker sinds</td>
				<td><form:input path="yearsPlayingSnooker" maxlength="4" size="4"/></td>
			</tr>
			<tr>
				<td>Merk en type van je keu</td>
				<td><form:input path="cueType" maxlength="45" size="45" /></td>
			</tr>
			<tr>
				<td>Favorite internationale speler</td>
				<td><form:input path="favoriteInternationalPlayer" maxlength="50" size="50"/></td>
			</tr>
			<tr>
				<td>Favorite belgische speler</td>
				<td><form:input path="favoriteBelgianPlayer" maxlength="50" size="50" /></td>
			</tr>
			<tr>
				<td>Hoogste break tijdens training</td>
				<td><form:input path="hbTraining" maxlength="3" size="4" /></td>
			</tr>
			<tr>
				<td>Hoogste break tijdens wedstrijd</td>
				<td><form:input path="hbMatch" maxlength="3" size="4" /></td>
			</tr>
			<tr>
				<td valign="top">Opmerkelijke gebeurtenis in jouw snooker carri�re</td>
				<td><form:textarea path="story" rows="10" cssStyle="width: 100%"  /></td>
			</tr>
		</table>
		<input type="submit" value="save" name="saveProfile" />		
	</form:form>
</div>