<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>



<div class="panel">			
	<b>Belgisch</b>
	<p>
		<a href=<c:url value="http://www.bbsa.be" /> target="_blank">Belgium Billiards &amp; Snooker Association (BBSA)</a>						
	</p>
	<p>
		<a href=<c:url value="http://www.vsf-oostvlaanderen.be" /> target="_blank">VSF Oostvlaanderen</a>
	</p>
	<p>
		<a href=<c:url value="http://www.snookerforum.be" /> target="_blank">Snookerforum</a>
	</p>
	<br />
	<b>Internationaal</b>
	<p>
		<a href=<c:url value="http://www.global-snooker.com" /> target="_blank">Global Snooker</a>
	</p>
	<p>
		<a href=<c:url value="http://www.thesnookerforum.com" /> target="_blank">The Snooker Forum</a>
	</p>
	<br />
	<b>practice makes perfect ... </b>
	<p>
		<a href=<c:url value="http://fergalobrien.ie/practice.html" /> target="_blank">Fergal O'Briens practice routines</a>
	</p>
	<p>
		<a href=<c:url value="http://www.dunns-cues.com/pics/Junior.pdf" /> target="_blank">The PJ Nolan Snooker Coaching Guide - Juniors</a>
		<br />
		<a href=<c:url value="http://www.dunns-cues.com/pics/Senior.pdf" /> target=_blank">The PJ Nolan Snooker Coaching Guide - Seniors</a>
	</p>
</div>		
	
