

/* Script to generate a knockout-table.
 (c) Dimitri Gheysels 2009
*/

function generate(height, position, data, parentContainer)
{
	var nodeWidth = Math.floor((parentContainer.getWidth()-20) * 0.2);
	var restWidth = Math.floor((parentContainer.getWidth()-20) - nodeWidth);
		
	var normalizedData = normalize(data);
	updateContainer(height, position, parentContainer);
	generateTableHeaders(height, parentContainer, nodeWidth);	
	generateTable(0, height, position, normalizedData, parentContainer, nodeWidth, restWidth);
}

/*
 * set height of the knockout-container
 * height = #player/2 * 200
 * 
 * width = 100%
 */
function updateContainer(height, position, parentContainer)
{
	parentContainer.setStyle({"height":Math.pow(2, height) * 70 + "px"});
}

function generateTableHeaders(height, parentContainer, nodeWidth)
{
	for (var i=0; i<=height; i++)
	{		
		var DIV_header = new Element("div", {"id":"header_" + i});
		var H3 = new Element("h3");		
	
		DIV_header.addClassName("knockoutTableHeader");		
		DIV_header.setStyle({"float":"right", "width":nodeWidth + "px"});
		
		H3.insert((i==0) ? "finale" : "1/" + Math.pow(2, i) + " finale");
		DIV_header.insert(H3);
		parentContainer.insert(DIV_header);
	}
	
	var DIV_cleaner = new Element("div");
	DIV_cleaner.addClassName("cleaner");
	parentContainer.insert(DIV_cleaner);	
}


/*
height:  0           	1           	2			
											
	LEAF (100)----								
			      |---NODE (10)---					
	LEAF (101)----			     |					
		   				         |------NODE (1)				
	LEAF (110)----			     |					
			      |---NODE (11)---					
	LEAF (111)----								
	
INPUT:
 - level: current level in tree (level <= height)
 - height: height of tree
 - position: position of node/leaf in the table as a binary value. Cfr. picture above
 - data: match data
 - parentContainer: DIV element to put subtrees in
*/
function generateTable(level, height, position, data, parentContainer, nodeWidth, restWidth)
{	
	if (level == height) 
	{
		// at bottom of the tree 
		generateNode(position, data, parentContainer, nodeWidth + restWidth);
	}
	else 
	{
		generateLeft(level+1, height, position + '0', data, parentContainer, nodeWidth, restWidth);
		generateNode(position, data, parentContainer, nodeWidth);
		generateRight(level+1, height, position + '1', data, parentContainer, nodeWidth, restWidth);
	}
}

function generateLeft(level, height, position, data, parentContainer, nodeWidth, restWidth)
{
	//container for left-tree 
	var DIV_left = new Element("div", {"id":"left_" + level});
	DIV_left.addClassName("left");
	DIV_left.setStyle({"float":"left", "width":restWidth + "px"});

	//create left-table into this new container 
	generateTable(level, height, position, data, DIV_left, nodeWidth, restWidth - nodeWidth);
	
	//place the container in its parent-container 
	parentContainer.insert(DIV_left);		
}


function generateRight(level, height, position, data, parentContainer, nodeWidth, restWidth)
{
	// container for left-tree 
	var DIV_right = new Element("div", {"id":"right_" + level});
	DIV_right.addClassName("right");
	DIV_right.setStyle({"float":"left", "width":restWidth + "px"});
				
	//create left-table into this new container
	generateTable(level, height, position, data, DIV_right, nodeWidth, restWidth - nodeWidth);
	
	// place the container in its parent-container 
	parentContainer.insert(DIV_right);		
}

function generateNode(position, data, parentContainer, nodeWidth)
{	
	var DIV_node = new Element("div", {"id":"node_" + position});
	DIV_node.addClassName("node");
	DIV_node.setStyle({"float":"right", "width":nodeWidth + "px"});
		
	// retrieve data based on position in table
	var namePlayerA = getMatchData(data, 'playerA', position);
	var namePlayerB = getMatchData(data, 'playerB', position);	
	var scorePlayerA = getMatchData(data, 'scoreA', position);
	var scorePlayerB = getMatchData(data, 'scoreB', position);
		
	/* put table into node to be able to align text vertically 
	 * 
	 * <TABLE>
	 * 	<TR>
	 * 		<TD>playerAname</TD><TD>playerAscore</TD>
	 *  </TR>
	 *  <TR>
	 *  	<TD>playerBname</TD><TD>playerBscore</TD>
	 *  </TR>
	 * </TABLE>
	 * */

	var TABLE = new Element("table");
	TABLE.setStyle({"width":"100%", "height":"100%"});
			
	var TR_playerA = new Element("tr");
	var TD_playerAName = new Element("td", {"valign":"bottom", "align":"right"});
	var TD_playerAScore = new Element("td", {"valign":"bottom", "align":"right"});
	var TR_playerB = new Element("tr");
	var TD_playerBName = new Element("td", {"valign":"top", "align":"right"});
	var TD_playerBScore = new Element("td", {"valign":"top", "align":"right"});
		
	if (scorePlayerA > scorePlayerB) {
		TD_playerAName.setStyle({"fontWeight":"bold"});
		TD_playerAScore.setStyle({"fontWeight":"bold"});
	} else {
		TD_playerBName.setStyle({"fontWeight":"bold"});
		TD_playerBScore.setStyle({"fontWeight":"bold"});		
	}
	
	namePlayerA = (namePlayerA == "") ? "-" : namePlayerA;
	scorePlayerA = (scorePlayerA == -1) ? "FF" : scorePlayerA;
	namePlayerB = (namePlayerB == "") ? "-" : namePlayerB;
	scorePlayerB = (scorePlayerB == -1) ? "FF" : scorePlayerB;
		
	TD_playerAName.insert(namePlayerA);
	TD_playerAScore.insert(scorePlayerA);
	TD_playerBName.insert(namePlayerB);
	TD_playerBScore.insert(scorePlayerB);
		
	TR_playerA.insert(TD_playerAName);
	TR_playerA.insert(TD_playerAScore);
	TR_playerB.insert(TD_playerBName);
	TR_playerB.insert(TD_playerBScore);
	
	TABLE.insert(TR_playerA);
	TABLE.insert(TR_playerB);
		
	DIV_node.insert(TABLE);
	
	parentContainer.insert(DIV_node);
}

function binaryToDecimal(binary) 
{	
	var decimal = 0;
	
	for (i=0; i<binary.length; i++)
	{	
		if (binary.charAt(binary.length-i-1) == '1')
		{			
			decimal = decimal + Math.pow(2, i);						
		}
	}
	return decimal;
}


function getMatchData(data, property, position) 
{
	var pos = binaryToDecimal(position);
	if (data[pos] != null) 
	{
		return data[pos].knockoutMatch[property];
	} else {
		return "";
	}
}

/*
 * put all data in  array with index 'position'
 */
function normalize(data)
{
	var normalizedData = new Array();
	for (var i=0; i<data.knockoutMatches.length; i++) {
		var temp = data.knockoutMatches[i];
		normalizedData[temp.position] = temp;
	}
	
	return normalizedData;

}

