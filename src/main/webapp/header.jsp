<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<p align="right" style="margin: 0px">
	<security:authorize ifAnyGranted="ROLE_PLAYER, ROLE_ADMIN, ROLE_SUPERUSER">
		<c:url value="/editProfile.htm" var="editProfileURL" />					
		Welkom <a href="<c:out value="${editProfileURL}"/>"><security:authentication property="name" /></a> | <a href="<c:url value='j_spring_security_logout'/>">logout</a>
	</security:authorize>
	<security:authorize ifNotGranted="ROLE_PLAYER, ROLE_ADMIN, ROLE_SUPERUSER">
		<a href="<c:url value="/login.htm" />">login</a>
	</security:authorize>			
</p>