package domain;

import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import domain.visitor.AbstractRankingVisitor;
import domain.visitor.UpdateRankingVisitor;


public class Tournament  {

	private int id;
	private String name = "";
	private String season = "";
	private boolean active;
	private Set<TournamentPlayer> subscribers = new TreeSet<TournamentPlayer>();
	private Set<SubTournament> subTournaments = new TreeSet<SubTournament>();
	private Set<SingleMatchPeriod> singleMatchPeriods = new TreeSet<SingleMatchPeriod>();
	
	private transient TournamentRanking ranking = new TournamentRanking(this);

	public Tournament() {
		setActive(true);		
	}

	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public Set<TournamentPlayer> getSubscribers() { return subscribers; }
	public void setSubscribers(Set<TournamentPlayer> subscribers) { this.subscribers = subscribers; }

	public TournamentPlayer findSubscription(final Player player) {
		return (TournamentPlayer)CollectionUtils.find(subscribers, new Predicate() {

			public boolean evaluate(Object object) {
				TournamentPlayer ts = (TournamentPlayer)object;
				return ts.getPlayer().equals(player);
			}			
		});

	}

	public Set<TournamentEvent> getTournamentEvents() {	
		TreeSet<TournamentEvent> events = new TreeSet<TournamentEvent>();
		events.addAll(singleMatchPeriods);
		events.addAll(subTournaments);
		
		return events;
		
	}
	
	public String getSeason() { return season; }
	public void setSeason(String season) { this.season = season; }
	
	public Set<SubTournament> getSubTournaments() { return subTournaments; }
	public void setSubTournaments(Set<SubTournament> subTournaments) { this.subTournaments = subTournaments; }

	public Set<SingleMatchPeriod> getSingleMatchPeriods() { return singleMatchPeriods; }
	public void setSingleMatchPeriods(Set<SingleMatchPeriod> singleMatchPeriods) { this.singleMatchPeriods = singleMatchPeriods; }

	public TournamentRanking calculateRanking() {		
		AbstractRankingVisitor visitor = new UpdateRankingVisitor(ranking);
		for (TournamentEvent event : getTournamentEvents()) {
			if (event.isFinished()) {
				event.accept(visitor);
			}
		}
		
		return visitor.getRanking();
	}
	
	public TournamentRanking getRanking() {
		return calculateRanking();		
	}
	

	public void addSingleMatchPeriod(SingleMatchPeriod period) {
		if (period.getTournament() == null) {
			period.setTournament(this);
		}
		singleMatchPeriods.add(period);
		//tournamentEvents.add(period);		
	}

	public void addSubTournament(SubTournament subTournament) {
		if (subTournament.getTournament() == null) {
			subTournament.setTournament(this);
		}
		subTournaments.add(subTournament);
		//tournamentEvents.add(subTournament);		
	}

	public boolean isActive() { return active; }
	public void setActive(boolean active) { this.active = active; }

	public boolean equals(Object other) {
		if (other instanceof Tournament) {
			Tournament that = (Tournament)other;
			return this.getId() == that.getId();
		} else {
			return false;
		}
	}
}