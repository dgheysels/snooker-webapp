package domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

public class TournamentRanking  {

	private Tournament tournament;
	private List<TournamentRankingEntry> entries;
	
	public TournamentRanking(Tournament tournament) {		
		this.tournament = tournament;
		this.entries = new ArrayList<TournamentRankingEntry>();		
	}
	
	public List<TournamentRankingEntry> getEntries() { return entries; }
		
	public Tournament getTournament() {	return tournament;}
	public void setTournament(Tournament tournament) {this.tournament = tournament;	}
	
	public Hashtable<TournamentType, Integer> getHighestBreaks() {
		Hashtable<TournamentType, Integer> highestBreaksOverall = new Hashtable<TournamentType, Integer>();

		for (TournamentRankingEntry entry : entries) {
			// hb = eventtype1 value1, eventtype2 value2
			Hashtable<TournamentType, Integer> highestBreaksEntry = entry.getHighestBreaks();
			
			for (Entry<TournamentType, Integer> hbEntry : highestBreaksEntry.entrySet()) {
				if ((!highestBreaksOverall.containsKey(hbEntry.getKey())) || (hbEntry.getValue() > highestBreaksOverall.get(hbEntry.getKey()))) {
					highestBreaksOverall.put(hbEntry.getKey(), hbEntry.getValue());					
				}	
			}
		}
		
		return highestBreaksOverall;
	}
	
	public TournamentRankingEntry find(final Player player) {
		TournamentRankingEntry entry = (TournamentRankingEntry)CollectionUtils.find(entries, new Predicate() {
			
			public boolean evaluate(Object e) {
				return ((TournamentRankingEntry)e).getPlayer().equals(player);
			}			
		});
		
		if (entry != null) {
			return entry;
		} else {
			TournamentRankingEntry newEntry = new TournamentRankingEntry(player, tournament);
			entries.add(newEntry);
			return newEntry;
		}
	}
	
	public void sortEntries() {
		Collections.sort(getEntries());
	}
	
	public boolean isDisqualified(Player player) {
		TournamentRankingEntry entry = find(player);
		return entry.isDisqualified();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int i=1;
		
		Iterator<TournamentRankingEntry> it = entries.iterator();
		while (it.hasNext()) {
			sb.append("---------------------------------------------------------------------------------------------------------\n");
			sb.append((i++) + " " + it.next() + "\n");
		}
		
		return sb.toString();
	}
}
