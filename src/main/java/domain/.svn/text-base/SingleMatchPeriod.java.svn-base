package domain;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

public class SingleMatchPeriod extends TournamentEvent /*implements Comparable<SingleMatchPeriod>*/ {

	private Set<SingleMatch> matches = new TreeSet<SingleMatch>();
	
	public SingleMatchPeriod() {}

	public Set<SingleMatch> getMatches() { return matches; }
	public void setMatches(Set<SingleMatch> matches) { this.matches = matches; }
	
	public List<SingleMatch> getMatchesA() { 
		return (List<SingleMatch>) CollectionUtils.select(matches, new Predicate() {
			
			public boolean evaluate(Object object) {
				SingleMatch match = (SingleMatch)object;
				return 
					"A".equalsIgnoreCase(match.getPlayerA().getGrade(tournament)) &&
					"A".equalsIgnoreCase(match.getPlayerB().getGrade(tournament));  
			}
		});					
	}
	
	public List<SingleMatch> getMatchesB() { 
		return (List<SingleMatch>) CollectionUtils.select(matches, new Predicate() {
			
			public boolean evaluate(Object object) {
				SingleMatch match = (SingleMatch)object;
				return 
					"B".equalsIgnoreCase(match.getPlayerA().getGrade(tournament)) &&
					"B".equalsIgnoreCase(match.getPlayerB().getGrade(tournament)); 
			}
		});					
	}
	
	public void addMatch(SingleMatch match) {
		if (match.getPeriod() == null) {
			match.setPeriod(this);
		}
		
		getMatches().add(match);
	}
	

	public void updateRanking(TournamentRanking ranking) {

		for (SingleMatch match : getMatches()) {
			Player winner = match.getWinner();
			Player loser  = match.getLoser();	
			
			TournamentRankingEntry entryWinner = ranking.find(winner);
			TournamentRankingEntry entryLoser  = ranking.find(loser);

			/* winner is A-player, 10pts
			 * loser is A-player, #frames won * 2
			 * winner is B-player, 5pts;
			 * loser is B-player, #frames won 
			 */
			// process points
			// winner			
			if (winner.isDisqualified(tournament)) {
				entryWinner.setPoints(this, -1);
			} else {
				if (winner.getGrade(tournament).equalsIgnoreCase("A")) {
					entryWinner.setPoints(this, 10);
				} else {
					entryWinner.setPoints(this, 5);
				}
				entryWinner.updateSingleMatchWins(1);
				Break hbWinner = match.getHighestBreak(winner);
				entryWinner.updateHighestBreak(getType(), hbWinner.getValue());
			}
			
			// loser
			if (loser.isDisqualified(tournament)) {
				entryLoser.setPoints(this, -1);
			} else {
				if (match.getScore(loser) > -1) {
					if (loser.getGrade(tournament).equalsIgnoreCase("A")) {
						if (winner.isDisqualified(tournament)) {
							entryLoser.setPoints(this, 10);
							entryLoser.updateSingleMatchWins(1);
						} else {
							entryLoser.setPoints(this, match.getScore(loser) * 2);
						}
					} else {
						if (winner.isDisqualified(tournament)) {
							entryLoser.setPoints(this, 5);
							entryLoser.updateSingleMatchWins(1);
						} else {
							entryLoser.setPoints(this, match.getScore(loser));
						}
					}
					Break hbLoser = match.getHighestBreak(loser);
					entryLoser.updateHighestBreak(getType(), hbLoser.getValue());
				} else {
					entryLoser.addForfait(this);
				}
			}
		}
	}
	
	/**
	 * retrieve highest break of a particular player in this tournament-event
	 */
	public Break getHighestBreak(Player player) {
		SingleMatch match = findMatch(player);
		return match.getHighestBreak(player);
	}
		
	private SingleMatch findMatch(Player player) {
		for (SingleMatch match : getMatches()) {
			if (match.getPlayerA().equals(player) || match.getPlayerB().equals(player)) {
				return match;
			}
		}
		return null;
	}
}
