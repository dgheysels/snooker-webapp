package domain;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.TreeSet;

import domain.utils.Tuple;

public class KnockoutRound {
	
	private int id;
	private String name;
	private SubTournament subTournament;
	private Set<KnockoutMatch> matches = new TreeSet<KnockoutMatch>();
		
	private Set<Player> players = new HashSet<Player>();
	
	public KnockoutRound() {}
	
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public SubTournament getSubTournament() { return subTournament; }
	public void setSubTournament(SubTournament subTournament) { this.subTournament = subTournament; }

	public Set<KnockoutMatch> getMatches() { return matches; }
	public void setMatches(Set<KnockoutMatch> matches) { this.matches = matches; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	public void addMatch(KnockoutMatch match) {
		if (getMatches().contains(match)) {
			getMatches().remove(match);
		}
		
		if (match.getKnockoutRound() == null) {
			match.setKnockoutRound(this);
		}
		getMatches().add(match);
		
	}
	
	public Set<Player> getPlayers() { 
		if (players.isEmpty()) {
			if (getSubTournament().getPoules().isEmpty()) {				
				Set<TournamentPlayer> subscribers = getSubTournament().getTournament().getSubscribers();
				Set<Player> players = new HashSet<Player>();
				for (TournamentPlayer tp : subscribers) {
					players.add(tp.getPlayer());					
				}
				return players;
			} else {
				for (Poule p : getSubTournament().getPoules()) {
					players.addAll(p.getWinners());
				}
			}
		}
		return players;
	}
		
	public void setPlayers(Set<Player> players) { this.players = players; }
	
	/**
	 * Get the height of the knockout-table.  
	 * Height = upperbound( log2(number_of_players) ) - 1
	 */	
	public int getHeight() {
		return (int)Math.ceil((Math.log(getPlayers().size()) / Math.log(2))) - 1;
	}	
	
	/**
	 * For each player in knockout-round, calculate his points
	 * Matches are sorted and traversed from lowest-rank to highest-rank (= FINAL)
	 */	
	public Hashtable<Player, Tuple<Integer, Integer>> calculatePoints() {
		
		Hashtable<Player, Tuple<Integer, Integer>> points = new Hashtable<Player, Tuple<Integer, Integer>>();
		
		for (KnockoutMatch match : getMatches()) {
			
			Player pA = match.getPlayerA();
			int scoreA = (match.getScoreA() == -1) ? 0 : match.getScoreA();
			int highestBreakA = match.getHighestBreak(pA).getValue();
			
			Player pB = match.getPlayerB();
			int scoreB = (match.getScoreB() == -1) ? 0 : match.getScoreB();
			int highestBreakB = match.getHighestBreak(pB).getValue();
									
			if (!points.containsKey(pA)) {
				points.put(pA, new Tuple<Integer, Integer>(0, 0));
			}
						
			if (!points.containsKey(pB)) {
				points.put(pB, new Tuple<Integer, Integer>(0, 0));
			}
			
			Tuple<Integer, Integer> pointsA = points.get(pA);
			Tuple<Integer, Integer> pointsB = points.get(pB);
			
			switch(match.getType()) {
			case EIGHT_FINAL:
				if (scoreA < scoreB) {
					pointsA.setValue1(subTournament.getPointsLoserEF() + scoreA * 2);
				} else {
					//points.put(pB, 15 + scoreB * 2);
					pointsB.setValue1(subTournament.getPointsLoserEF() + scoreB * 2);
				}				
				break;
				
			case QUARTER_FINAL:
				if (scoreA < scoreB) {
					pointsA.setValue1(subTournament.getPointsLoserQF() + scoreA * 2);
				} else {
					//points.put(pB, 15 + scoreB * 2);
					pointsB.setValue1(subTournament.getPointsLoserQF() + scoreB * 2);
				}
				
				break;
				
			case SEMI_FINAL:
				if (scoreA < scoreB) {
					pointsA.setValue1(subTournament.getPointsLoserSF() + scoreA * 2);
				} else {
					//points.put(pB, 15 + scoreB * 2);
					pointsB.setValue1(subTournament.getPointsLoserSF() + scoreB * 2);
				}
				
				break;
				
			case FINAL:
				if (scoreA < scoreB) {
					pointsA.setValue1(subTournament.getPointsLoserF() + scoreA * 2);
					pointsB.setValue1(subTournament.getPointsWinnerF());
				} else {
					//points.put(pB, 15 + scoreB * 2);
					pointsB.setValue1(subTournament.getPointsLoserF() + scoreB * 2);
					pointsA.setValue1(subTournament.getPointsWinnerF());
				}
				
				break;
			}
			
			if (highestBreakA > pointsA.getValue2()) {
				pointsA.setValue2(highestBreakA);
			}
			if (highestBreakB > pointsB.getValue2()) {
				pointsB.setValue2(highestBreakB);
			}
			
			points.put(pA, pointsA);
			points.put(pB, pointsB);
			
		}
		return points;
	}
}
