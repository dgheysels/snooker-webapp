package domain;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;


public class Tournament  {

	private int id;
	private String name = "";
	private String season = "";
	private boolean active;
	private Set<TournamentPlayer> subscribers = new TreeSet<TournamentPlayer>();
	private Set<SubTournament> subTournaments = new TreeSet<SubTournament>();
	private Set<SingleMatchPeriod> singleMatchPeriods = new TreeSet<SingleMatchPeriod>();
	
	private transient TournamentRanking ranking = new TournamentRanking(this);

	public Tournament() {
		setActive(true);		
	}

	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public Set<TournamentPlayer> getSubscribers() { return subscribers; }
	public void setSubscribers(Set<TournamentPlayer> subscribers) { this.subscribers = subscribers; }

	public TournamentPlayer findSubscription(final Player player) {
		return (TournamentPlayer)CollectionUtils.find(subscribers, new Predicate() {

			public boolean evaluate(Object object) {
				TournamentPlayer ts = (TournamentPlayer)object;
				return ts.getPlayer().equals(player);
			}			
		});

	}

	public Set<SubTournament> getSubTournaments() { return subTournaments; }
	public void setSubTournaments(Set<SubTournament> subTournaments) { this.subTournaments = subTournaments; }

	public SubTournament getSubTournament(int id) {
		for (SubTournament tr : getSubTournaments()) {
			if (tr.getId() == id) {
				return tr;
			}
		}

		return null;
	}

	public String getSeason() { return season; }
	public void setSeason(String season) { this.season = season; }

	public TournamentRanking calculateRanking() {		
		ranking.calculate();
		return ranking;
	}

	public TournamentRanking getRanking() {
		return calculateRanking();		
	}

	public void disqualify(Player player) {
		// TODO: alle best-of-7 matches van deze speler worden NUL-wedstrijden


	}

	public Set<SingleMatchPeriod> getSingleMatchPeriods() { return singleMatchPeriods; }
	public void setSingleMatchPeriods(Set<SingleMatchPeriod> singleMatchPeriods) { this.singleMatchPeriods = singleMatchPeriods; }

	public void addSingleMatchPeriod(SingleMatchPeriod period) {
		if (period.getTournament() == null) {
			period.setTournament(this);
		}
		getSingleMatchPeriods().add(period);	
	}

	public void addSubTournament(SubTournament subTournament) {
		if (subTournament.getTournament() == null) {
			subTournament.setTournament(this);
		}

		getSubTournaments().add(subTournament);	
	}

	public Set<TournamentEvent> getAllEvents() {
		Set<TournamentEvent> events = new TreeSet<TournamentEvent>(getSubTournaments());
		events.addAll(getSingleMatchPeriods());
		return events;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean equals(Object other) {
		if (other instanceof Tournament) {
			Tournament that = (Tournament)other;
			return this.getId() == that.getId();
		} else {
			return false;
		}
	}

	public Set<TournamentType> getDifferentTypes() {
		TreeSet<TournamentType> result = new TreeSet<TournamentType>(
				new Comparator<TournamentType>() {
					public int compare(TournamentType o1, TournamentType o2) {
						return o1.getSortOrder() - o2.getSortOrder();
					}
				});

		result.addAll(CollectionUtils.collect(getAllEvents(), new Transformer() {
			public Object transform(Object input) {
				return ((TournamentEvent)input).getType();				
			}
		}));
		
		return result;
	}
}