package domain;


public enum TournamentType implements Comparable<TournamentType> {
	
	SM(1, "SMB"),
	HT(2, "HTB"),
	ET(3, "ETB");

	TournamentType(int value, String breakname) {
		this.value = value;
		this.breakname = breakname;
	}
	
	private int value;
	private String breakname;
		
	public static TournamentType fromValue(int value) {
		for (TournamentType t : values()) {
			if (t.value == value) {
				return t;
			}
		}
		
		return null;
	}
	
	public int getValue() { return this.value; }
	public String getBreakname() { return this.breakname; }	
}
