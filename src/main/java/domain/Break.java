package domain;

public class Break implements Comparable<Break> {

	private int id;
	private Player player;	
	private Match match;
	private int value;
	
	public Break() {
		this.value = 0;
	}

	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public Player getPlayer() { return player; }
	public void setPlayer(Player player) { this.player = player; }
	
	public int getValue() { return value; }
	public void setValue(int value) { this.value = value; }

	public Match getMatch() { return match; }
	public void setMatch(Match match) { this.match = match; }
		
	public int compareTo(Break other) {		
		return this.getValue() - other.getValue();
	}
	
	@Override
	public String toString() {
		return String.valueOf(value);
	}
	
	@Override
	public boolean equals(Object other) {
		if (other != null && other instanceof Break) {
			Break that = (Break)other;
			return this.value == that.getValue();
		} else {
			return false;
		}
	}
}
