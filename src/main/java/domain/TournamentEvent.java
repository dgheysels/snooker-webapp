package domain;

import java.util.Date;

import domain.visitor.AbstractRankingVisitor;

public abstract class TournamentEvent implements Comparable<TournamentEvent> {

	protected int id;
	protected Tournament tournament;
	protected String name;
	protected Date fromDate;
	protected Date toDate;
	protected boolean finished;
	
	transient protected TournamentType type;
	protected int typeValue;
	
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }
	
	public Tournament getTournament() { return tournament; }
	public void setTournament(Tournament tournament) { this.tournament = tournament; }
	
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	public Date getFromDate() { return fromDate; }
	public void setFromDate(Date fromDate) { this.fromDate = fromDate; }
	
	public Date getToDate() { return toDate; }
	public void setToDate(Date toDate) { this.toDate = toDate; }

	public boolean isFinished() { return finished; }
	public void setFinished(boolean finished) { this.finished = finished; }

	public TournamentType getType() { return TournamentType.fromValue(this.getTypeValue()); }
	public void setType(TournamentType type) { this.setTypeValue(type.getValue()); }
	
	public int getTypeValue() { return typeValue; }
	public void setTypeValue(int typeValue) { this.typeValue = typeValue; }
	
	public abstract void accept(AbstractRankingVisitor visitor);	
	
	public int compareTo(TournamentEvent event) {
		return getFromDate().compareTo(event.getFromDate());
	}
}
