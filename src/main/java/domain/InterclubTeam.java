package domain;

import java.util.ArrayList;
import java.util.List;

public class InterclubTeam {

	private int id;
	private String name;
	private InterclubDivision interclubDivision;
	private List<Player> players = new ArrayList<Player>();

	public InterclubTeam() {}

	public void setPlayer1(Player player) {		
		player.setInterclubTeam(this);
		player.setInterclubRank(0);
		players.add(0, player);		
	}
	
	public Player getPlayer1() {
		return players.get(0);
	}
	
	public void setPlayer2(Player player) {
		players.add(1, player);
		player.setInterclubTeam(this);
		player.setInterclubRank(1);
	}
	
	public Player getPlayer2() {
		return players.get(1);
	}
	
	public void setPlayer3(Player player) {
		if (player != null) {
			players.add(2, player);
			player.setInterclubTeam(this);
			player.setInterclubRank(2);
		}
	}
	
	public Player getPlayer3() {
		return players.get(2);
	}
	
	public boolean contains(Player p) {
		return players.contains(p);
	}	
	
	
	public Player getPlayer(int index) throws Exception {
		if (index < 3) {
			return players.get(index);
		} else {
			throw new Exception("index should be between 0 and 3");
		}
	}
	
	
	
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public List<Player> getPlayers() { return players; }
	public void setPlayers(List<Player> players) { this.players = players; }

	public InterclubDivision getInterclubDivision() { return interclubDivision; }
	public void setInterclubDivision(InterclubDivision interclubDivision) { this.interclubDivision = interclubDivision; }
	
}
