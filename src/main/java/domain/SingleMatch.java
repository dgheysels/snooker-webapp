package domain;

import java.util.Date;

public class SingleMatch extends Match implements Comparable<SingleMatch> {
	
	private Date date;
	private SingleMatchPeriod period;
	
	public SingleMatch() {}
	
	public SingleMatch(int id, Player playerA, Player playerB) {
		super(id, playerA, playerB);		
	}

	public Date getDate() { return date; }
	public void setDate(Date date) { this.date = date; }
	
	public SingleMatchPeriod getPeriod() { return period; }
	public void setPeriod(SingleMatchPeriod period) { this.period = period; }


	/**
	 * Comparison of single-matches is based on their date
	 * If dates are equal, then ID of player-A makes distinction
	 * 
	 * TODO check (if necessary) : compareTo = 0 , then equal = true
	 */
	
	public int compareTo(SingleMatch other) {
		if (this.getDate().getTime() == other.getDate().getTime()) {
			return this.getPlayerA().getId() - other.getPlayerA().getId();
		} else if (this.getDate().getTime() > other.getDate().getTime()) {
			return 1;
		} else {
			return -1;
		}
	}
	
	/**
	 * Single-matches are equal if:
	 *  > players are equal
	 *  > scores are equal
	 *  > dates are equal
	 */
	public boolean equals(Object other) {
		if (other != null && other instanceof SingleMatch) {
			SingleMatch that = (SingleMatch)other;
			return super.equals(that) && this.getDate().equals(that.getDate());
		} else {
			return false;
		}
	}
}
