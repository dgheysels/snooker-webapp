package domain;

import java.util.Hashtable;
import java.util.Map.Entry;

public class TournamentRankingEntry implements Comparable<TournamentRankingEntry> {

	private Tournament tournament;
	private Player player;
	private Hashtable<TournamentEvent, Integer> rankingPoints;
	private int numberOfForfaits;
	private int singleMatchWins;
	private Hashtable<TournamentType, Integer> highestBreaks;
			
	public TournamentRankingEntry(Player player, Tournament tournament) {
		this.tournament = tournament;
		this.player = player;
		this.rankingPoints = new Hashtable<TournamentEvent, Integer>();
		this.singleMatchWins = 0;
		this.highestBreaks = new Hashtable<TournamentType, Integer>();
		for (TournamentType type : TournamentType.values()) {
			this.highestBreaks.put(type, 0);
		}
	}
	
	public Player getPlayer() { return player; }
	public void setPlayer(Player player) { this.player = player; }

	public Hashtable<TournamentEvent, Integer> getRankingPoints() { return rankingPoints; }

	public void increaseSingleMatchWins() {
		singleMatchWins++;
	}
	
	public boolean isDisqualified() {
		return player.isDisqualified(tournament);
	}
	
	/**
	 * update the highest break for a particular event for this entry
	 */
	public void updateHighestBreak(TournamentType eventType, int value) {				
		if (value > highestBreaks.get(eventType)) {
			highestBreaks.put(eventType, value);
		}
	}
	
	public void setPoints(TournamentEvent event, int points) {
		if (points < 0) {
			addForfait(event);
		}
		rankingPoints.put(event, points);
	}
	
	public void addPoints(TournamentEvent event, int points) {
		int pts = rankingPoints.get(event);
		rankingPoints.put(event, pts + points);
	}
	
	public int getPoints(TournamentEvent event) {
		return rankingPoints.get(event);
	}
	
	public void inversePoints(TournamentEvent event) {
		int pts = rankingPoints.get(event);
		rankingPoints.put(event, pts * -1);
	}

	/**
	 * Player forfaits a tournamentEvent.  The player's points for this event is -1
	 */
	public void addForfait(TournamentEvent event) {
		//setPoints(event, -1);
		this.numberOfForfaits++;
	}
		
	/**
	 * Calculate the total points for the player in the rankingtable
	 * point = -1 --> match not played
	 * point < -10 / -25 / ... --> forfait during knockoutround, get -points 
	 * 
	 */
	public int getTotal() {
		//if (this.numberOfForfaits >= 3) {
		//	return -1;
		//} else {
			int total = 0;
			for (Integer p : rankingPoints.values()) {			
				// don't take FF into account
				if (p != -1) {
					total += Math.abs(p);
				}
			}		
			
			return total;
		//}
	}
	
	public String getGrade() {
		return player.getGrade(tournament);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();		
		sb.append(player + "\t\t\n");
		for (Entry<TournamentEvent, Integer> entry : rankingPoints.entrySet()) {
			sb.append("\t" + entry.getKey().getName() + ":" + entry.getValue());
		}
		
		sb.append("\t FF: " + numberOfForfaits);
		sb.append("\t = " + getTotal());
		
		return sb.toString();
	}

	/**
	 * Compare TournamentRankingEntry
	 * Compare on:
	 *  - total points
	 *  - break
	 */
	
	public int compareTo(TournamentRankingEntry o) {
		if (o.getTotal() == getTotal()) {
			if (o.getSingleMatchWins() == getSingleMatchWins()) {
				return o.getHighestBreak() - getHighestBreak();
			} else {
				return o.getSingleMatchWins() - getSingleMatchWins();
			}
		} else {
			return o.getTotal() - getTotal();
		}
	}

	public int getSingleMatchWins() {
		return singleMatchWins;
	}

	/**
	 * returns highest break over all tournamentevents for this entry
	 */
	public int getHighestBreak() {
		int highestBreak = 0;
		for (int v : highestBreaks.values()) {
			if (v > highestBreak) {
				highestBreak = v;
			}
		}
				
		return highestBreak;
	}
	
	/**
	 * returns the set of highest breaks for all tournamentevents in this entry
	 */
	public Hashtable<TournamentType, Integer> getHighestBreaks() {
		return highestBreaks;
	}

	public int getNumberOfForfaits() {
		return numberOfForfaits;
	}
}
