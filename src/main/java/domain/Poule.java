package domain;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import domain.utils.Tuple;

public class Poule implements Comparable<Poule> {

	private int id;
	private String name;
	private SubTournament subTournament;
	private Set<PouleMatch> matches = new HashSet<PouleMatch>();
	private Set<Player> winners = new HashSet<Player>();
	
	// table containing number of frames won and highest break for each player in the poule
	// needed to (easily) determine the ranking of player in the poule
	private transient PouleTable pouleranking;
	
	public Poule() {	
		this.pouleranking = new PouleTable(this);
	}

	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public SubTournament getSubTournament() { return subTournament; }
	public void setSubTournament(SubTournament subTournament) { this.subTournament = subTournament; }

	public Set<PouleMatch> getMatches() { return matches; }
	public void setMatches(Set<PouleMatch> matches) { this.matches = matches; }
	
	/**
	 * Find match played between the two players 
	 */
	public PouleMatch findMatch(Player playerA, Player playerB) {
		for (PouleMatch match : getMatches()) {
			if (match.getPlayerA().equals(playerA) && match.getPlayerB().equals(playerB) ||
				match.getPlayerA().equals(playerB) && match.getPlayerB().equals(playerA)) {
				
				return match;
			}
		}	
		return null;
	}
	
	/**
	 * Add a match to this poule and update poule-ranking
	 */
	public void addMatch(PouleMatch match) {
		if (match.getPoule() == null) {
			match.setPoule(this);
		}
		
		this.getMatches().add(match);		
		this.pouleranking.add(match);
	}
		
	public PouleTable getPouleranking() {
		
		// if position-table is empty (poule loaded from database),
		// try to generate positions table using the persisted matches and breaks
		if (pouleranking.isEmpty()) {
			for (PouleMatch match : this.getMatches()) {				
				pouleranking.add(match);
			}
		}
		
		return pouleranking;
	}
	
	public void setWinners(Set<Player> winners) { this.winners = winners; }
	public Set<Player> getWinners() { return winners; }
	
	public void addWinner(Player p) {
		getWinners().add(p);
		pouleranking.get(p).setWinner(true);
	}
	
	public boolean isWinner(Player p) {
		return getWinners().contains(p);
	}

	public Set<Player> getPlayers() {
		Set<Player> players = new HashSet<Player>();
		
		for (PouleMatch m : getMatches()) {
			players.add(m.getPlayerA());
			players.add(m.getPlayerB());
		}
		
		return players;
	}
	
	/**
	 * calculates for each player the number of points earned in this poule
	 * and the highest break
	 * 
	 * @return
	 */
	public Hashtable<Player, Tuple<Integer, Integer>> calculatePoints() {
		
		Hashtable<Player, Tuple<Integer, Integer>> points = new Hashtable<Player, Tuple<Integer, Integer>>();
		
		Iterator<PouleEntry> it = getPouleranking().iterator();
		while (it.hasNext()) {
			PouleEntry next = it.next();
			points.put(next.getPlayer(), new Tuple<Integer, Integer>(next.getFramesWon() * getSubTournament().getPointsFrame(), next.getBrk().getValue()));
		}
	
		return points;
	}
	
	public int compareTo(Poule o) {
		return this.id - o.getId();
	}
}
