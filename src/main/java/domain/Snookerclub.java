package domain;

import java.util.HashSet;
import java.util.Set;

public class Snookerclub {

	private int id;
	private String name;
	private Address address;
	private String telnr;
	private String email;
	private Set<Player> members = new HashSet<Player>();
	
	public Snookerclub() {}
	
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public Set<Player> getMembers() { return members; }
	public void setMembers(Set<Player> members) { this.members = members; }

	public Address getAddress() { return address; }
	public void setAddress(Address address) { this.address = address; }

	public String getTelnr() { return telnr; }
	public void setTelnr(String telnr) { this.telnr = telnr; }

	public String getEmail() { return email; }
	public void setEmail(String email) { this.email = email; }
	
}
