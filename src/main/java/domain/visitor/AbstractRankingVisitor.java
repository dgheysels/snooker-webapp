package domain.visitor;

import domain.KnockoutMatch;
import domain.KnockoutRound;
import domain.Poule;
import domain.PouleEntry;
import domain.SingleMatch;
import domain.Tournament;
import domain.TournamentEvent;
import domain.TournamentRanking;

public abstract class AbstractRankingVisitor {

	protected TournamentRanking ranking;
	protected TournamentEvent event;
	
	protected AbstractRankingVisitor(TournamentRanking ranking) {
		this.ranking = ranking;
	}
	
	protected Tournament getTournament() { return getRanking().getTournament(); }
	public TournamentRanking getRanking() { ranking.sortEntries(); return ranking;	}
	protected TournamentEvent getEvent() { return event; }
	public void setEvent(TournamentEvent event) { this.event = event; }
	
	public abstract void visitSingleMatch(SingleMatch singleMatch);
	public abstract void visitPoule(Poule poule);
	public abstract void visitPouleEntry(PouleEntry pouleEntry);
	public abstract void visitKnockoutRound(KnockoutRound knockoutRound);
	public abstract void visitKnockoutMatch(KnockoutMatch knockoutMatch);
	
}
