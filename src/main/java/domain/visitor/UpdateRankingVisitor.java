package domain.visitor;

import java.util.Iterator;

import domain.Break;
import domain.KnockoutMatch;
import domain.KnockoutRound;
import domain.Player;
import domain.Poule;
import domain.PouleEntry;
import domain.PouleTable;
import domain.SingleMatch;
import domain.SubTournament;
import domain.TournamentRanking;
import domain.TournamentRankingEntry;

public class UpdateRankingVisitor extends AbstractRankingVisitor {

	public UpdateRankingVisitor(TournamentRanking ranking) {
		super(ranking);
	}
	
	@Override
	public void visitKnockoutMatch(KnockoutMatch knockoutMatch) {

		if (!knockoutMatch.isPlayed()) {
			TournamentRankingEntry entryPlayerA = getRanking().find(knockoutMatch.getPlayerA());
			TournamentRankingEntry entryPlayerB  = getRanking().find(knockoutMatch.getPlayerB());
			
			entryPlayerA.setPoints(getEvent(), -1);
			entryPlayerB.setPoints(getEvent(), -1);
		} else {
			Player winner = knockoutMatch.getWinner();
			TournamentRankingEntry entryWinner = ranking.find(winner);
			Player loser = knockoutMatch.getLoser();
			TournamentRankingEntry entryLoser = ranking.find(loser);
			
			if (winner.isDisqualified(getTournament())) {
				entryWinner.setPoints(event, -1);
			} else {			
				int breakWinner = knockoutMatch.getHighestBreak(winner).getValue();
				int pointsWinner = knockoutMatch.getPointsWinner((SubTournament)getEvent());
		
				entryWinner.setPoints(getEvent(), pointsWinner);
				entryWinner.updateHighestBreak(getEvent().getType(), breakWinner);
			}
			
			if (loser.isDisqualified(getTournament())) {
				entryLoser.setPoints(event, -1);
			} else {		
				int breakLoser = knockoutMatch.getHighestBreak(loser).getValue();
				int pointsLoser = knockoutMatch.getPointsLoser((SubTournament)getEvent());
							
				entryLoser.setPoints(getEvent(), pointsLoser);
				entryLoser.updateHighestBreak(getEvent().getType(), breakLoser);
			}		
		}
	}

	@Override
	public void visitKnockoutRound(KnockoutRound knockoutRound) {
		for (KnockoutMatch match : knockoutRound.getMatches()) {
			visitKnockoutMatch(match);
			
		}
	}

	@Override
	public void visitPoule(Poule poule) {
		PouleTable pouleRanking = poule.getPouleranking();
		
		Iterator<PouleEntry> it = pouleRanking.iterator();
		while (it.hasNext()) {
			PouleEntry pouleEntry = it.next();
			visitPouleEntry(pouleEntry);
		}
	}

	@Override
	public void visitPouleEntry(PouleEntry pouleEntry) {
		Player player = pouleEntry.getPlayer();
		TournamentRankingEntry entry = getRanking().find(player);
		
		if (player.isDisqualified(getTournament())) {
			entry.setPoints(getEvent(), -1);			
		} else {
			int points = pouleEntry.getFramesWon() * ((SubTournament)getEvent()).getPointsFrame();
			int highestBreak = pouleEntry.getBrk().getValue();
			entry.setPoints(getEvent(), points);
			entry.updateHighestBreak(getEvent().getType(), highestBreak);
		}
	}

	@Override
	public void visitSingleMatch(SingleMatch singleMatch) {
		// process points
		if (!singleMatch.isPlayed()) {
			
			TournamentRankingEntry entryPlayerA = getRanking().find(singleMatch.getPlayerA());
			TournamentRankingEntry entryPlayerB  = getRanking().find(singleMatch.getPlayerB());
			
			entryPlayerA.setPoints(getEvent(), -1);
			entryPlayerB.setPoints(getEvent(), -1);
		} else {
							
			Player winner = singleMatch.getWinner();
			Player loser  = singleMatch.getLoser();	
			
			TournamentRankingEntry entryWinner = getRanking().find(winner);
			TournamentRankingEntry entryLoser  = getRanking().find(loser);

			/* winner is A-player, 10pts
			 * loser is A-player, #frames won * 2
			 * winner is B-player, 5pts;
			 * loser is B-player, #frames won 
			 */

			// winner			
			if (winner.isDisqualified(getTournament())) {
				entryWinner.setPoints(getEvent(), -1);
			} else {
				if (winner.getGrade(getTournament()).equalsIgnoreCase("A")) {
					entryWinner.setPoints(getEvent(), 10);
				} else {
					entryWinner.setPoints(getEvent(), 5);
				}
				entryWinner.increaseSingleMatchWins();
				Break hbWinner = singleMatch.getHighestBreak(winner);
				entryWinner.updateHighestBreak(getEvent().getType(), hbWinner.getValue());
			}
			
			// loser
			if (loser.isDisqualified(getTournament())) {
				entryLoser.setPoints(getEvent(), -1);
			} else {
				if (singleMatch.getScore(loser) > -1) {
					if (loser.getGrade(getTournament()).equalsIgnoreCase("A")) {
						if (winner.isDisqualified(getTournament())) {
							entryLoser.setPoints(getEvent(), 10);
							entryLoser.increaseSingleMatchWins();
						} else {
							entryLoser.setPoints(getEvent(), singleMatch.getScore(loser) * 2);
						}
					} else {
						if (winner.isDisqualified(getTournament())) {
							entryLoser.setPoints(getEvent(), 5);
							entryLoser.increaseSingleMatchWins();
						} else {
							entryLoser.setPoints(getEvent(), singleMatch.getScore(loser));
						}
					}
					Break hbLoser = singleMatch.getHighestBreak(loser);
					entryLoser.updateHighestBreak(getEvent().getType(), hbLoser.getValue());
				} else {
					entryLoser.setPoints(getEvent(), -1);
				}
			}
		}

	}

}
