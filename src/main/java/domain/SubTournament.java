package domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;

import domain.visitor.AbstractRankingVisitor;

public class SubTournament extends TournamentEvent {

	private Set<Poule> poules = new TreeSet<Poule>();
	private KnockoutRound knockoutRound;
	private boolean withPoules;
	private boolean inKnockoutStage;
		
	private int pointsFrame;
	private int pointsLoser16F;
	private int pointsLoser8F;
	private int pointsLoser4F;
	private int pointsLoser2F;
	private int pointsLoserF;
	private int pointsWinnerF;
	private int pointsFrameKnockout;
	
	public SubTournament() {}
	
	public Set<Poule> getPoules() { return poules; }
	public void setPoules(Set<Poule> poules) { this.poules = poules; }

	public KnockoutRound getKnockoutRound() { return knockoutRound; }
	public void setKnockoutRound(KnockoutRound knockoutRound) { this.knockoutRound = knockoutRound; }

	/**
	 * Get players which are subscribed to the tournament, but didn't play in the poules
	 * @return set of players
	 */
	@SuppressWarnings(value="all")
	public Set<Player> getForfaits() {

		// retrieve all players subscribed for the tournament
		Set<Player> subscribers = new HashSet<Player>(CollectionUtils.collect(tournament.getSubscribers(), new Transformer() {
			public Object transform(Object el) {
				return ((TournamentPlayer)el).getPlayer();				
			}			
		}));

		// collect all players which have played a match in any poule
		Set<Player> players = new HashSet<Player>();
		for (Poule p : getPoules()) {
			players.addAll(p.getPlayers());
		}
		
		return new HashSet<Player>(CollectionUtils.subtract(subscribers, players));
	}
	
	public void accept(AbstractRankingVisitor visitor) {
		visitor.setEvent(this);
		
		for (Poule poule : getPoules()) {
			visitor.visitPoule(poule);
		}		
		
		// process forfaits: if player hasn't played all matches during poule-stage, then FF.
		// if player forfaits in knockout-stage, then he gets the points until last match he played
		for (Player forfait : getForfaits()) {
			TournamentRankingEntry entry = visitor.getRanking().find(forfait);
			entry.setPoints(this, -1);
		}
				
		visitor.visitKnockoutRound(getKnockoutRound());
	}
	
	public void addMatch(Poule poule, PouleMatch match) {		
		poule.addMatch(match);		
	}
	
	public void addMatch(KnockoutMatch match) {		
		getKnockoutRound().addMatch(match);
	}
	
	public int getPointsLoser16F() { return pointsLoser16F; }
	public void setPointsLoser16F(int pointsLoser16F) { this.pointsLoser16F = pointsLoser16F; }
	
	public int getPointsLoser8F() { return pointsLoser8F; }
	public void setPointsLoser8F(int pointsLoser8F) { this.pointsLoser8F = pointsLoser8F; }

	public int getPointsLoser4F() { return pointsLoser4F; }
	public void setPointsLoser4F(int pointsLoser4F) { this.pointsLoser4F = pointsLoser4F; }

	public int getPointsLoser2F() { return pointsLoser2F; }
	public void setPointsLoser2F(int pointsLoser2F) { this.pointsLoser2F = pointsLoser2F; }

	public int getPointsLoserF() { return pointsLoserF; }
	public void setPointsLoserF(int pointsLoserF) { this.pointsLoserF = pointsLoserF; }

	public int getPointsWinnerF() { return pointsWinnerF; }
	public void setPointsWinnerF(int pointsWinnerF) { this.pointsWinnerF = pointsWinnerF; }

	public int getPointsFrame() { return pointsFrame; }
	public void setPointsFrame(int pointsFrame) { this.pointsFrame = pointsFrame; }
	
	public int getPointsFrameKnockout() { return pointsFrameKnockout; }
	public void setPointsFrameKnockout(int pointsFrameKnockout) { this.pointsFrameKnockout = pointsFrameKnockout; }

	public boolean isWithPoules() { return withPoules; }
	public void setWithPoules(boolean withPoules) { this.withPoules = withPoules; }

	public boolean isInKnockoutStage() { return inKnockoutStage; }
	public void setInKnockoutStage(boolean inKnockoutStage) { this.inKnockoutStage = inKnockoutStage; }
	
	public Date getFromDate() { return fromDate; }
	public void setFromDate(Date fromDate) { this.fromDate = fromDate; }

	public Date getToDate() { return toDate; }
	public void setToDate(Date toDate) { this.toDate = toDate; }

	public boolean equals(Object other) {
		if (other != null && other instanceof SubTournament) {
			SubTournament that = (SubTournament)other;
			return this.name.equals(that.getName());
		} else {
			return false;
		}
	}
}
