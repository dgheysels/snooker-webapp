package domain;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Player implements Comparable<Player> {
	
	public enum InterclubRank {
		CAPTAIN(0, "Captain"),
		PLAYER2(1, "2nd player"),
		PLAYER3(2, "3rd player");
		
		private InterclubRank(int idx, String description) {
			this.idx = idx;
			this.description = description;
		}
		
		private int idx;
		private String description;
		
		public int getIdx() { return this.idx; }
		public String getDescription() { return this.description; }
	}
	
	private int id;
	private String name;
	private Address address = new Address();
	private String telnr;
	private String email;
	private String password;	
	private byte[] photo;
	private boolean active;	
	private boolean management;
	private String managementPosition;
	private Snookerclub snookerclub;
	private InterclubTeam interclubTeam;
	private Integer interclubRank;
	private Role role;
				
	private Set<Break> breaks = new HashSet<Break>();
	private Set<TournamentPlayer> subscriptions = new TreeSet<TournamentPlayer>();	
	private Set<Match> matchesAsPlayerA = new HashSet<Match>();
	private Set<Match> matchesAsPlayerB = new HashSet<Match>();
	
	// player info
	private String cueType;
	private String favoriteInternationalPlayer;
	private String favoriteBelgianPlayer;
	private Integer hbTraining;
	private Integer hbMatch;
	private String yearsPlayingSnooker;
	private String story;
	
	
	public Player() {}
	
	public Player(Role role) {
		this.active = true;
		this.management = false;
		this.role = role;
	}
		
	public Player(int id, String name, String password, boolean admin) {
		this.id = id;
		this.name = name;
		this.password = password;
		
	}
	
	public Player(int id, String name, String password, Snookerclub snookerclub, InterclubTeam interclubTeam, boolean admin) {
		this.id = id;
		this.name = name;
		this.password = password;
		this.snookerclub = snookerclub;
		this.interclubTeam = interclubTeam;
		
	}
	
	public int getId() { return this.id; }
	public void setId(int id) { this.id = id; }
	
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
		
	public String getPassword() { return password; }
	public void setPassword(String password) { this.password = password; }
	
	public Snookerclub getSnookerclub() { return snookerclub; }
	public void setSnookerclub(Snookerclub snookerclub) { this.snookerclub = snookerclub; }
	
	public InterclubTeam getInterclubTeam() { return interclubTeam; }
	
	public void setInterclubTeam(InterclubTeam interclubTeam) { 
		this.interclubTeam = interclubTeam;
//		if (this.interclubTeam != null && !this.interclubTeam.contains(this)) {
//			this.interclubTeam.getPlayers().add(this);
//		}
	}

	public Set<TournamentPlayer> getSubscriptions() { return subscriptions; }
	public void setSubscriptions(Set<TournamentPlayer> subscriptions) { this.subscriptions = subscriptions; }

	
	public Address getAddress() { return address; }
	public void setAddress(Address address) { this.address = address; }

	public String getTelnr() { return telnr; }
	public void setTelnr(String telnr) { this.telnr = telnr; }

	public String getEmail() { return email; }
	public void setEmail(String email) { this.email = email; }

	public Role getRole() { return role; }
	public void setRole(Role role) { this.role = role; }

	public boolean isActive() { return active; }
	public void setActive(boolean active) { this.active = active; }
	
	public Set<Break> getBreaks() { return breaks; }
	public void setBreaks(Set<Break> breaks) { this.breaks = breaks; }	
	
	/**
	 * get the all-time highest break of this player
	 */
	public Break getHighestBreak() {
		return ((TreeSet<Break>)getBreaks()).last();
	}
	
	public boolean equals(Object other) {
		if (other != null && other instanceof Player) {
			Player that = (Player)other;
			return this.getId() == that.getId();
		} else {
			return false;
		}
	}
	
	public int hashCode() {
		return 0;
	}
	
	public String toString() {
		return name;
	}

	public boolean isManagement() { return management; }
	public void setManagement(boolean management) { this.management = management; }

	public String getManagementPosition() { return managementPosition; }
	public void setManagementPosition(String managementPosition) { this.managementPosition = managementPosition; }

	public byte[] getPhoto() { return photo; }
	public void setPhoto(byte[] photo) { this.photo = photo; }

	public Integer getInterclubRank() { return interclubRank; }
	public void setInterclubRank(Integer interclubRank) { this.interclubRank = interclubRank; }

	public Set<Match> getMatchesAsPlayerA() { return matchesAsPlayerA; }
	public void setMatchesAsPlayerA(Set<Match> matchesAsPlayerA) { this.matchesAsPlayerA = matchesAsPlayerA; }

	public Set<Match> getMatchesAsPlayerB() { return matchesAsPlayerB; }
	public void setMatchesAsPlayerB(Set<Match> matchesAsPlayerB) { this.matchesAsPlayerB = matchesAsPlayerB; }
	
	public String getGrade(Tournament t) {
		TournamentPlayer tp = t.findSubscription(this);
		return tp.getGrade();
	}
		
	public int compareTo(Player o) {
		return this.getName().compareTo(o.getName());		
	}
	
	public boolean isDisqualified(Tournament t) {
		TournamentPlayer tp = t.findSubscription(this);
		return tp.isDisqualified();
	}

	public String getCueType() { return cueType; }
	public void setCueType(String cueType) { this.cueType = cueType; }

	public String getFavoriteInternationalPlayer() { return favoriteInternationalPlayer; }
	public void setFavoriteInternationalPlayer(String favoriteInternationalPlayer) { this.favoriteInternationalPlayer = favoriteInternationalPlayer; }

	public String getFavoriteBelgianPlayer() { return favoriteBelgianPlayer; }
	public void setFavoriteBelgianPlayer(String favoriteBelgianPlayer) { this.favoriteBelgianPlayer = favoriteBelgianPlayer; }

	public int getHbTraining() { return hbTraining; }
	public void setHbTraining(int hbTraining) { this.hbTraining = hbTraining; }

	public int getHbMatch() { return hbMatch; }
	public void setHbMatch(int hbMatch) { this.hbMatch = hbMatch; }

	public String getYearsPlayingSnooker() { return yearsPlayingSnooker; }
	public void setYearsPlayingSnooker(String yearsPlayingSnooker) { this.yearsPlayingSnooker = yearsPlayingSnooker; }

	public String getStory() { return story; }
	public void setStory(String story) { this.story = story; }
}
