package domain;

import java.util.HashSet;
import java.util.Set;

public class InterclubDivision {

	private int id;
	private String name;
	private int sortorder;
	private Set<InterclubTeam> teams = new HashSet<InterclubTeam>();
	
	public InterclubDivision() {}
	
	public InterclubDivision(int id, String name, int sortorder) {
		this.id = id;
		this.name = name;
		this.sortorder = sortorder;
	}

	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public Set<InterclubTeam> getTeams() { return teams; }
	public void setTeams(Set<InterclubTeam> teams) { this.teams = teams; }

	public int getSortorder() { return sortorder; }
	public void setSortorder(int sortorder) { this.sortorder = sortorder; }
}
