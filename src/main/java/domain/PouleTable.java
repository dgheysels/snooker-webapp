package domain;

import java.util.Iterator;
import java.util.TreeSet;

public class PouleTable extends TreeSet<PouleEntry> {

	private Poule poule;
	
	public PouleTable(Poule poule) {
		super();
		this.poule = poule;
	}
	
	public PouleMatch findMatch(Player playerA, Player playerB) {
		return poule.findMatch(playerA, playerB);
	}
			
	/**
	 * update poule-table with match-result
	 */
	public void add(Match match) {
		Player playerA = match.getPlayerA();
		Player playerB = match.getPlayerB();
				
		// entries already present: remove them from set, update en add again.
		PouleEntry entryA = get(playerA);
		PouleEntry entryB = get(playerB);		
				
		if (entryA != null) {
			remove(entryA);
		} else {
			entryA = new PouleEntry(playerA, this);
		}
		if (entryB != null) {
			remove(entryB);
		} else {
			entryB = new PouleEntry(playerB, this);
		}
		
		// update entries
		entryA.setFramesWon(entryA.getFramesWon() + match.getScoreA());
		Break currentA = entryA.getBrk();
		Break matchHighestA = match.getHighestBreak(playerA);//match.getHighestBreakPlayerA();
		if (currentA == null || (matchHighestA != null &&  (matchHighestA.getValue() > currentA.getValue()))) {
			entryA.setBrk(matchHighestA);
		}
				
		entryB.setFramesWon(entryB.getFramesWon() + match.getScoreB());
		Break currentB = entryB.getBrk();
		Break matchHighestB = match.getHighestBreak(playerB);//match.getHighestBreakPlayerB();
		if (currentB == null || (matchHighestB != null &&  (matchHighestB.getValue() > currentB.getValue()))) {
			entryB.setBrk(matchHighestB);
		}
		
		entryA.setWinner(poule.isWinner(entryA.getPlayer()));
		entryB.setWinner(poule.isWinner(entryB.getPlayer()));
		
		// add entries
		super.add(entryA);
		super.add(entryB);						
	}
	
	public PouleEntry get(Player player) {

		Iterator<PouleEntry> it = iterator();
		while (it.hasNext()) {
			PouleEntry e = it.next();
			if (e.getPlayer().equals(player)) {
				return e;
			}
		}
		
		return null;
	}
	
	public boolean contains(Object entry) {
		return super.contains(entry);
	}
	
}
