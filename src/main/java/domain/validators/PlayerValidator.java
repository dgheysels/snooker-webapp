package domain.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import domain.Player;

public class PlayerValidator implements Validator {

	
	public boolean supports(Class clazz) {
		return Player.class.equals(clazz);
	}

	
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "empty");
		
		
		
	}
	

}
