package domain;


public class KnockoutMatch extends Match implements Comparable<KnockoutMatch> {
	
	public enum Type {
		FINAL(2) {
			public int getPointsWinner(SubTournament event) {
				return event.getPointsWinnerF();
			}
			public int getPointsLoser(SubTournament event) {
				return event.getPointsLoserF();				
			}
		},
		SEMI_FINAL(4) {
			public int getPointsWinner(SubTournament event) {
				return event.getPointsLoserF();
			}
			public int getPointsLoser(SubTournament event) {
				return event.getPointsLoser2F();
			}
		},
		QUARTER_FINAL(8) {
			public int getPointsWinner(SubTournament event) {
				return event.getPointsLoser2F();
			}
			public int getPointsLoser(SubTournament event) {
				return event.getPointsLoser4F();				
			}
		},
		EIGHT_FINAL(16) {
			public int getPointsWinner(SubTournament event) {
				return event.getPointsLoser4F();
			}
			public int getPointsLoser(SubTournament event) {
				return event.getPointsLoser8F();
			}
		},
		SIXTEENTH_FINAL(32) {
			public int getPointsWinner(SubTournament event) {
				return event.getPointsLoser8F();
			}
			public int getPointsLoser(SubTournament event) {
				return event.getPointsLoser16F();				
			}
		};
		
		private Type(int value) {
			this.value = value;
		}
		
		private int value;

		public int getValue() { return value; }
		public void setValue(int value) { this.value = value; }
		
		public abstract int getPointsWinner(SubTournament event);
		public abstract int getPointsLoser(SubTournament event);
		
	}
	
	private int position;
	private KnockoutRound knockoutRound;
	
	public KnockoutMatch() {}

	public int getPosition() { return position; }
	public void setPosition(int position) { this.position = position; }

	public KnockoutRound getKnockoutRound() { return knockoutRound; }
	public void setKnockoutRound(KnockoutRound knockoutRound) { this.knockoutRound = knockoutRound; }
	
	public int getPointsLoser(SubTournament event) {
		Player loser = getLoser();
		int scoreLoser = getScore(loser);
		if (scoreLoser == -1) {
			if (this.isFirstMatchForPlayer(loser)) {
				return -1;
			} else {
				return -getType().getPointsLoser(event);
			}
		} else {
			return getType().getPointsLoser(event) + (scoreLoser * event.getPointsFrameKnockout());
		}
	}
	
	public int getPointsWinner(SubTournament event) {
		return getType().getPointsWinner(event);
	}
	
	public boolean isFirstMatchForPlayer(Player player) {
		KnockoutMatch match = knockoutRound.getFirstMatchForPlayer(player);
		return this == match;
	}
	
	public Type getType() {
		for (Type t : Type.values()) {
			if (getPosition() < t.getValue()) {
				return t;
			}
		}
		
		return null;
	}

	
	public int compareTo(KnockoutMatch o) {		
		return o.getPosition() - this.getPosition();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof KnockoutMatch) {
			KnockoutMatch that = (KnockoutMatch)other;
			return super.equals(that) && this.getPosition() == that.getPosition();
		} else {
			return false;
		}
	}
}
