package domain;

import java.util.HashSet;
import java.util.Set;

public abstract class Match {

	protected int id;
	protected Player playerA;
	protected Player playerB;
	protected int scoreA;
	protected int scoreB;	
	protected Set<Break> breaks = new HashSet<Break>();

	protected Match() {}
	
	protected Match(int id, Player playerA, Player playerB) {
		this.id = id;
		this.playerA = playerA;
		this.playerB = playerB;
	}
	
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }
	
	public Player getPlayerA() { return playerA; }
	public void setPlayerA(Player playerA) { this.playerA = playerA; }
	
	public Player getPlayerB() { return playerB; }
	public void setPlayerB(Player playerB) { this.playerB = playerB; }

	public int getScoreA() { return scoreA; }
	public void setScoreA(int scoreA) { this.scoreA = scoreA; }

	public int getScoreB() { return scoreB; }
	public void setScoreB(int scoreB) { this.scoreB = scoreB; }

	public Set<Break> getBreaks() { return breaks; }
	public void setBreaks(Set<Break> breaks) { this.breaks = breaks; }
		
	public Break getHighestBreakPlayerA() { return getHighestBreak(playerA); }
	public Break getHighestBreakPlayerB() { return getHighestBreak(playerB); }
		
	/**
	 * get highest break for a particular player in this match 
	 */
	public Break getHighestBreak(Player player) {
		Break highest = new Break();
		for (Break b : getBreaks()) {
			if (b.getPlayer().equals(player) && (highest == null || highest.getValue() < b.getValue())) {
				highest = b;
			}
		}
		
		return highest;
	}
	
	public Player getWinner() {
		if (scoreA > scoreB) {
			return getPlayerA();
		} else {
			return getPlayerB();
		}
	}
	
	public Player getLoser() {
		if (getPlayerA().equals(getWinner())) {
			return getPlayerB();
		} else {
			return getPlayerA();
		}
	}
	
	public boolean isPlayed() {
		return (scoreA != -1) || (scoreB != -1);
	}
	
	public int getScore(Player player) {
		if (player.equals(getPlayerA())) {
			return scoreA;
		} else {
			return scoreB;
		}
	}
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof Match) {
			Match that = (Match)other;						
			return (this.getPlayerA().equals(that.getPlayerA()) && 
					this.getPlayerB().equals(that.getPlayerB()) &&
					this.getScoreA() == that.getScoreA() &&
					this.getScoreB() == that.getScoreB());
		} else {
			return false;
		}
	}
}
