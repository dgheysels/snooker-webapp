package domain;

public class PouleMatch extends Match {
	
	private Poule poule;
		
	public PouleMatch() {}
	
	public PouleMatch(int id, Player playerA, Player playerB, Poule poule) {
		super(id, playerA, playerB);
		this.poule = poule;
	}

	public Poule getPoule() { return poule; }
	public void setPoule(Poule poule) { this.poule = poule; } 		
}
