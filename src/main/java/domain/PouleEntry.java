package domain;


/**
 * This class represents an entry in a pouleTable.  
 * Each entry consists of a Player, number of frames won and a highest break.
 * There exist a total ordering between the PouleEntries.
 * 
 * @author Dimitri Gheysels
 */
public class PouleEntry implements Comparable<PouleEntry> {

	private Player player;
	private int framesWon;
	private Break brk;
	private boolean winner;

	private PouleTable pouleTable;

	public PouleEntry(Player player, PouleTable pouleTable) {
		setPlayer(player);
		setFramesWon(0);
		setBrk(null);
		setPouleTable(pouleTable);
	}

	/*
	 * This entry is listed higher in the poule if
	 *  1) player has won more frames,
	 *  2) if draw, match between both players is won by this player
	 *  3) if draw, this player has a higher break in the poule
	 *  4) if draw, then undetermined (winner has to be selected manually)
	 */
	public int compareTo(PouleEntry other) {
		if (!this.equals(other)) {
			if (this.framesWon == other.framesWon) {
				// equal number of frames won: result of match between players decides
				PouleMatch match = pouleTable.findMatch(this.player, other.getPlayer());
	
				if (match == null || (match.getScoreA() == match.getScoreB())) {
					// no match is played yet between players or
					// match between players is draw: highest break decides
					
					//TODO use compareTo and Equals for Breaks !
					int breakA = 0;
					int breakB = 0;
	
					if (this.brk != null) {
						breakA = this.brk.getValue();
					}
					if (other.getBrk() != null) {
						breakB = other.getBrk().getValue();
					}
	
					if (breakA == breakB) {
						// equal breaks, then name makes difference ... (undetermined)
						return this.player.compareTo(other.getPlayer());					
					} else {
						return breakB - breakA;
					}
					
				} else {									
					// scoreA > scoreB --> entry of playerA wins, else playerB wins
					// scoreA < scoreB --> entry of playerB wins, else playerA wins
					if ( (match.getScoreA() > match.getScoreB() && this.player.equals(match.getPlayerA())) ||
						 (match.getScoreB() > match.getScoreA() && this.player.equals(match.getPlayerB()))) {
						return -1; 
					} else {
						return 1; 
					}
				}
			} else {
				return other.getFramesWon() - this.framesWon;
			}
		} else {
			return 0;
		}
	} 


	/**
	 * Two entries are considered the same if:
	 *  their players are the same
	 *  frames-won are the same
	 *  breaks are the same
	 */
	@Override
	public boolean equals(Object o) {		
		if (o != null && o instanceof PouleEntry) {
			PouleEntry that = (PouleEntry)o;
			if (this == that) {
				return true;
			} else {
				return 	(this.getPlayer().equals(that.getPlayer()) &&
						((this.getBrk() != null && this.getBrk().equals(that.getBrk())) || (this.brk == null && that.getBrk() == null)) && 
						(this.getFramesWon() == that.getFramesWon()));
			}
		} else {
			return false;
		}
	}

	public Player getPlayer() { return player; }
	public void setPlayer(Player player) { this.player = player; }

	public int getFramesWon() { return framesWon; }
	public void setFramesWon(int framesWon) { this.framesWon = framesWon; }

	public Break getBrk() { return brk; }
	public void setBrk(Break brk) { this.brk = brk; }

	public PouleTable getPouleTable() { return pouleTable; }
	public void setPouleTable(PouleTable pouleTable) { this.pouleTable = pouleTable; }

	public boolean isWinner() { return winner; }
	public void setWinner(boolean winner) { this.winner = winner; }
}
