package domain;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import domain.visitor.AbstractRankingVisitor;

public class SingleMatchPeriod extends TournamentEvent {

	private Set<SingleMatch> matches = new TreeSet<SingleMatch>();
	
	public SingleMatchPeriod() {}

	public Set<SingleMatch> getMatches() { return matches; }
	public void setMatches(Set<SingleMatch> matches) { this.matches = matches; }
	
	@SuppressWarnings(value="all")
	public List<SingleMatch> getMatchesA() { 
		return (List<SingleMatch>) CollectionUtils.select(matches, new Predicate() {
			
			public boolean evaluate(Object object) {
				SingleMatch match = (SingleMatch)object;
				return 
					"A".equalsIgnoreCase(match.getPlayerA().getGrade(tournament)) &&
					"A".equalsIgnoreCase(match.getPlayerB().getGrade(tournament));  
			}
		});					
	}
	
	@SuppressWarnings(value="all")
	public List<SingleMatch> getMatchesB() {
		
		return (List<SingleMatch>) CollectionUtils.select(matches, new Predicate() {
			
			public boolean evaluate(Object object) {
				SingleMatch match = (SingleMatch)object;
				return 
					"B".equalsIgnoreCase(match.getPlayerA().getGrade(tournament)) &&
					"B".equalsIgnoreCase(match.getPlayerB().getGrade(tournament)); 
			}
		});					
	}
	
	public void addMatch(SingleMatch match) {
		if (match.getPeriod() == null) {
			match.setPeriod(this);
		}
		
		getMatches().add(match);
	}
	
	public void accept(AbstractRankingVisitor visitor) {
		visitor.setEvent(this);
		for (SingleMatch match : getMatches()) {
			visitor.visitSingleMatch(match);
		}
	}
	
	/**
	 * retrieve highest break of a particular player in this tournament-event
	 */
	public Break getHighestBreak(Player player) {
		SingleMatch match = findMatch(player);
		return match.getHighestBreak(player);
	}
		
	private SingleMatch findMatch(Player player) {
		for (SingleMatch match : getMatches()) {
			if (match.getPlayerA().equals(player) || match.getPlayerB().equals(player)) {
				return match;
			}
		}
		return null;
	}
}
