package domain;

import java.util.Date;

public class Notice {
	private int id;
	private Date date;
	private String title;
	private String contents;
	
	public Notice() {}

	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public Date getDate() { return date; }
	public void setDate(Date date) { this.date = date; }

	public String getContents() { return contents; }
	public void setContents(String contents) { this.contents = contents; }

	public String getTitle() { return title; }
	public void setTitle(String title) { this.title = title; }	
}
