package domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Meeting {

	private int id;
	private Date date;
	private String name;
	private byte[] report;
		
	public Meeting() {}

	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public Date getDate() { return date; }
	public void setDate(Date date) { this.date = date; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }


	public byte[] getReport() {
		return report;
	}

	public void setReport(byte[] report) {
		this.report = report;
	}
}
