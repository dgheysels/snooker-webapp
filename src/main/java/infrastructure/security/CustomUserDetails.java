package infrastructure.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.userdetails.User;

import domain.Player;

public class CustomUserDetails extends User {

	private int userId;
	
	public CustomUserDetails(Player p) {
		super(p.getName(), p.getPassword(), true, true, true, true, getAuthorities(p));
		this.userId = p.getId();
	}
	
	private static GrantedAuthority[] getAuthorities(Player p) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		authorities.add(new GrantedAuthorityImpl(p.getRole().getName()));
		
		return authorities.toArray(new GrantedAuthority[] {});
	}

	public int getUserId() {
		return userId;
	}
}
