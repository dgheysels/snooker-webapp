package service;

import java.util.List;

import domain.InterclubDivision;
import domain.InterclubTeam;

public interface IInterclubService {

	public List<InterclubDivision> getInterclubDivisions();
	public InterclubDivision getInterclubDivision(int id);
	
	public List<InterclubTeam> getInterclubTeams();
	public InterclubTeam getInterclubTeam(int id);
	
	
	public List<InterclubDivision> getOverview();
	public List<InterclubDivision> getFullOverview();
	
	public void saveInterclubTeam(InterclubTeam team);
	public void deleteInterclubTeam(InterclubTeam team);
}
