package service;

import org.springframework.security.userdetails.UserDetailsService;

import domain.Player;

public interface IAuthenticationService extends UserDetailsService {

	public boolean isAuthenticated();
	public Player getAuthenticatedUser();
	public boolean hasRole(String role);
		
	public int getUserId();
	
}
