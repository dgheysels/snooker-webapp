package service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;

import domain.Snookerclub;

@Service("snookerclubService")
public class SnookerclubService extends HibernateDaoSupport implements ISnookerclubService {

	@Autowired
	public SnookerclubService(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public List<Snookerclub> getSnookerclubs() {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.find("FROM Snookerclub");
	}
	
	public Snookerclub getSnookerclub(int id) {
		HibernateTemplate ht = getHibernateTemplate();
		return (Snookerclub)ht.load(Snookerclub.class, id);
	}
	
}
