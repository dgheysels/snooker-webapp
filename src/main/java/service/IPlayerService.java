package service;

import java.util.List;

import domain.Player;
import domain.Role;

public interface IPlayerService {
	
	public List<Player> getPlayers();
	public List<Player> getAllPlayers();
	public List<Player> getManagement();
	
	public Player getPlayer(int id);
	public Player getPlayer(String username);
	
	public void addPlayer(Player player);
	public void savePlayer(Player player, boolean generatePassword);
	public void saveProfile(Player player, boolean sendEmail);
	
	public void inactivatePlayer(int playerId);
	public void activatePlayer(int playerId);
	
	public List<Role> getRoles();
	public Role getRole(int id);
	public Role getRole(String name);
		
}
