package service;

import java.util.List;

import domain.Player;
import domain.SingleMatchPeriod;
import domain.SubTournament;
import domain.Tournament;
import domain.TournamentPlayer;
import domain.TournamentType;

public interface ITournamentService {

	public Tournament getActiveTournament();
	public List<Tournament> getAllTournaments();
	public Tournament getTournament(int tournamentId);
	public void closeActiveTournament();
	public void saveTournament(Tournament tournament);
	
	
	public TournamentPlayer findSubscription(Tournament tournament, Player player);
	
	public SubTournament getSubTournament(int subTournamentId);
	public void saveSubTournament(SubTournament subTournament);
	
	public SingleMatchPeriod getSingleMatchPeriod(int periodId);
	public void saveSingleMatchPeriod(SingleMatchPeriod period);	
	
	public List<TournamentType> getAllTournamentTypes();
	
	public void disqualifyPlayer(Tournament tournament, Player player);
}
