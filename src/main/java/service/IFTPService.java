package service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface IFTPService {

	public boolean upload(MultipartFile file, String location) throws IOException;
	public boolean delete(MultipartFile file, String location) throws IOException;
}
