package service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import domain.Meeting;

public interface IMeetingService {

	public List<Meeting> getAllMeetings();
	public Meeting getMeeting(int meetingId);
	public void saveMeeting(Meeting meeting);
//	public void closeMeeting(Meeting meeting, MultipartFile report) throws Exception;
}
