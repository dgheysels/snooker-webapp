package service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import service.IPasswordGenerator;

@Service
public class PasswordGenerator implements IPasswordGenerator {


	public PasswordGenerator() {}
	
	public String generatePassword() {
		String pwd = StringUtils.EMPTY;
		
		for (int i=0; i<8; i++) {
			char c = (char)((int)(Math.random() * 25) + 97);
			pwd += c;
		}
		
		return pwd;
		
	}
	
}
