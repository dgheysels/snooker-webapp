package service.impl;

import java.io.IOException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import service.IFTPService;

@Service("ftpService")
public class FTPService implements IFTPService {
	
	//private static final Logger log = Logger.getLogger(MailService.class);
	private static final String FTP_SUBDIR = "httpdocs/";
	private static final String USERNAME = "testdimitri";
	private static final String PASSWORD = "anaconda";
	
	public FTPService() {}
	
	
	public boolean upload(MultipartFile file, String location) throws IOException {
		FTPClient client = new FTPClient();
		client.connect("www.thenewcrucible.be");
		if (client.isConnected() &&
				client.login(USERNAME, PASSWORD)) {
			
			client.setFileType(FTPClient.BINARY_FILE_TYPE);
			client.storeFile(FTP_SUBDIR + location, file.getInputStream());

			if (FTPReply.isPositiveCompletion(client.getReplyCode())) {
				client.logout();
				client.disconnect();
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public boolean delete(MultipartFile file, String location) throws IOException {
		FTPClient client = new FTPClient();
		client.connect("www.thenewcrucible.be");
		if (client.isConnected() && client.login(USERNAME, PASSWORD)) {
			client.setFileType(FTPClient.BINARY_FILE_TYPE);
			return client.deleteFile(FTP_SUBDIR + location + file.getOriginalFilename());
		} else {
			return false;
		}
	}
}
