package service.impl;

import infrastructure.security.CustomUserDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import service.IAuthenticationService;
import service.IPlayerService;
import domain.Player;

@Service("authenticationService")
public class AuthenticationService implements IAuthenticationService {

	
	private IPlayerService playerService;
	
	@Autowired
	public AuthenticationService(IPlayerService playerService) {
		this.playerService = playerService;
	}
	
	public boolean isAuthenticated() {
		return SecurityContextHolder.getContext().getAuthentication() != null;
	}
	
	public int getUserId() {
		if (isAuthenticated()) {
			return ((CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
		} else {
			return -1;
		}
	}
	
	public Player getAuthenticatedUser() {
		return playerService.getPlayer(getUserId());
	}
	
	public boolean hasRole(String role) {
		if (isAuthenticated()) {
			GrantedAuthority[] authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
			for (GrantedAuthority authority : authorities) {
				if (authority.getAuthority().equalsIgnoreCase(role)) {
					return true;
				}
			}
			return false;
		} else {
			return false;
		}
	}
	
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException, DataAccessException {

		Player p = playerService.getPlayer(username);
		
		if ( p != null ) {
			//UserDetails user = new User(p.getName(), p.getPassword(), true, true, true, true, getAuthorities(p));
			CustomUserDetails user = new CustomUserDetails(p); 
			
			return user;
		}
		else {
			throw new UsernameNotFoundException("user " + username + " doesn't exist");
		}	
	}
	
}
