package service.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.ISingleMatchPeriodRepository;
import repository.ISubTournamentRepository;
import repository.ITournamentRepository;
import service.ITournamentService;
import domain.Player;
import domain.SingleMatchPeriod;
import domain.SubTournament;
import domain.Tournament;
import domain.TournamentPlayer;
import domain.TournamentType;

@Service("tournamentService")
public class TournamentService extends HibernateDaoSupport implements ITournamentService {
	
	@Autowired private ITournamentRepository tournamentRepository;
	@Autowired private ISubTournamentRepository subTournamentRepository;
	@Autowired private ISingleMatchPeriodRepository singleMatchPeriodRepository;
	
	
	@Autowired
	public TournamentService(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);	
	}
	
	public Tournament getActiveTournament() {
		return tournamentRepository.getActiveTournament();
	}
	
	public List<Tournament> getAllTournaments() {
		return tournamentRepository.getAllTournaments();
		
	}
	
	public Tournament getTournament(int tournamentId) {
		return tournamentRepository.getTournament(tournamentId);		
	}
	
	@Transactional
	public void closeActiveTournament() {
		Tournament activeTournament = getActiveTournament();
		
		if (activeTournament != null) {
			activeTournament.setActive(false);
			tournamentRepository.saveTournament(activeTournament);
		}	
	}
	
	@Transactional
	public void saveTournament(Tournament tournament) {
		tournamentRepository.saveTournament(tournament);			
	}
		
	public TournamentPlayer findSubscription(Tournament tournament, Player player) {
		return tournamentRepository.getSubscription(tournament, player);
	}
	
	/*public void updateSubscription(TournamentPlayer subscription) {
		tournamentRepository.updateSubscription(subscription);
	}*/
	
	@Transactional
	public void disqualifyPlayer(Tournament tournament, Player player) {
		TournamentPlayer tp = findSubscription(tournament, player);
		tp.setDisqualified(true);
		//updateSubscription(tp);
		tournamentRepository.updateSubscription(tp);
	}
	
	/******************* SUB TOURNAMENTS ***************************/
	public SubTournament getSubTournament(int subTournamentId) {
		return subTournamentRepository.getSubTournament(subTournamentId);
	}
	
	@Transactional
	public void saveSubTournament(SubTournament subTournament) {
		subTournamentRepository.saveSubTournament(subTournament);
	}
	
	
	
	/******************* SINGLE MATCH PERIODS ***********************/	
	public SingleMatchPeriod getSingleMatchPeriod(int periodId) {
		return singleMatchPeriodRepository.getPeriod(periodId);
	}
	
	
	@Transactional
	public void saveSingleMatchPeriod(SingleMatchPeriod period) {
		singleMatchPeriodRepository.savePeriod(period);
	}
	
	public List<TournamentType> getAllTournamentTypes() {
		return tournamentRepository.getAllTournamentTypes();
	}
	
}
