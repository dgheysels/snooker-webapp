package service.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import service.IMailService;
import domain.Player;

@Service("mailService")
public class MailService implements IMailService{

	private static final Logger log = Logger.getLogger(MailService.class);
	private MailSender mailSender;
	
	@Autowired
	public MailService(MailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void sendWelcomeMail(Player player) {
		if (!StringUtils.isEmpty(player.getEmail())) {
			StringBuilder body = new StringBuilder();
			body.append(player.getName());
			body.append("\n\nWelkom bij The New Crucible !");
			body.append("\n\nNa enkele maanden van hard zwoegen en overuren kloppen, staat de eerste versie van de website online.  De URL is  ' www.thenewcrucible.be '.");
			body.append("\nDe bedoeling van deze website is momenteel om: ");
			body.append("\n\t - onze club meer in de schijnwerpers te zetten,");
			body.append("\n\t - communicatie verzorgen naar alle leden: verslagen van bestuursvergaderingen, evenementen en andere nieuwsfeiten zullen op de website gepubliceerd worden,");
			body.append("\n\t - informatie opvragen van alle leden zoals contactgegevens, interclub, ...");
			body.append("\n\t - resultaten bekijken van het clubkampioenschap, Davy's Cup en eventueel andere kampioenschappen,");
			body.append("\n\t - en tenslotte een meerwaarde te geven voor onze sponsors.");
			body.append("\n\n");
			body.append("\nVooraleer de contactgegevens van de leden opgevraagd kunnen worden, dient er ingelogd te worden.  Dit is om in zekere mate de privacy te bewaren en niet zomaar vertrouwelijke data de wereld in te sturen.  ");
			body.append("Jouw individuele gebruikersnaam en paswoord worden hieronder vermeld.  Het is aangeraden om je password te wijzigen nadat je voor de eerste keer ingelogd bent.");
			body.append("\nDit kun je doen door op je naam te klikken rechtboven op het scherm.");
			body.append("\n\ngebruikersnaam: " + player.getName());
			body.append("\npaswoord: " + player.getPassword());
			body.append("\n\nVoor problemen, opmerkingen of andere opbouwende kritiek kun je altijd emailen naar dimitrigheysels@yahoo.com .");
						
			SimpleMailMessage message = constructBasicMessage();
			message.setTo(player.getEmail());
			message.setSubject("[The New Crucible] Welkom !");
			message.setText(body.toString());
			
			log.info("sending welcome email to " + player.getEmail());			
			try {
				mailSender.send(message);
			} catch (MailException ex) {
				log.info("sending welcome email failed");
			}
			log.info("sending welcome email to succeeded");
		} else {
			log.info("unable to send welcome email because of empty email-adres");
		}
	}
	
	public void sendPwdChangedMail(Player player) {
		if (!StringUtils.isEmpty(player.getEmail())) {
			StringBuilder body = new StringBuilder();
			body.append(player.getName());
			body.append("\n\nJe paswoord is gewijzigd.  Dit zijn jouw nieuwe gegevens om in te loggen:  ");
			body.append("\n\nusername: " + player.getName());
			body.append("\npassword: " + player.getPassword());
			
			
			SimpleMailMessage message = new SimpleMailMessage();			
			message.setFrom("dimitri.gheysels@telenet.be");			
			message.setTo(player.getEmail());
			message.setSubject("[The New Crucible] Passwoord gewijzigd");
			message.setText(body.toString());
			
			log.info("sending password-changed email to " + message.getTo());			
			try {
				mailSender.send(message);
			} catch (MailException ex) {
				log.info("sending password-changed email failed");
			}
			log.info("sending password-changed email succeeded");
		} else {
			log.info("unable to send password-changed email because of empty email-adres");
		}			
	}
	
	private SimpleMailMessage constructBasicMessage() {
		SimpleMailMessage message = new SimpleMailMessage();			
		message.setFrom("dimitri.gheysels@telenet.be");
		
		return message;
	}
}
