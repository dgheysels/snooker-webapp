package service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.IPlayerRepository;
import repository.IRoleRepository;
import service.IMailService;
import service.IPasswordGenerator;
import service.IPlayerService;
import domain.Player;
import domain.Role;

@Service("playerService")
public class PlayerService implements IPlayerService {
		
	@Autowired private IPlayerRepository playerRepository;
	@Autowired private IRoleRepository roleRepository;
	@Autowired private IPasswordGenerator passwordGenerator;
	@Autowired private IMailService mailService;
	
	@Autowired
	public PlayerService(IPlayerRepository playerRepository, IRoleRepository roleRepository) {
		this.playerRepository = playerRepository;
		this.roleRepository = roleRepository;		
	}
	
	public Player getPlayer(final int playerId) {
		return playerRepository.getPlayer(playerId);
	}

	public Player getPlayer(final String playerName) {
		return playerRepository.getPlayer(playerName);
	}
	
	public List<Player> getPlayers() {
		return playerRepository.getPlayers();
	}
	
	public List<Player> getAllPlayers() {
		return playerRepository.getAllPlayers();
	}
	
	public List<Player> getManagement() {
		return playerRepository.getManagement();
	}

	/**
	 * Add a new player
	 * A password is generated and a welcome-email is sent
	 */
	@Transactional
	public void addPlayer(Player player) {
		
		// generate password
		String pwd = passwordGenerator.generatePassword();
		player.setPassword(pwd);
			
		// add player
		playerRepository.savePlayer(player);
						
		// send email
		mailService.sendWelcomeMail(player);		
		
	}
	
	/**
	 * Update a player
	 * If a new password is generated, send an email
	 */
	@Transactional
	public void savePlayer(Player player, boolean generatePassword) {
		if (generatePassword) {
			String pwd = passwordGenerator.generatePassword();
			player.setPassword(pwd);
			
			playerRepository.savePlayer(player);
			
			// send email
			mailService.sendPwdChangedMail(player);
			
		} else {
			playerRepository.savePlayer(player);
		}
	}
	/**
	 * Update player's profile
	 * Sent an email if password is changed
	 */
	@Transactional
	public void saveProfile(Player player, boolean sendEmail) {
		playerRepository.savePlayer(player);
		
		if (sendEmail) {
			mailService.sendPwdChangedMail(player);
		}
	}
	
	public List<Role> getRoles() {
		return roleRepository.getRoles();
	}
	
	public Role getRole(int id) {
		return roleRepository.getRole(id);
	}
	
	public Role getRole(final String name) {
		return roleRepository.getRole(name);
	}
	
	@Transactional
	public void inactivatePlayer(int playerId) {		
		Player player = getPlayer(playerId);
		player.setActive(false);
		savePlayer(player, false);		
	}
	
	@Transactional
	public void activatePlayer(int playerId) {
		Player player = getPlayer(playerId);
		player.setActive(true);
		savePlayer(player, false);
	}
	
}
