package service.impl;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.INoticeRepository;
import service.INoticeService;
import domain.Notice;

@Service("noticeService")
public class NoticeService implements INoticeService {

	private INoticeRepository noticeRepository;
	
	@Autowired
	public NoticeService(INoticeRepository noticeRepository) {
		this.noticeRepository = noticeRepository;
	}
	
	public List<Notice> getRecentNotices() {
		DateTime recent = new DateTime().minusDays(14);
		return noticeRepository.getAllNotices(recent.toDate());
	}
	
	
	public List<Notice> getRecentNotices(int number) {
		return noticeRepository.getLastNotices(number);
	}
	
	
	
	@Transactional
	public void saveNotice(Notice n) {
		noticeRepository.saveNotice(n);
	}

}
