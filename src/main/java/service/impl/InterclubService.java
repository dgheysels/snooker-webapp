package service.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.IInterclubDivisionRepository;
import repository.IInterclubTeamRepository;
import service.IInterclubService;
import service.IPlayerService;
import domain.InterclubDivision;
import domain.InterclubTeam;
import domain.Player;

@Service("interclubService")
public class InterclubService extends HibernateDaoSupport implements IInterclubService {
	
	@Autowired private IPlayerService playerService;
	@Autowired private IInterclubDivisionRepository interclubDivisionRepository;
	@Autowired private IInterclubTeamRepository interclubTeamRepository;
	
	@Autowired
	public InterclubService(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	public List<InterclubDivision> getInterclubDivisions() {
		return interclubDivisionRepository.getAllInterclubDivisions();
	}
	
	public InterclubDivision getInterclubDivision(int divisionId) {
		return interclubDivisionRepository.getInterclubDivision(divisionId);		
	}
	
	public List<InterclubTeam> getInterclubTeams() {
		return interclubTeamRepository.getInterclubTeams();		
	}
	
	public InterclubTeam getInterclubTeam(int teamId) {
		return interclubTeamRepository.getInterclubTeam(teamId);		
	}
	
	public List<InterclubDivision> getOverview() {
		return interclubDivisionRepository.getInterclubDivisions();
	}
	
	public List<InterclubDivision> getFullOverview() {
		return interclubDivisionRepository.getAllInterclubDivisions();
	}
	
	@Transactional
	public void saveInterclubTeam(InterclubTeam team) {
		interclubTeamRepository.saveInterclubTeam(team);
	}
	
	@Transactional
	public void deleteInterclubTeam(InterclubTeam team) {
		for (Player p : team.getPlayers()) {
			p.setInterclubTeam(null);
			p.setInterclubRank(-1);
			playerService.savePlayer(p, false);
		}
		
		interclubTeamRepository.deleteInterclubTeam(team);
	
	}

}
