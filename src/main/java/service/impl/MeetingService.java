package service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import repository.IMeetingRepository;
import service.IFTPService;
import service.IMeetingService;
import domain.Meeting;

@Service("meetingService")
public class MeetingService implements IMeetingService {

	private static final Logger log = Logger.getLogger(MeetingService.class);
	private static final String LOCATION = "docs/reports/";
	
	@Autowired private IMeetingRepository meetingRepository;
	@Autowired private IFTPService ftpService;
	
	public List<Meeting> getAllMeetings() {
		return meetingRepository.getAllMeetings();
	}
	
	public Meeting getMeeting(int meetingId) {
		return meetingRepository.getMeeting(meetingId);
	}
	
	@Transactional
	public void saveMeeting(Meeting meeting) {
		meetingRepository.saveMeeting(meeting);
	}
	
//	@Transactional
//	public void closeMeeting(Meeting meeting, MultipartFile report) throws Exception {
//		log.info("meeting is being closed...");
//		meeting.setClosed(true);
//		meeting.setReportURL(LOCATION + report.getOriginalFilename());
//			
//		if (ftpService.upload(report, meeting.getReportURL())) {
//			log.info("report is successfully uploaded");
//			try {				
//				saveMeeting(meeting);
//				log.info("meeting updated");
//			} catch(Exception ex) {
//				log.error(ex.getMessage());				
//				if (ftpService.delete(report, meeting.getReportURL())) {
//					log.info("report is removed again");
//				} else {
//					log.info("report could not be removed again");
//				}
//				
//				throw ex;
//			}
//		} else {
//			log.info("an error happened during upload");
//		}
//	}
}
