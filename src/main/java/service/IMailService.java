package service;

import domain.Player;

public interface IMailService {

	public void sendWelcomeMail(Player p);
	public void sendPwdChangedMail(Player p);
}
