package service;

import java.util.List;

import domain.Notice;

public interface INoticeService {

	public List<Notice> getRecentNotices();
	public List<Notice> getRecentNotices(int number);
	public void saveNotice(Notice notice);
}
