package service;

import java.util.List;

import domain.Snookerclub;

public interface ISnookerclubService {

	public List<Snookerclub> getSnookerclubs();
	public Snookerclub getSnookerclub(int id);
}
