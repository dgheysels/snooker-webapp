package test;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.orm.hibernate3.SessionFactoryUtils;

import controller.newNoticeController;

import domain.Player;
import domain.SubTournament;
import domain.Tournament;
import domain.TournamentRanking;
import domain.TournamentType;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		Session s = sf.openSession();
		
			
		Player p = (Player)s.load(Player.class, new Integer(1));
		
		System.out.println(p.getName());
		System.out.println(p.getPhoto());
		
		

	}

}
