package controller;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import service.INoticeService;
import domain.Notice;

@Controller
@RequestMapping("/newNotice.htm")
public class newNoticeController {

	@Autowired
	private INoticeService noticeService;
	
	@ModelAttribute("notice")
	public Notice getNotice() {
		return new Notice();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/newNotice.htm")
	public ModelAndView requestNewNotice() {
		return new ModelAndView("newNotice");
	}
	
	@RequestMapping(method=RequestMethod.POST, params="saveNotice")
	public ModelAndView submitSaveNotice(@ModelAttribute("notice") Notice notice) {
		notice.setDate(new DateTime().toDate());
		noticeService.saveNotice(notice);
		
		return new ModelAndView(new RedirectView("main.htm"));
	}
}
