package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import domain.Player;
import domain.Tournament;

import service.IPlayerService;
import service.ITournamentService;

@Controller
@RequestMapping({"/openTournament.htm", "/disqualify.htm"})
public class OpenTournamentController {

	@Autowired private ITournamentService tournamentService;
	@Autowired private IPlayerService playerService;
	
	@RequestMapping(method=RequestMethod.GET, value="/openTournament.htm")
	public ModelAndView requestOpenTournament(@RequestParam("id") int id, ModelMap model) {
		
		model.addAttribute("tournament", tournamentService.getTournament(id));

		return new ModelAndView("openTournament", model);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/disqualify.htm")
	public ModelAndView requestDisqualification(@RequestParam("tournamentId") int tournamentId, @RequestParam("playerId") int playerId, ModelMap model) {
		Player player = playerService.getPlayer(playerId);
		Tournament tournament = tournamentService.getTournament(tournamentId);
		
		tournamentService.disqualifyPlayer(tournament, player);
		
		return new ModelAndView(new RedirectView("viewRanking.htm?tournamentId=" + tournamentId));
	}
	
}
