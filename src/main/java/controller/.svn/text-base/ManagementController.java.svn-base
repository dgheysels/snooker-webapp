package controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import domain.Meeting;

import service.IMeetingService;
import service.IPlayerService;

@Controller
@RequestMapping({"/management.htm", "/getReport.htm"})
public class ManagementController {

	@Autowired private IPlayerService playerService;
	@Autowired private IMeetingService meetingService;
	
	@RequestMapping(method=RequestMethod.GET, value="/management.htm")
	public String requestManagement(ModelMap model) {		
		model.addAttribute("management", playerService.getManagement());		
		model.addAttribute("meetings", meetingService.getAllMeetings());
		
		return "management";
	}	
	
	@RequestMapping(method=RequestMethod.GET, value="/getReport.htm")
	public ModelAndView getReport(@RequestParam("meetingId") int meetingId, HttpServletResponse response) throws Exception {
		Meeting meeting = meetingService.getMeeting(meetingId);
		byte[] report = meeting.getReport();

		if (report != null) {
			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Length", String.valueOf(report.length));

			BufferedInputStream input = null;
			BufferedOutputStream output = null;
			
			try {
				// Open streams.				
				ByteArrayInputStream bais = new ByteArrayInputStream(report);
				input = new BufferedInputStream(bais);
				output = new BufferedOutputStream(response.getOutputStream());

				// Write file contents to response.
				byte[] buffer = new byte[report.length];
				int length;
				while ((length = input.read(buffer)) > 0) {
					output.write(buffer, 0, length);
				}

				output.flush();
			} finally {	            
				output.close();
				input.close();
			}

		}
		return null;

	}
}
