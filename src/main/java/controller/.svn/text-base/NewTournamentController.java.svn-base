package controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import service.IPlayerService;
import service.ITournamentService;
import domain.KnockoutRound;
import domain.Player;
import domain.SingleMatchPeriod;
import domain.SubTournament;
import domain.Tournament;
import domain.TournamentPlayer;

@Controller
@RequestMapping("/newTournament.htm")
@SessionAttributes("tournament")
public class NewTournamentController {

	@Autowired private ITournamentService tournamentService;
	@Autowired private IPlayerService playerService;
	
	@ModelAttribute("players")
	public List<Player> getPlayers() {
		return playerService.getPlayers();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/newTournament.htm")
	public ModelAndView requestNewTournament(ModelMap model) {
		//model.addAttribute("players", playerService.getPlayers());
		model.addAttribute("tournament", new Tournament());
		model.addAttribute("tournamentTypes", tournamentService.getAllTournamentTypes());
		return new ModelAndView("newTournament", model);
	}
	
	@InitBinder
	protected void initBinder(final HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {

		binder.registerCustomEditor(Set.class, "subscribers", new CustomCollectionEditor(SortedSet.class) {
			@Override
			protected Object convertElement(Object element) {
				
				int id = Integer.parseInt((String)element);
				Player p = playerService.getPlayer(id);
				
//				TournamentPlayer tp = new TournamentPlayer();
//				tp.setPlayer(p);
//				//	tp.setTournament((Tournament)request.getSession().getAttribute("tournament"));
//				tp.setDisqualified(false);
//				return tp;
								
				return p;
			}						
		});
	}
	
	@RequestMapping(method=RequestMethod.POST, params="addSubtournament")
	public String submitNewSubTournament(@ModelAttribute("tournament") Tournament tournament, BindingResult result, HttpServletRequest request, SessionStatus status) throws Exception {
		SubTournament st = new SubTournament();		
		st.setWithPoules(ServletRequestUtils.getBooleanParameter(request, "withPoules", false));
		st.setName(ServletRequestUtils.getStringParameter(request, "descriptionRound"));
		
		String fd = ServletRequestUtils.getStringParameter(request, "stFromDate");
		String td = ServletRequestUtils.getStringParameter(request, "stToDate");
		Date fromDate = new SimpleDateFormat("dd/MM/yyyy").parse(fd);
		Date toDate = new SimpleDateFormat("dd/MM/yyyy").parse(td);
		
		st.setFromDate(fromDate);
		st.setToDate(toDate);
		
		st.setPointsFrame(ServletRequestUtils.getIntParameter(request, "pointsFrame"));
		st.setPointsLoserEF(ServletRequestUtils.getIntParameter(request, "pointsLoserEF"));
		st.setPointsLoserQF(ServletRequestUtils.getIntParameter(request, "pointsLoserQF"));
		st.setPointsLoserSF(ServletRequestUtils.getIntParameter(request, "pointsLoserSF"));
		st.setPointsLoserF(ServletRequestUtils.getIntParameter(request, "pointsLoserF"));
		st.setPointsWinnerF(ServletRequestUtils.getIntParameter(request, "pointsWinnerF"));
		
		KnockoutRound knockoutRound = new KnockoutRound();
		knockoutRound.setSubTournament(st);
		st.setKnockoutRound(knockoutRound);
		st.setInKnockoutStage(!st.isWithPoules());
		
		//tournament.addEvent(st);
		tournament.addSubTournament(st);
		
		return "newTournament";
	}
	
	
	/**
	 * A Tournament contains multiple single-match periods.  There is no distinction
	 * between single-match periods for A-serie ('A'-players) or B-serie('B'-players).  
	 * There are an equal amount of periods for both series
	 * A single-match periods will contain matches of both A-players and B-players
	 */
	@RequestMapping(method=RequestMethod.POST, params="addPeriod")
	public String submitNewPeriod(@ModelAttribute("tournament") Tournament tournament, BindingResult result, HttpServletRequest request, SessionStatus status) throws Exception {
		SingleMatchPeriod period = new SingleMatchPeriod();
		
		period.setName(ServletRequestUtils.getStringParameter(request, "periodName"));		
		String fd = ServletRequestUtils.getStringParameter(request, "pFromDate");
		String td = ServletRequestUtils.getStringParameter(request, "pToDate");		
		Date fromDate = new SimpleDateFormat("dd/MM/yyyy").parse(fd);
		Date toDate = new SimpleDateFormat("dd/MM/yyyy").parse(td);		
		period.setFromDate(fromDate);
		period.setToDate(toDate);

		//tournament.addEvent(period);
		tournament.addSingleMatchPeriod(period);
		
				
		return "newTournament";
	}
	
	
	@RequestMapping(method=RequestMethod.POST, params="saveTournament")
	public String submitNewTournament(@ModelAttribute("tournament") Tournament tournament, BindingResult result, HttpServletRequest request, SessionStatus status) throws Exception {
		
		int[] subscribers = ServletRequestUtils.getIntParameters(request, "subscribers");
		
		for (int i=0; i<subscribers.length; i++) {		
			TournamentPlayer ts = new TournamentPlayer();
			ts.setPlayer(playerService.getPlayer(subscribers[i]));
			ts.setGrade(ServletRequestUtils.getStringParameter(request, Integer.toString(subscribers[i]))/*grades[i]*/);
			ts.setDisqualified(false);
			ts.setTournament(tournament);
			
			tournament.getSubscribers().add(ts);
		}
		
		tournamentService.createTournament(tournament);
		
		return "main";
	}
	
}
