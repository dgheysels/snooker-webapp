package controller;

import java.beans.PropertyEditorSupport;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import service.IInterclubService;
import service.IPasswordGenerator;
import service.IPlayerService;
import service.ISnookerclubService;
import domain.InterclubTeam;
import domain.Player;
import domain.Role;
import domain.Snookerclub;


@Controller
@RequestMapping("/editMember.htm")
public class EditMemberController implements ServletContextAware {

	private ServletContext servletContext;
	@Autowired private IPlayerService playerService;
	@Autowired private ISnookerclubService snookerclubService;
	@Autowired private IInterclubService interclubService;
	@Autowired private IPasswordGenerator passwordGenerator;
	
	@Autowired private Validator validator;
	
	/**
	 * This method return an player object used as backing-object
	 * for the form.
	 * The player has by default:
	 *   - active=true
	 *   - ROLE_PLAYER
	 */
	@ModelAttribute("player")
	public Player getPlayer(@RequestParam("playerId") int playerId) {
		return playerService.getPlayer(playerId);
	}
	
	@InitBinder
	public void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		
		binder.registerCustomEditor(Snookerclub.class, "snookerclub", 
			new PropertyEditorSupport() {
				@Override
				public String getAsText() {
					Snookerclub sc = (Snookerclub)getValue();
					if (sc != null) {
						return String.valueOf(sc.getId());
					} else {
						return null;
					}
				}
				
				public void setAsText(String text) {
					Snookerclub sc = snookerclubService.getSnookerclub(Integer.parseInt(text));
					setValue(sc);					
				}
		});
		
		binder.registerCustomEditor(InterclubTeam.class, "interclubTeam",
				new PropertyEditorSupport() {
					@Override
					public String getAsText() {
						InterclubTeam icTeam = (InterclubTeam)getValue();
						if (icTeam != null) {
							return String.valueOf(icTeam.getId());
						} else {
							return null;
						}
					}
					
					@Override
					public void setAsText(String text) {
						int id = Integer.parseInt(text);
						if (id == -1) {
							setValue(null);
						} else {
							InterclubTeam icTeam = interclubService.getInterclubTeam(id);
							setValue(icTeam);
						}
					}
				});
		
		
		
		binder.registerCustomEditor(Integer.class, "interclubRank",
				new CustomNumberEditor(Integer.class, true) {
					@Override
					public String getAsText() {
						Integer rank = (Integer)getValue();
						if (rank != null) {
							switch(rank) {
								case 0: return Player.InterclubRank.CAPTAIN.toString();
								case 1: return Player.InterclubRank.PLAYER2.toString();
								case 2: return Player.InterclubRank.PLAYER3.toString();
								default: return StringUtils.EMPTY;
							}
						} else {
							return StringUtils.EMPTY;
						}
					}
					
					@Override
					public void setAsText(String text) {
						int id = Integer.parseInt(text);
						if (id == -1) {
							setValue(null);
						} else {
							setValue(id);							
						}
					}
				});
		
		binder.registerCustomEditor(Role.class, "role",
			new PropertyEditorSupport() {
				@Override
				public String getAsText() {
					Role r = (Role)getValue();
					if (r != null) {
						return String.valueOf(r.getId());
					} else {
						return null;
					}
				}
				
				@Override
				public void setAsText(String text) {
					Role r = playerService.getRole(Integer.parseInt(text));
					setValue(r);
				}
			});
		
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/editMember.htm")
	public String requestEditPlayer(@RequestParam("playerId") int playerId, ModelMap model) {
		
		model.addAttribute("snookerclubs", snookerclubService.getSnookerclubs());
		model.addAttribute("interclubTeams", interclubService.getInterclubTeams());
		//model.addAttribute("interclubRanks", Player.InterclubRank.values());
		model.addAttribute("roles", playerService.getRoles());
		
		//TODO check if:
		// 1) user is ingelogd 
		// 2) ingelogde user = ROLE_ADMIN of ROLE_SUPERUSER
		//                     of ingelogde-user-Id = playerId
		//model.addAttribute("player", playerService.getPlayer(playerId));
		return "editMember";
	}
	
	@RequestMapping(method = RequestMethod.POST, params="savePlayer")
	public ModelAndView submitEditPlayer(@ModelAttribute("player") Player player, BindingResult result, HttpServletRequest request, SessionStatus status) throws Exception {
		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request;
		MultipartFile f = mpr.getFile("photo");
		
		if (f != null && !StringUtils.isEmpty(f.getOriginalFilename())) {			
			player.setPhoto(f.getBytes());					
		}
		
		if (player.getInterclubTeam() != null) {
			int idx = ServletRequestUtils.getIntParameter(request, "interclubRank");
			player.setInterclubRank(idx);
			switch (idx) {
			case 0:
				player.getInterclubTeam().setPlayer1(player);
				break;
			case 1:
				player.getInterclubTeam().setPlayer2(player);
				break;
			case 2:
				player.getInterclubTeam().setPlayer3(player);
				break;
				
			default:
				break;
			}			
		}
						
		boolean generatePassword = ServletRequestUtils.getStringParameter(request, "regeneratePwd") != null;
		
		playerService.savePlayer(player, generatePassword);		
	
		return new ModelAndView(new RedirectView("listMembers.htm"));
	}
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;		
	}
}
