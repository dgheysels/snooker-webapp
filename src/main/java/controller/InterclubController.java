package controller;

import java.beans.PropertyEditorSupport;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import service.IAuthenticationService;
import service.IInterclubService;
import service.IPlayerService;
import domain.InterclubDivision;
import domain.InterclubTeam;
import domain.Player;

@Controller
@RequestMapping({"/viewInterclub.htm", "/newInterclubTeam.htm", "/editInterclubTeam.htm", "/deleteInterclubTeam.htm"})
public class InterclubController {

	@Autowired private IInterclubService interclubService;
	@Autowired private IPlayerService playerService;
	@Autowired private IAuthenticationService authenticationService;
	
	@ModelAttribute("divisions")
	public List<InterclubDivision> getDivisions() {
		return interclubService.getInterclubDivisions();
	}
	
	@ModelAttribute("players") 
	public List<Player> getPlayers() {
		return playerService.getPlayers(); 
	}
	
	@ModelAttribute("team")
	public InterclubTeam getTeam() {				
		return new InterclubTeam();		
	}	
	
	@InitBinder
	public void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		binder.registerCustomEditor(InterclubDivision.class, "interclubDivision", 
			new PropertyEditorSupport() {
				@Override
				public String getAsText() {
					InterclubDivision d = (InterclubDivision)getValue();
					if (d != null) {
						return String.valueOf(d.getId());
					} else {
						return null;
					}
				}
				
				public void setAsText(String text) {
					InterclubDivision d = interclubService.getInterclubDivision(Integer.parseInt(text));
					setValue(d);					
				}
		});
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/viewInterclub.htm")
	public String requestViewInterclub(ModelMap model) throws Exception {		
		
		if (authenticationService.hasRole("ROLE_ADMIN") ||
			authenticationService.hasRole("ROLE_SUPERUSER")) {
			
			model.addAttribute("interclubOverview", interclubService.getFullOverview());
		} else {
			model.addAttribute("interclubOverview", interclubService.getOverview());
		}

		
		return "viewInterclub";		
	}

	@RequestMapping(method = RequestMethod.GET, value="/newInterclubTeam.htm")
	public String requestAddInterclubTeam(ModelMap model, @ModelAttribute("team") InterclubTeam team, @RequestParam("divisionId") int divisionId) throws Exception {

		team.setInterclubDivision(interclubService.getInterclubDivision(divisionId));
		
		return "newInterclubTeam";
	}	
	
	@RequestMapping(method = RequestMethod.POST, params="saveTeam")
	public ModelAndView submitInterclubTeam(@ModelAttribute("team") InterclubTeam team, BindingResult result, HttpServletRequest request, SessionStatus status) throws Exception {
	
		int player1Id = ServletRequestUtils.getIntParameter(request, "player1");
		int player2Id = ServletRequestUtils.getIntParameter(request, "player2");
		int player3Id = ServletRequestUtils.getIntParameter(request, "player3");
		
		Player p1 = playerService.getPlayer(player1Id);		
		Player p2 = playerService.getPlayer(player2Id);		
		Player p3 = null;
		
		if (player3Id != -1) {
			p3 = playerService.getPlayer(ServletRequestUtils.getIntParameter(request, "player3"));
		}
		
		team.setPlayer1(p1); //captain
		team.setPlayer2(p2); //2nd lieutenant
		team.setPlayer3(p3); //just a soldier ...
		
		interclubService.saveInterclubTeam(team);
		
		return new ModelAndView(new RedirectView("viewInterclub.htm"));
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/editInterclubTeam.htm")
	public ModelAndView requestEditInterclubTeam(ModelMap model, @RequestParam("teamId") int teamId) throws Exception {

		model.addAttribute("team", interclubService.getInterclubTeam(teamId));
		
		return new ModelAndView("editInterclubTeam", model);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/deleteInterclubTeam.htm")
	public ModelAndView requestDeleteInterclubTeam(ModelMap model, @RequestParam("teamId") int teamId) throws Exception {
		InterclubTeam team = interclubService.getInterclubTeam(teamId);
		interclubService.deleteInterclubTeam(team);
		
		return new ModelAndView(new RedirectView("viewInterclub.htm"));
	}


}
