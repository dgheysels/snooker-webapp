package controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import domain.Meeting;

import service.IMeetingService;
import service.IPlayerService;

@Controller
@RequestMapping({"/management.htm", "/newMeeting.htm", "/getReport.htm"})
public class ManagementController {

	@Autowired private IPlayerService playerService;
	@Autowired private IMeetingService meetingService;
	
	@ModelAttribute("meeting")
	public Meeting getMeeting() {
		return new Meeting();
	}
	
	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws ServletException {        
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("dd/MM/yyyy"), false));
    }
	
	@RequestMapping(method=RequestMethod.GET, value="/management.htm")
	public String requestManagement(ModelMap model) {		
		model.addAttribute("management", playerService.getManagement());		
		model.addAttribute("meetings", meetingService.getAllMeetings());
		
		return "management";
	}	
		
	@RequestMapping(method=RequestMethod.GET, value="/newMeeting.htm")
	public ModelAndView requestNewMeeting(ModelMap model) {		
		return new ModelAndView("newMeeting", model);
	}
	
	@RequestMapping(method=RequestMethod.POST, params="saveMeeting")
	public ModelAndView submitNewMeeting(@ModelAttribute("meeting") Meeting meeting, BindingResult result, HttpServletRequest request, SessionStatus status) throws Exception {
				
		meetingService.saveMeeting(meeting);
		
		return new ModelAndView(new RedirectView("main.htm"));		
	}	
	
	@RequestMapping(method=RequestMethod.GET, value="/getReport.htm")
	public ModelAndView getReport(@RequestParam("meetingId") int meetingId, HttpServletResponse response) throws Exception {
		Meeting meeting = meetingService.getMeeting(meetingId);
		byte[] report = meeting.getReport();

		if (report != null) {
			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Length", String.valueOf(report.length));

			BufferedInputStream input = null;
			BufferedOutputStream output = null;
			
			try {
				// Open streams.				
				ByteArrayInputStream bais = new ByteArrayInputStream(report);
				input = new BufferedInputStream(bais);
				output = new BufferedOutputStream(response.getOutputStream());

				// Write file contents to response.
				byte[] buffer = new byte[report.length];
				int length;
				while ((length = input.read(buffer)) > 0) {
					output.write(buffer, 0, length);
				}

				output.flush();
			} finally {	            
				output.close();
				input.close();
			}

		}
		return null;

	}
}
