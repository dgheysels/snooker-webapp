package controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import service.ITournamentService;
import view.TournamentRankingPDF;
import domain.Tournament;
import domain.TournamentType;

@Controller
@RequestMapping({"/viewRanking.htm", "/printRanking.htm"})
public class ViewTournamentRankingController {

	@Autowired private ITournamentService tournamentService;
		
	@ModelAttribute("tournamentTypes")
	public TournamentType[] getTournamentTypes() {
		return TournamentType.values();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/viewRanking.htm")
	public ModelAndView requestTournamentRanking(@RequestParam("tournamentId") int tournamentId, ModelMap model) {
		
		Tournament tournament = tournamentService.getTournament(tournamentId);
		model.addAttribute("tournament", tournament);
		model.addAttribute("ranking", tournament.calculateRanking());
		
		return new ModelAndView("viewRanking", model);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/printRanking.htm")
	public ModelAndView requestPrintTournamentRanking(@RequestParam("tournamentId") int tournamentId) {

		Map model = new HashMap();
				
		Tournament tournament = tournamentService.getTournament(tournamentId);
		
		model.put("ranking", tournament.getRanking());
				
		return new ModelAndView(new TournamentRankingPDF(), model);	
	}
	
//	public void requestDisqualifyPlayer(@RequestParam("tournamentId") int tournamentId, @RequestParam("playerId") int playerId) {
//		
//	}
}
