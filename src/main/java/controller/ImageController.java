package controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.IPlayerService;
import domain.Player;

@Controller
@RequestMapping("/getImage.htm")
public class ImageController {

	@Autowired
	private IPlayerService playerService;

	@RequestMapping(method=RequestMethod.GET, value="/getImage.htm")
	public String requestImage(@RequestParam("playerId") int playerId, HttpServletResponse response) throws Exception {
		Player player = playerService.getPlayer(playerId);
		byte[] image = player.getPhoto();


		if (image != null) {
			response.setHeader("Content-Type", "image/jpeg");
			response.setHeader("Content-Length", String.valueOf(image.length));

			BufferedInputStream input = null;
			BufferedOutputStream output = null;
			
			try {
				// Open streams.				
				
				ByteArrayInputStream bais = new ByteArrayInputStream(image);
				input = new BufferedInputStream(bais);
				output = new BufferedOutputStream(response.getOutputStream());

				// Write file contents to response.
				byte[] buffer = new byte[image.length];
				int length;
				while ((length = input.read(buffer)) > 0) {
					output.write(buffer, 0, length);
				}

				output.flush();
			} finally {	            
				output.close();
				input.close();
			}

		}
		return null;

	}
}
