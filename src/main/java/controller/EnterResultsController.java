package controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import service.IPlayerService;
import service.ITournamentService;
import domain.Break;
import domain.KnockoutMatch;
import domain.Player;
import domain.Poule;
import domain.PouleMatch;
import domain.SingleMatch;
import domain.SingleMatchPeriod;
import domain.SubTournament;
import domain.Tournament;
import domain.TournamentPlayer;

@Controller
@RequestMapping({"/enterResults.htm", "/enterResults_SelectSubTournament.htm"})
@SessionAttributes({"tournament", "poule", "subTournament", "period"})
public class EnterResultsController {

	@Autowired private ITournamentService tournamentService;
	@Autowired private IPlayerService playerService;
	
	@RequestMapping(method=RequestMethod.GET, value="/enterResults.htm")
	public ModelAndView requestEnterResults(@RequestParam("tournamentId") int tournamentId, ModelMap model) {		
		model.addAttribute("tournament", tournamentService.getTournament(tournamentId));
		return new ModelAndView("enterResults", model);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/enterResults_SelectSubTournament.htm")
	public ModelAndView requestEnterResults_selectSubTournament(@RequestParam("tournamentId") int tournamentId, ModelMap model) {		
		return new ModelAndView("enterResults_SelectSubTournament", model);
	}
	
	@RequestMapping(method=RequestMethod.POST, params="selectRoundEnterResults")
	public ModelAndView submitRoundEnterResults(HttpServletRequest request, ModelMap model) throws Exception {
		
		String round_or_match = ServletRequestUtils.getStringParameter(request, "round_or_match");
		
		Tournament tournament = tournamentService.getTournament(ServletRequestUtils.getIntParameter(request, "tournamentId"));
		model.addAttribute("tournament", tournament);
		
		if (round_or_match.equalsIgnoreCase("round")) {
					
			SubTournament subTournament = tournamentService.getSubTournament(ServletRequestUtils.getIntParameter(request, "subTournamentId"));			
			
			model.addAttribute("subTournament", subTournament);
			
			if (subTournament.isWithPoules() && !subTournament.isInKnockoutStage()) {
				// input poule-results not done yet...	
				model.addAttribute("poule", new Poule());
				return new ModelAndView("enterResults_Poule", model);			
			} else {
				// input poule-results is done, a knockout-round is created for this sub-tournament			
				return new ModelAndView("enterResults_Knockout", model);			
			}
		} else {	
			
			SingleMatchPeriod period = tournamentService.getSingleMatchPeriod(ServletRequestUtils.getIntParameter(request, "periodId"));
			model.addAttribute("period", period);
			
			return new ModelAndView("enterResults_SingleMatch", model);
		}
	}

	/**
	 * A single-match is added to the selected period.  This match is played be between
	 * two A-players or two B-players.
	 * 
	 * TODO 1. check if both players are of the same grade
	 * TODO 2. check if match-date is in between period-fromdate and period-todate
	 */
	@RequestMapping(method=RequestMethod.POST, params="addSingleMatch")
	public ModelAndView addSingleMatch(@ModelAttribute("period") SingleMatchPeriod period, HttpServletRequest request, ModelMap model) throws Exception {
		/* retrieve match data: players + scores + breaks */
		Player playerA = playerService.getPlayer(ServletRequestUtils.getIntParameter(request, "playerA"));
		Player playerB = playerService.getPlayer(ServletRequestUtils.getIntParameter(request, "playerB"));
		int scoreA = ServletRequestUtils.getIntParameter(request, "scoreA");
		int scoreB = ServletRequestUtils.getIntParameter(request, "scoreB");
		String[] breaksA = StringUtils.split(ServletRequestUtils.getStringParameter(request, "breaksPlayerA"), ",");
		String[] breaksB = StringUtils.split(ServletRequestUtils.getStringParameter(request, "breaksPlayerB"), ",");		
		String d = ServletRequestUtils.getStringParameter(request, "date");
		Date date = new SimpleDateFormat("dd/MM/yyyy").parse(d);			
		
		SingleMatch match = new SingleMatch();
		match.setPlayerA(playerA);
		match.setPlayerB(playerB);
		match.setScoreA(scoreA);
		match.setScoreB(scoreB);		
		match.setDate(date);
				
		for (String b : breaksA) {
			Break brk = new Break();
			brk.setPlayer(playerA);
			brk.setMatch(match);
			brk.setValue(Integer.valueOf(b.trim()));
			
			match.getBreaks().add(brk);
			playerA.getBreaks().add(brk);
		}
		
		for (String b : breaksB) {
			Break brk = new Break();
			brk.setPlayer(playerB);
			brk.setMatch(match);
			brk.setValue(Integer.valueOf(b.trim()));
			
			match.getBreaks().add(brk);
			playerB.getBreaks().add(brk);	
		}	
		
		
		period.addMatch(match);
		
		return new ModelAndView("enterResults_SingleMatch", model);
	}

	
	@RequestMapping(method=RequestMethod.POST, params="addPouleMatch")
	public ModelAndView addPouleMatch(@ModelAttribute("subTournament") SubTournament subTournament, @ModelAttribute("poule") Poule poule, HttpServletRequest request, ModelMap model) throws Exception {

		/* retrieve match data: players + scores + breaks */
		Player playerA = playerService.getPlayer(ServletRequestUtils.getIntParameter(request, "playerA"));
		Player playerB = playerService.getPlayer(ServletRequestUtils.getIntParameter(request, "playerB"));
		int scoreA = ServletRequestUtils.getIntParameter(request, "scoreA");
		int scoreB = ServletRequestUtils.getIntParameter(request, "scoreB");
		String[] breaksA = StringUtils.split(ServletRequestUtils.getStringParameter(request, "breaksPlayerA"), ",");
		String[] breaksB = StringUtils.split(ServletRequestUtils.getStringParameter(request, "breaksPlayerB"), ",");
		
		PouleMatch match = new PouleMatch();
		match.setPlayerA(playerA);
		match.setPlayerB(playerB);
		match.setScoreA(scoreA);
		match.setScoreB(scoreB);		
						
		for (String b : breaksA) {
			Break brk = new Break();
			brk.setPlayer(playerA);
			brk.setMatch(match);
			brk.setValue(Integer.valueOf(b.trim()));
			
			match.getBreaks().add(brk);
			playerA.getBreaks().add(brk);
		}
		
		for (String b : breaksB) {
			Break brk = new Break();
			brk.setPlayer(playerB);
			brk.setMatch(match);
			brk.setValue(Integer.valueOf(b.trim()));
			
			match.getBreaks().add(brk);
			playerB.getBreaks().add(brk);	
		}		
		
				
		subTournament.addMatch(poule, match);
		
		return new ModelAndView("enterResults_Poule", model);
	}
	
	@RequestMapping(method=RequestMethod.POST, params="savePoule")
	public ModelAndView savePoule(@ModelAttribute("subTournament") SubTournament subTournament, @ModelAttribute("poule") Poule poule, HttpServletRequest request, ModelMap model) throws Exception {
		
		String poulename = ServletRequestUtils.getStringParameter(request, "poulename");
		int[] poulewinners = ServletRequestUtils.getIntParameters(request, "winner");
		
		poule.setName(poulename);
		poule.setSubTournament(subTournament);
		
		for (int winnerId : poulewinners) {
			Player p = playerService.getPlayer(winnerId);
			//TournamentPlayer ts = tournamentService.findSubscription(subTournament.getTournament(), p);
			TournamentPlayer tp = subTournament.getTournament().findSubscription(p);
			poule.addWinner(tp.getPlayer());
		}
		
		subTournament.getPoules().add(poule);
		
		// save poule + matches + breaks		
		tournamentService.saveSubTournament(subTournament);
					
		// new poule data
		model.addAttribute("poule", new Poule());		
		
		return new ModelAndView("enterResults_Poule", model);
	}
	
	@RequestMapping(method=RequestMethod.POST, params="savePeriod")
	public ModelAndView savePeriod(@ModelAttribute("period") SingleMatchPeriod period, HttpServletRequest request, ModelMap model) throws Exception {
	
		// save period + matches + breaks		
		tournamentService.saveSingleMatchPeriod(period);
						
		return new ModelAndView(new RedirectView("main.htm"));
	}
	
	@RequestMapping(method=RequestMethod.POST, params="generateKnockoutTable")
	public ModelAndView generateKnockoutTable(@ModelAttribute("subTournament") SubTournament subTournament, HttpServletRequest request, ModelMap model) throws Exception {

		subTournament.setInKnockoutStage(true);
		
		tournamentService.saveSubTournament(subTournament);
				
		return new ModelAndView("enterResults_Knockout", model);			
	}	
	
	@RequestMapping(method=RequestMethod.POST, params="addKnockoutMatch")
	public ModelAndView addKnockoutMatch(@ModelAttribute("subTournament") SubTournament subTournament, HttpServletRequest request, ModelMap model) throws Exception {

		int position = ServletRequestUtils.getIntParameter(request, "knockoutPosition");
		Player playerA = playerService.getPlayer(ServletRequestUtils.getIntParameter(request, "playerA"));
		Player playerB = playerService.getPlayer(ServletRequestUtils.getIntParameter(request, "playerB"));
		int scoreA = ServletRequestUtils.getIntParameter(request, "scoreA");
		int scoreB = ServletRequestUtils.getIntParameter(request, "scoreB");	
		String[] breaksA = StringUtils.split(ServletRequestUtils.getStringParameter(request, "breaksPlayerA"), ",");
		String[] breaksB = StringUtils.split(ServletRequestUtils.getStringParameter(request, "breaksPlayerB"), ",");
		
		KnockoutMatch match = new KnockoutMatch();
		match.setPlayerA(playerA);
		match.setPlayerB(playerB);
		match.setScoreA(scoreA);
		match.setScoreB(scoreB);
		match.setPosition(position);
		
		for (String b : breaksA) {
			Break brk = new Break();
			brk.setPlayer(playerA);
			brk.setMatch(match);
			brk.setValue(Integer.valueOf(b.trim()));
			
			match.getBreaks().add(brk);
			playerA.getBreaks().add(brk);
		}
		
		for (String b : breaksB) {
			Break brk = new Break();
			brk.setPlayer(playerB);
			brk.setMatch(match);
			brk.setValue(Integer.valueOf(b.trim()));
			
			match.getBreaks().add(brk);
			playerB.getBreaks().add(brk);	
		}

		subTournament.addMatch(match);
				
		model.addAttribute("subTournament", subTournament);		
		
		return new ModelAndView("enterResults_Knockout", model);
	}
	
	@RequestMapping(method=RequestMethod.POST, params="saveKnockout")
	public ModelAndView saveKnockout(@ModelAttribute("subTournament") SubTournament subTournament, HttpServletRequest request, ModelMap model) throws Exception {
	
		// save subTournament + matches + breaks		
		tournamentService.saveSubTournament(subTournament);
				
		return new ModelAndView(new RedirectView("main.htm"));
	}
	
	@RequestMapping(method=RequestMethod.POST, params="finishKnockout")
	public ModelAndView finishKnockout(@ModelAttribute("subTournament") SubTournament subTournament, HttpServletRequest request, ModelMap model) throws Exception {

		subTournament.setFinished(true);
		
		// save subTournament + matches + breaks		
		tournamentService.saveSubTournament(subTournament);
		
		return new ModelAndView(new RedirectView("main.htm"));
	}
	
	@RequestMapping(method=RequestMethod.POST, params="finishPeriod")
	public ModelAndView finishPeriod(@ModelAttribute("period") SingleMatchPeriod period, HttpServletRequest request, ModelMap model) throws Exception {

		period.setFinished(true);
		
		// save period + matches + breaks		
		tournamentService.saveSingleMatchPeriod(period);
		
		return new ModelAndView(new RedirectView("main.htm"));
	}
}
