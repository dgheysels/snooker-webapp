package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import service.INoticeService;


@Controller
@RequestMapping("/main.htm")
public class MainController {

	@Autowired private INoticeService noticeService;
	
	@RequestMapping(method = RequestMethod.GET, value="/main.htm")
	public ModelAndView requestMainPage(ModelMap model) {		
		
		model.addAttribute("recentNotices", noticeService.getRecentNotices(3));
		
		return new ModelAndView("main", model);					
	}	
}

