package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import service.ITournamentService;
import domain.Tournament;

@Controller
@RequestMapping("/navigation.htm")
public class NavigationController {

	@Autowired private ITournamentService tournamentService;
		
	@RequestMapping("/navigation.htm")
	protected ModelAndView requestForm(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		Tournament tournament = tournamentService.getActiveTournament();
		
		ModelAndView model = new ModelAndView("navigation");
		model.addObject("menuId", ServletRequestUtils.getStringParameter(request, "menuId"));
		model.addObject("activeTournament", tournament);
		
		return model;
	}
	
	public void setTournamentService(ITournamentService tournamentService) {
		this.tournamentService = tournamentService;
	}

}
