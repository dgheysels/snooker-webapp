package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping({"/clubChampionship.htm", "/viewRankingClubChampionship.htm", "/viewResultsClubChampionship.htm"})
public class ClubChampionshipController {

	@RequestMapping(method=RequestMethod.GET, value="/clubChampionship.htm")
	public ModelAndView requestClubChampionship() {
		return new ModelAndView("clubChampionship");
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/viewRankingClubChampionship.htm")
	public ModelAndView requestViewRankingClubChampionship() {
		return new ModelAndView("viewRankingClubChampionship");
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/viewResultsClubChampionship.htm")
	public ModelAndView requestViewResultsClubChampionship() {
		return new ModelAndView("viewResultsClubChampionship");
	}
}
