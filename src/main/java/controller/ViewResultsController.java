package controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import service.ITournamentService;
import domain.SubTournament;
import domain.Tournament;

@Controller
@RequestMapping({"/viewResults.htm", "/viewResults_SelectSubTournament.htm"})
@SessionAttributes({"tournament", "poule", "subTournament"})
public class ViewResultsController {

	@Autowired private ITournamentService tournamentService;
	
	@RequestMapping(method=RequestMethod.GET, value="/viewResults.htm")
	public ModelAndView requestViewResults(@RequestParam("tournamentId") int tournamentId, ModelMap model) {		
		model.addAttribute(tournamentService.getTournament(tournamentId));
		model.addAttribute("subTournament", new SubTournament());
		return new ModelAndView("viewResults", model);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/viewResults_SelectSubTournament.htm")
	public ModelAndView requestViewResults_selectSubTournament(@RequestParam("tournamentId") int tournamentId, ModelMap model) {
		//model.addAttribute("tournament", tournamentService.getTournament(tournamentId));
		return new ModelAndView("viewResults_SelectSubTournament", model);
	}
	
	/**
	 * Dependending on the selected serie (A-serie or B-serie), the matches of A-players
	 * or B-players are shown
	 */
	@RequestMapping(method=RequestMethod.POST, params="selectRoundViewResults")
	public ModelAndView submitRoundViewResults(final HttpServletRequest request, ModelMap model) throws Exception {
		
		Tournament tournament = tournamentService.getTournament(ServletRequestUtils.getIntParameter(request, "tournamentId"));
		
		
		String round_or_match = ServletRequestUtils.getStringParameter(request, "round_or_match");
		
		model.addAttribute("tournament", tournament);
		
		if (round_or_match.equals("round")) {
			SubTournament subTournament = tournamentService.getSubTournament(ServletRequestUtils.getIntParameter(request, "subTournamentId"));
			model.addAttribute("subTournament", subTournament);
			
			return new ModelAndView("viewResults_Round", model);
		} else {
			
			String series = ServletRequestUtils.getStringParameter(request, "series");
						
			model.addAttribute("series", series);
			
			return new ModelAndView("viewResults_SingleMatch", model);
			
		}
	}
}
