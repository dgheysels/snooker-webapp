package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/login.htm")
public class LoginController {
	
	@RequestMapping(method=RequestMethod.GET, value="/login.htm")
	public String requestLoginPage(HttpServletRequest request, HttpServletResponse response) {
		return "login";
	}

}
