package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import domain.Tournament;

import service.ITournamentService;

@Controller
@RequestMapping("/closeTournament.htm")
public class CloseTournamentController {

	@Autowired
	private ITournamentService tournamentService;
	
	@RequestMapping(method=RequestMethod.GET, value="/closeTournament.htm")
	public ModelAndView requestCloseTournament() {

		tournamentService.closeActiveTournament();
		
		return new ModelAndView(new RedirectView("main.htm"));		
	}
}
