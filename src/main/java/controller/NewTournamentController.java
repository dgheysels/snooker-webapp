package controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import service.IPlayerService;
import service.ITournamentService;
import domain.KnockoutRound;
import domain.Player;
import domain.SingleMatchPeriod;
import domain.SubTournament;
import domain.Tournament;
import domain.TournamentPlayer;
import domain.TournamentType;

@Controller
@RequestMapping("/newTournament.htm")
@SessionAttributes("tournament")
public class NewTournamentController {

	@Autowired private ITournamentService tournamentService;
	@Autowired private IPlayerService playerService;
	
	@ModelAttribute("players")
	public List<Player> getPlayers() {
		return playerService.getPlayers();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/newTournament.htm")
	public ModelAndView requestNewTournament(ModelMap model) {
		//model.addAttribute("players", playerService.getPlayers());
		model.addAttribute("tournament", new Tournament());
		//model.addAttribute("tournamentTypes", tournamentService.getAllTournamentTypes());
		return new ModelAndView("newTournament", model);
	}
	
	@InitBinder
	protected void initBinder(final HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {

		binder.registerCustomEditor(Set.class, "subscribers", new CustomCollectionEditor(SortedSet.class) {
			@Override
			protected Object convertElement(Object element) {
				
				int id = Integer.parseInt((String)element);
				Player p = playerService.getPlayer(id);
				
//				TournamentPlayer tp = new TournamentPlayer();
//				tp.setPlayer(p);
//				//	tp.setTournament((Tournament)request.getSession().getAttribute("tournament"));
//				tp.setDisqualified(false);
//				return tp;
								
				return p;
			}						
		});
	}
	
	/**
	 * A Tournament contains multiple single-match periods.  There is no distinction
	 * between single-match periods for A-serie ('A'-players) or B-serie('B'-players).  
	 * There are an equal amount of periods for both series
	 * A single-match periods will contain matches of both A-players and B-players
	 */
	@RequestMapping(method=RequestMethod.POST, params="addPeriod")
	public String submitNewPeriod(@ModelAttribute("tournament") Tournament tournament, BindingResult result, HttpServletRequest request, SessionStatus status) throws Exception {
		SingleMatchPeriod period = new SingleMatchPeriod();
		
		period.setName(ServletRequestUtils.getStringParameter(request, "smName"));		
		String fd = ServletRequestUtils.getStringParameter(request, "smFromDate");
		String td = ServletRequestUtils.getStringParameter(request, "smToDate");		
		Date fromDate = new SimpleDateFormat("dd/MM/yyyy").parse(fd);
		Date toDate = new SimpleDateFormat("dd/MM/yyyy").parse(td);		
		period.setFromDate(fromDate);
		period.setToDate(toDate);
		period.setType(TournamentType.SM);
				
		tournament.addSingleMatchPeriod(period);
						
		return "newTournament";
	}
	
	
	@RequestMapping(method=RequestMethod.POST, params="saveTournament")
	public ModelAndView submitNewTournament(@ModelAttribute("tournament") Tournament tournament, BindingResult result, HttpServletRequest request, SessionStatus status) throws Exception {
		
		//players
		int[] subscribers = ServletRequestUtils.getIntParameters(request, "subscribers");		
		for (int i=0; i<subscribers.length; i++) {		
			TournamentPlayer ts = new TournamentPlayer();
			ts.setPlayer(playerService.getPlayer(subscribers[i]));
			ts.setGrade(ServletRequestUtils.getStringParameter(request, Integer.toString(subscribers[i]))/*grades[i]*/);
			ts.setDisqualified(false);
			ts.setTournament(tournament);
			
			tournament.getSubscribers().add(ts);
		}
		
		//handicap tournament
		SubTournament handicapTournament = new SubTournament();
		handicapTournament.setName(ServletRequestUtils.getStringParameter(request, "htName"));
		String fd = ServletRequestUtils.getStringParameter(request, "htFromDate");
		String td = ServletRequestUtils.getStringParameter(request, "htToDate");		
		handicapTournament.setFromDate(new SimpleDateFormat("dd/MM/yyyy").parse(fd));
		handicapTournament.setToDate(new SimpleDateFormat("dd/MM/yyyy").parse(td));	
		handicapTournament.setWithPoules(ServletRequestUtils.getBooleanParameter(request, "htWithPoules", false));
		
		handicapTournament.setPointsFrame(ServletRequestUtils.getIntParameter(request, "htPointsFrame"));
		handicapTournament.setPointsLoser16F(ServletRequestUtils.getIntParameter(request, "htPointsLoser16F"));
		handicapTournament.setPointsLoser8F(ServletRequestUtils.getIntParameter(request, "htPointsLoser8F"));
		handicapTournament.setPointsLoser4F(ServletRequestUtils.getIntParameter(request, "htPointsLoser4F"));
		handicapTournament.setPointsLoser2F(ServletRequestUtils.getIntParameter(request, "htPointsLoser2F"));
		handicapTournament.setPointsLoserF(ServletRequestUtils.getIntParameter(request, "htPointsLoserF"));
		handicapTournament.setPointsWinnerF(ServletRequestUtils.getIntParameter(request, "htPointsWinnerF"));
		
		KnockoutRound knockoutRound = new KnockoutRound();
		knockoutRound.setSubTournament(handicapTournament);
		handicapTournament.setKnockoutRound(knockoutRound);
		handicapTournament.setInKnockoutStage(!handicapTournament.isWithPoules());
		
		handicapTournament.setType(TournamentType.HT);
		
		// eind tournament
		SubTournament endTournament = new SubTournament();		
		endTournament.setName(ServletRequestUtils.getStringParameter(request, "etName"));
		fd = ServletRequestUtils.getStringParameter(request, "etFromDate");
		td = ServletRequestUtils.getStringParameter(request, "etToDate");		
		endTournament.setFromDate(new SimpleDateFormat("dd/MM/yyyy").parse(fd));
		endTournament.setToDate(new SimpleDateFormat("dd/MM/yyyy").parse(td));	
		endTournament.setWithPoules(ServletRequestUtils.getBooleanParameter(request, "etWithPoules", false));
		
		endTournament.setPointsFrame(ServletRequestUtils.getIntParameter(request, "etPointsFrame"));
		endTournament.setPointsLoser16F(ServletRequestUtils.getIntParameter(request, "etPointsLoser16F"));
		endTournament.setPointsLoser8F(ServletRequestUtils.getIntParameter(request, "etPointsLoserEF"));
		endTournament.setPointsLoser4F(ServletRequestUtils.getIntParameter(request, "etPointsLoserQF"));
		endTournament.setPointsLoser2F(ServletRequestUtils.getIntParameter(request, "etPointsLoserSF"));
		endTournament.setPointsLoserF(ServletRequestUtils.getIntParameter(request, "etPointsLoserF"));
		endTournament.setPointsWinnerF(ServletRequestUtils.getIntParameter(request, "etPointsWinnerF"));
		
		knockoutRound = new KnockoutRound();
		knockoutRound.setSubTournament(endTournament);
		endTournament.setKnockoutRound(knockoutRound);
		endTournament.setInKnockoutStage(!endTournament.isWithPoules());
		
		endTournament.setType(TournamentType.ET);
		
		// save
		tournament.addSubTournament(handicapTournament);
		tournament.addSubTournament(endTournament);
		
		tournamentService.saveTournament(tournament);
		
		return new ModelAndView(new RedirectView("main.htm"));
	}
	
}
