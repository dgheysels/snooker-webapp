package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/tnc.htm")
public class TNCController {
	
	@RequestMapping(method = RequestMethod.GET, value="/tnc.htm")
	public String requestAboutPage(HttpServletRequest request, HttpServletResponse response) {
		return "tnc";
	}
}
