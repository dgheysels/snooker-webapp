package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import service.ITournamentService;

@Controller
@RequestMapping("/openTournamentArchive.htm")
public class OpenTournamentArchiveController {

	@Autowired private ITournamentService tournamentService;
	
	@RequestMapping(method=RequestMethod.GET, value="/openTournamentArchive.htm")
	public ModelAndView requestOpenTournamentArchive(ModelMap model) {
				
		return new ModelAndView("openTournamentArchive", model);
	}
}
