package controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import service.IAuthenticationService;
import service.IPlayerService;
import domain.Player;


@Controller
@RequestMapping("/editProfile.htm")
public class EditProfileController {

	@Autowired private IPlayerService playerService;
	
	@Autowired private IAuthenticationService authenticationService;
	@Autowired private MailSender mailSender;
	
	/**
	 * This method return an player object used as backing-object
	 * for the form.
	 * The player has by default:
	 *   - active=true
	 *   - ROLE_PLAYER
	 */
	@ModelAttribute("user")
	public Player getUser() {
		return playerService.getPlayer(authenticationService.getUserId());
	}
	
		
	@RequestMapping(method=RequestMethod.GET, value="/editProfile.htm")
	public String requestEditPlayer(ModelMap model) {		
		return "editProfile";
	}
	
	@RequestMapping(method = RequestMethod.POST, params="saveProfile")
	public ModelAndView submitSaveProfile(@ModelAttribute("user") Player player, BindingResult result, HttpServletRequest request, SessionStatus status) throws Exception {
		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request;
		MultipartFile f = mpr.getFile("photo");
		
		if (f != null && !StringUtils.isEmpty(f.getOriginalFilename())) {			
			player.setPhoto(f.getBytes());
		}
		
		String currentPwd = ServletRequestUtils.getStringParameter(request, "currentPwd");
		boolean pwdChanged = false;
		if (!StringUtils.isEmpty(currentPwd)) {
			if (StringUtils.equals(currentPwd, player.getPassword())) {
				String newPwd = ServletRequestUtils.getStringParameter(request, "newPwd");
				String newPwd2 = ServletRequestUtils.getStringParameter(request, "newPwd2");
				
				if (StringUtils.equals(newPwd, newPwd2)) {
					player.setPassword(newPwd);
					pwdChanged = true;
				}
			}
		}
		
		playerService.saveProfile(player, pwdChanged);				
		
		return new ModelAndView(new RedirectView("main.htm"));
	}
}
