package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SnookerclubController {

	@RequestMapping(method=RequestMethod.GET, value="/history.htm")
	public String requestHistory() {
		return "history";		
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/sponsors.htm")
	public String requestSponsors() {
		return "sponsors";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/links.htm")
	public String requestLinks() {
		return "links";
	}
	
	@RequestMapping(method=RequestMethod.GET , value="/multimedia.htm")
	public String requestMultimedia() {
		return "multimedia";
	}
}
