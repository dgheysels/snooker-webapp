package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"/davyscup.htm", "/davyscupPhotos.htm"})
public class DavyscupController {

	@RequestMapping(method=RequestMethod.GET, value="/davyscup.htm")
	public String requestDavyscup() {
		return "davyscup";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/davyscupPhotos.htm")
	public String requestDavyscupPhotos() {
		return "davyscupPhotos";
	}
	
/*
	@RequestMapping(method=RequestMethod.GET, value="/dc_20062007.htm")
	public String request20062007() {
		return "davyscup20062007";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/dc_20072008.htm")
	public String request20072008() {
		return "davyscup20072008";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/dc_20082009.htm")
	public String request20082009() {
		return "davyscup20082009";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/dc_20092010.htm")
	public String request20092010() {
		return "davyscup20092010";
	}
*/
}
