package repository;

import java.util.List;

import domain.InterclubDivision;

public interface IInterclubDivisionRepository {

	public List<InterclubDivision> getAllInterclubDivisions();
	public List<InterclubDivision> getInterclubDivisions();
	public InterclubDivision getInterclubDivision(int divisionId);
}
