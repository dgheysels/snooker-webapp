package repository;

import java.util.List;

import domain.Role;

public interface IRoleRepository {

	public List<Role> getRoles();
	public Role getRole(int id);
	public Role getRole(final String name);
	
}
