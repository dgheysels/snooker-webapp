package repository.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import repository.ITournamentRepository;
import domain.Player;
import domain.Tournament;
import domain.TournamentPlayer;
import domain.TournamentType;

@Repository("tournamentRepository")
public class TournamentRepository extends HibernateDaoSupport implements ITournamentRepository {

	@Autowired
	public TournamentRepository(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}
	
	public List<Tournament> getAllTournaments() {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.find("FROM Tournament");			
	}
	
	public Tournament getTournament(int tournamentId) {
		HibernateTemplate ht = getHibernateTemplate();
		return (Tournament)ht.load(Tournament.class, tournamentId);
	}
	
	public Tournament getActiveTournament() {
		HibernateTemplate ht = getHibernateTemplate();
		List activeTournaments = ht.find("FROM Tournament t WHERE t.active = true");
		if (!activeTournaments.isEmpty()) {
			return (Tournament)activeTournaments.get(0);
		} else {
			return null;
		}
	}
	
	public void saveTournament(Tournament tournament) {
		HibernateTemplate ht = getHibernateTemplate();		
		ht.saveOrUpdate(tournament);			
	}
	
	
	public List<TournamentPlayer> getSubscriptions(Tournament tournament) {
		HibernateTemplate ht = getHibernateTemplate();
		return (List<TournamentPlayer>)ht.findByNamedParam(
				"FROM TournamentPlayer ts " +
				"JOIN ts.tournament t " +
				"WHERE t = :tournament", "tournament", tournament);
	}
	
	public TournamentPlayer getSubscription(Tournament tournament, Player player) {
		HibernateTemplate ht = getHibernateTemplate();
		List<TournamentPlayer> results = (List<TournamentPlayer>)ht.findByNamedParam(
				"SELECT ts " +
				"FROM TournamentPlayer ts " +
				"JOIN ts.tournament t " +
				"JOIN ts.player p " +
				"WHERE t = :tournament " +
				"AND p = :player", new String[] {"tournament", "player"}, new Object[] {tournament, player});
	
		if (results.size() != 0) {
			return results.get(0);
		} else {
			return null;
		}
	}
	
	public void updateSubscription(TournamentPlayer subscription) {
		HibernateTemplate ht = getHibernateTemplate();
		ht.saveOrUpdate(subscription);
	}
	
	public List<TournamentType> getAllTournamentTypes() {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.loadAll(TournamentType.class);
	}
//	public void saveMatch(SingleMatch match) {
//		HibernateTemplate ht = getHibernateTemplate();
//		ht.saveOrUpdate(match);
//	}	
	
}
