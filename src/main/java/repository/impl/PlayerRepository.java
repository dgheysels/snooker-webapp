package repository.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import repository.IPlayerRepository;
import domain.Player;

@Repository("playerRepository")
public class PlayerRepository extends HibernateDaoSupport implements IPlayerRepository {

	@Autowired
	public PlayerRepository(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}
	
	public Player getPlayer(final int id) {
		HibernateTemplate ht = getHibernateTemplate();
		Player player = (Player)ht.load(Player.class, id);
		ht.initialize(player);
		return player;
	}
	
	public List<Player> getPlayers() {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.find("FROM Player p WHERE p.active = true ORDER BY p.name ASC");
	}
	
	public List<Player> getAllPlayers() {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.find("FROM Player p ORDER BY p.name ASC");
	}

	public List<Player> getManagement() {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.find("FROM Player p WHERE p.management = true ORDER BY p.name ASC");
	}
	
	public void savePlayer(Player player) {
		HibernateTemplate ht = getHibernateTemplate();
		ht.saveOrUpdate(player);		
	}

	public Player getPlayer(String playerName) {
		HibernateTemplate ht = getHibernateTemplate();
						
		ht.setMaxResults(1);
		List results = ht.find("FROM Player AS p INNER JOIN FETCH p.role AS r WHERE p.name = ?", playerName);
		if (results.size() > 0) {
			return (Player)results.get(0);
		} else {
			return null;
		}
	}
}
