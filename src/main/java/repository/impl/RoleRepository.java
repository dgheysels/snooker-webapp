package repository.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import repository.IRoleRepository;
import domain.Role;

@Repository("roleRepository")
public class RoleRepository extends HibernateDaoSupport implements IRoleRepository {

	@Autowired
	public RoleRepository(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}
	
	public List<Role> getRoles() {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.find("FROM Role r");
	}
	
	public Role getRole(int id) {
		HibernateTemplate ht = getHibernateTemplate();
		return (Role)ht.load(Role.class, id);
	}
	
	public Role getRole(final String name) {
		HibernateTemplate ht = getHibernateTemplate();
		return (Role)ht.execute(new HibernateCallback() {

			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				Query query = session.createQuery(						
						"FROM Role AS r WHERE r.name = :name");
				query.setParameter("name", name);
				return query.uniqueResult();
			}			
		});
	}
}
