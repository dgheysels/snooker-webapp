package repository.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import repository.IMeetingRepository;
import domain.Meeting;

@Repository("meetingRepository")
public class MeetingRepository extends HibernateDaoSupport implements IMeetingRepository{

	@Autowired
	public MeetingRepository(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}
	
	public List<Meeting> getAllMeetings() {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.loadAll(Meeting.class);
	}
	
	public Meeting getMeeting(int meetingId) {
		HibernateTemplate ht = getHibernateTemplate();
		return (Meeting)ht.load(Meeting.class, new Integer(meetingId));
	}
	
	public void saveMeeting(Meeting meeting) {
		HibernateTemplate ht = getHibernateTemplate();
		ht.saveOrUpdate(meeting);
	}
}
