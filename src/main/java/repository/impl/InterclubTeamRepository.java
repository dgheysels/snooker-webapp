package repository.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import repository.IInterclubTeamRepository;
import domain.InterclubTeam;

@Repository("interclubTeamRepository")
public class InterclubTeamRepository extends HibernateDaoSupport implements IInterclubTeamRepository {

	@Autowired
	public InterclubTeamRepository(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}

	
	public InterclubTeam getInterclubTeam(int teamId) {
		HibernateTemplate ht = getHibernateTemplate();
		return (InterclubTeam)ht.load(InterclubTeam.class, teamId);
	}

	
	public List<InterclubTeam> getInterclubTeams() {
		HibernateTemplate ht = getHibernateTemplate();
		return (List<InterclubTeam>)ht.loadAll(InterclubTeam.class);
	}

	
	public void saveInterclubTeam(InterclubTeam team) {
		HibernateTemplate ht = getHibernateTemplate();
		ht.saveOrUpdate(team);
	}

	
	public void deleteInterclubTeam(InterclubTeam team) {
		HibernateTemplate ht = getHibernateTemplate();
		ht.delete(team);
		
	}
	
	
}
