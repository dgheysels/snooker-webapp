package repository.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import repository.IInterclubDivisionRepository;
import domain.InterclubDivision;

@Repository("interclubDivisionRepository")
public class InterclubDivisionRepository extends HibernateDaoSupport implements IInterclubDivisionRepository {

	@Autowired
	public InterclubDivisionRepository(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}
	
	public List<InterclubDivision> getAllInterclubDivisions() {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.find("FROM InterclubDivision d ORDER BY d.sortorder ASC");		
	}
	
	public List<InterclubDivision> getInterclubDivisions() {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.find(
				"SELECT DISTINCT d " +
				"FROM InterclubDivision d " +
				"	JOIN d.teams t " +
				"ORDER BY d.sortorder ASC");
	}
	
	public InterclubDivision getInterclubDivision(int id) {
		HibernateTemplate ht = getHibernateTemplate();
		return (InterclubDivision)ht.load(InterclubDivision.class, id);
	}
}
