package repository.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import repository.INoticeRepository;
import domain.Notice;

@Repository("noticeRepository")
public class NoticeRepository extends HibernateDaoSupport implements INoticeRepository{

	@Autowired
	public NoticeRepository(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}

	public List<Notice> getAllNotices() {
		HibernateTemplate ht = getHibernateTemplate();
		return (List<Notice>)ht.loadAll(Notice.class);
	}

	public List<Notice> getAllNotices(Date from) {
		HibernateTemplate ht = getHibernateTemplate();
		return ht.find("FROM domain.Notice n WHERE n.date >= ?", from);				
	}
	
	public List<Notice> getLastNotices(int number) {
		HibernateTemplate ht = getHibernateTemplate();
		ht.setMaxResults(number);
		return ht.find("FROM domain.Notice n ORDER BY n.date DESC");
		
	}
	
	public Notice getNotice(int id) {
		HibernateTemplate ht = getHibernateTemplate();
		return (Notice)ht.load(Notice.class, id);
	}
	
	public void saveNotice(Notice notice) {
		HibernateTemplate ht = getHibernateTemplate();
		ht.saveOrUpdate(notice);
		
	}	
}
