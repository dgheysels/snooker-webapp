package repository.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import repository.ISingleMatchPeriodRepository;
import domain.SingleMatchPeriod;

@Repository("singleMatchPeriodRepository")
public class SingleMatchPeriodRepository extends HibernateDaoSupport implements ISingleMatchPeriodRepository {

	@Autowired
	public SingleMatchPeriodRepository(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}
	
	public SingleMatchPeriod getPeriod(int periodId) {
		HibernateTemplate ht = getHibernateTemplate();
		return (SingleMatchPeriod)ht.load(SingleMatchPeriod.class, periodId);
	}
	
	public void savePeriod(SingleMatchPeriod period) {
		HibernateTemplate ht = getHibernateTemplate();
		ht.saveOrUpdate(period);
	}
}
