package repository.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import repository.ISubTournamentRepository;
import domain.SubTournament;

@Repository("subTournamentRepository")
public class SubTournamentRepository extends HibernateDaoSupport implements ISubTournamentRepository {
	
	@Autowired
	public SubTournamentRepository(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}
	public SubTournament getSubTournament(int subTournamentId) {
		HibernateTemplate ht = getHibernateTemplate();
		return (SubTournament)ht.load(SubTournament.class, new Integer(subTournamentId));				
	}
		
	public void saveSubTournament(SubTournament subTournament) {
		HibernateTemplate ht = getHibernateTemplate();
		ht.saveOrUpdate(subTournament);
	}


}
