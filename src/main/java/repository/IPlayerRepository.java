package repository;

import java.util.List;

import domain.Player;

public interface IPlayerRepository {

	public Player getPlayer(int playerId);
	public Player getPlayer(String playerName);
	public List<Player> getPlayers();
	public List<Player> getAllPlayers();
	
	public List<Player> getManagement();
	
	public void savePlayer(Player player);
		
}
