package repository;

import java.util.List;

import domain.InterclubTeam;

public interface IInterclubTeamRepository {

	public List<InterclubTeam> getInterclubTeams();
	public InterclubTeam getInterclubTeam(int teamId);
	
	public void saveInterclubTeam(InterclubTeam team);
	public void deleteInterclubTeam(InterclubTeam team);
}
