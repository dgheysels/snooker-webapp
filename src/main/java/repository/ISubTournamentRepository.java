package repository;

import domain.SubTournament;

public interface ISubTournamentRepository {

	public SubTournament getSubTournament(int subTournamentId);
	public void saveSubTournament(SubTournament subTournament);
	
}
