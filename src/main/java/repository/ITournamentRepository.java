package repository;

import java.util.List;

import domain.Player;
import domain.Tournament;
import domain.TournamentPlayer;
import domain.TournamentType;

public interface ITournamentRepository {

	public List<Tournament> getAllTournaments();
	public Tournament getTournament(int tournamentId);
	public Tournament getActiveTournament();
	
	public void saveTournament(Tournament tournament);
	
	public List<TournamentPlayer> getSubscriptions(Tournament tournament);
	public TournamentPlayer getSubscription(Tournament tournament, Player player);
	public void updateSubscription(TournamentPlayer subscription);
	//public void saveSubTournament(SubTournament subTournament);
	//public void saveSingleMatchPeriod(SingleMatchPeriod period);	
	
	//public void saveMatch(SingleMatch match);
	
	public List<TournamentType> getAllTournamentTypes();
}
