package repository;

import domain.SingleMatchPeriod;

public interface ISingleMatchPeriodRepository {

	public SingleMatchPeriod getPeriod(int periodId);
	public void savePeriod(SingleMatchPeriod period);
}
