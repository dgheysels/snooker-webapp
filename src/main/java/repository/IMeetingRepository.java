package repository;

import java.util.List;

import domain.Meeting;

public interface IMeetingRepository {

	public List<Meeting> getAllMeetings();
	public Meeting getMeeting(int meetingId);
	public void saveMeeting(Meeting meeting);
}
