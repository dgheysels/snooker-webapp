package view;

import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import domain.TournamentEvent;
import domain.TournamentRanking;
import domain.TournamentRankingEntry;
import domain.TournamentType;

public class TournamentRankingPDF extends AbstractPdfView {

	private static final float WIDTH_POS_COLUMN = 0.030f;
	private static final float WIDTH_PLAYER_COLUMN = 0.20f;
	private static final float WIDTH_GRADE_COLUMN = 0.025f;
	private static final float WIDTH_EVENT_COLUMN = 0.50f;
	private static final float WIDTH_MW_COLUMN = 0.05f;
	private static final float WIDTH_HB_COLUMN = 0.05f;	
	private static final float WIDTH_TOTAL_COLUMN = 0.05f;		 
	
	private static final String FONTNAME = "Helvetica";
	private static final int FONTSIZE = 10;
	
	@Override
	protected void buildPdfDocument(Map model, Document document,
			PdfWriter writer, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		/* data */
		TournamentRanking ranking = (TournamentRanking)model.get("ranking");
		int numberOfTournamentTypes = ranking.getTournament().getDifferentTypes().size();
		
		Set<TournamentEvent> events = ranking.getTournament().getAllEvents();
		List<TournamentRankingEntry> entries = ranking.getEntries();
		
		// build contents
		// (left corner)  | p1 | p2 | ... | pn |    |    | 
		// (left corner)  | e1 | e2 | ... | en | GM | MB | HTB | ETB | Tot
		// --------------------------------------------------
		// # | NAME | A/B | .....
		
		/* general document properties */ 
		document.setMargins(1.5f, 1.5f, 1.5f, 1.5f);
		document.setPageSize(PageSize.A4);
		
		/* determine width of columns */
		int numberOfColumns = 3 + events.size() + 2 + numberOfTournamentTypes;
		int currentColumn = 0;
		float[] columnWidth = new float[numberOfColumns];
		
		columnWidth[currentColumn++] = WIDTH_POS_COLUMN;
		columnWidth[currentColumn++] = WIDTH_PLAYER_COLUMN;
		columnWidth[currentColumn++] = WIDTH_GRADE_COLUMN;
		for (int i=0; i<events.size(); i++) {
			columnWidth[currentColumn++] = WIDTH_EVENT_COLUMN / events.size();
		}
		columnWidth[currentColumn++] = WIDTH_MW_COLUMN;
		
		for (int i=0; i<numberOfTournamentTypes; i++) {
			columnWidth[currentColumn++] = WIDTH_HB_COLUMN;
		}
		
		columnWidth[currentColumn++] = WIDTH_TOTAL_COLUMN;
		
				
		Font font = FontFactory.getFont(FONTNAME, FONTSIZE);
		
		/* build table */
		PdfPTable table = new PdfPTable(columnWidth);
		table.setWidthPercentage(100.0f);
		
		PdfPCell leftCornerCell1 = new PdfPCell(new Paragraph(""));
		leftCornerCell1.setColspan(3);
		leftCornerCell1.setBorder(PdfPCell.NO_BORDER);
		PdfPCell leftCornerCell2 = new PdfPCell(new Paragraph(""));
		leftCornerCell2.setColspan(3);		
		leftCornerCell2.setBorder(PdfPCell.NO_BORDER);
		PdfPCell rightCornerCell1 = new PdfPCell(new Paragraph(""));
		rightCornerCell1.setColspan(5);		
		rightCornerCell1.setBorder(PdfPCell.NO_BORDER);
		
		// headers 1
		table.addCell(leftCornerCell1);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
		for (TournamentEvent event : events) {
							
			PdfPCell eventHeaderCell = new PdfPCell(new Paragraph(sdf.format(event.getFromDate()) + " - " + sdf.format(event.getToDate()), font));			
			eventHeaderCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			eventHeaderCell.setRotation(90);
			
			table.addCell(eventHeaderCell);	
		}

		table.addCell(rightCornerCell1);
		table.completeRow();
				
		// headers 2
		table.addCell(leftCornerCell2);
		for (TournamentEvent event : events) {						
			PdfPCell eventHeaderCell = new PdfPCell(new Paragraph(event.getName(), font));
			table.addCell(eventHeaderCell);	
		}		
		
		PdfPCell MWHeaderCell = new PdfPCell(new Paragraph("MW", font));
		table.addCell(MWHeaderCell);
		
		for (TournamentType type : ranking.getTournament().getDifferentTypes()) {
			PdfPCell HBHeaderCell = new PdfPCell(new Paragraph(type.getBreakname(), font));
			table.addCell(HBHeaderCell);
		}
				
		
		PdfPCell TotalHeaderCell = new PdfPCell(new Paragraph("Tot", font));
		table.addCell(TotalHeaderCell);	
		
		table.completeRow();
		
		// entries
		Hashtable<TournamentType, Integer> highestBreaksOverall = ranking.getHighestBreaks();
		int i = 1;
		
		for (TournamentRankingEntry entry : entries) {
			
			PdfPCell positionCell = new PdfPCell(new Phrase(Integer.toString(i), font));							
			table.addCell(positionCell);
						
			PdfPCell playerCell = new PdfPCell(new Phrase(entry.getPlayer().getName(), font));			
			table.addCell(playerCell);
			
			PdfPCell gradeCell = new PdfPCell(new Phrase(entry.getPlayer().getGrade(ranking.getTournament()), font));
			table.addCell(gradeCell);
			
			// match-results
			Hashtable<TournamentEvent, Integer> points = entry.getRankingPoints();
			for (TournamentEvent event : events) {
				Integer temp = points.get(event);
				String v = StringUtils.EMPTY;
				if (temp != null) {
					if (temp == -1) {
						v = "FF";
					} else {
						v = Integer.toString(temp);
					}
				}
				//String v = points.get(event) == null ? "" : Integer.toString(points.get(event));
				PdfPCell pointsCell = new PdfPCell(new Phrase(v, font));
				table.addCell(pointsCell);	
			}
			
			// MW
			PdfPCell MWCell = new PdfPCell(new Phrase(Integer.toString(entry.getSingleMatchWins()), font));
			table.addCell(MWCell);						
				
			// HB
			Hashtable<TournamentType, Integer> highestBreaksEntry = entry.getHighestBreaks();
			PdfPCell HBCell;
			
			for (TournamentType t : ranking.getTournament().getDifferentTypes()) {
				int b = highestBreaksEntry.get(t);			
				String value = StringUtils.EMPTY;
				Font f = new Font(font);
				
				// break of tournamenttype is highestbreak overall -> bold
				if (b != 0) {
					value = Integer.toString(b);
					
					if (highestBreaksOverall.get(t) == b) {											
						f.setStyle(Font.BOLD);						
					}
				}
				
				HBCell = new PdfPCell(new Paragraph(value, f));
				
				table.addCell(HBCell);
			}
//			
//			if (b == hb) {
//				Font bold = new Font(font);
//				bold.setStyle(Font.BOLD);
//				HBCell = new PdfPCell(new Paragraph(b == 0 ? "" : Integer.toString(entry.getHighestBreak()), bold));
//			} else {
//				HBCell = new PdfPCell(new Paragraph(b == 0 ? "" : Integer.toString(entry.getHighestBreak()), font));
//			}
						
			PdfPCell TotalCell = new PdfPCell(new Phrase(Integer.toString(entry.getTotal()), font));
			table.addCell(TotalCell);
			
			table.completeRow();
			
			i++;
		}
		
		
		document.add(table);
		

	}

}
