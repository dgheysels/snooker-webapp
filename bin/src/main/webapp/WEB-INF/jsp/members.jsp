<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.List"%>
<%@page import="domain.Player"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1><c:out value="${date}" /></h1>

<table>
<%
	List<Player> players = (List<Player>)request.getAttribute("players");

	for (Player p : players) {
%>
		<tr>
			<td>
<%= p.getId() %>
			</td>
			<td>
<%= p.getName() %>
			</td>
		</tr>
<%
	}
%>
</table>
</body>
</html>