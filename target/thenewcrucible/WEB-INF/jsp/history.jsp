<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


<div class="panel">
<p>						
	Het ontstaan van The Crucible begint op 3 december van het jaar 1994.
</p>
<p>
	Het was Alain Corbusier, die al de snookerzaal 'Chamberlains' op de markt in
Deinze uitbaatte, die ge�nteresseerd was in de oude fabrieksgebouwen aan de
Leiedam te Deinze. "Een perfect pand om een snookerzaal in onder te brengen"
moet hij toen gedacht hebben.
</p>
<p>
	Al enkele maanden na de opening van 'The Crucible' werd de zaak overgenomen
door Dirk Bijn, die meteen de naam veranderde in 'The New Grucible'. De
overname bracht nieuw leven in de zaak; er kwamen veel nieuwe leden bij en de
snookersport in Deinze nam een nieuwe start.
</p>
<p>
	Op 19 februari 2002 werd de zaak opnieuw overgenomen en werd Katrien Van
Praet de nieuwe uitbaatster. De naam van het caf� veranderde in Funkafee maar
de naam van de snookerciub bleef behouden.
</p>
<p>
	Intussen kreeg 'The New Crucible' een nieuw bestuursteam onder leiding van
Kenny Dekeyser. Met zijn team slaagde hij er in om de club op te waarderen en
meer bekendheid te geven in Deinze en omstreken, zodat meer mensen zich
gingen interesseren voor de snookersport.  Snooker in Deinze floreerde.
</p>
<p>
	Nadat de zaak nog maar eens werd overgenomen en volledig vernieuwd
door Bruno Van Driessche, is sinds de start van het seizoen 2006-2007 Rachid Yahiani de nieuwe uitbater
van dienst.
</p>
<p>
	.... groei sinds 2007-2008: 
	stijging nivo interclub-ploegen, 
	Davy's cup, 
	nieuwe sterke ERE ploeg ...
</p>

</div>
			
