<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="json" uri="/WEB-INF/tags/json.tld" %>

<link href="css/knockoutTable.css" rel="stylesheet" type="text/css" />

<script src="js/json.js"></script>
<script src="js/knockoutTable.js"></script>
<script src="js/prototype.js"></script>

<!-- show poules -->
<div class="panel">
			
	<table width="100%">		
		<tr>
			<c:forEach items="${subTournament.poules}" var="poule">
				<td valign="top" align="center">																
					<div>
						<h1><c:out value="${poule.name}" /></h1>						
						<table cellspacing="0" width="100%" style="border: 1px solid black">
							<tr class="tableheader">								
								<td align="left" style="border-bottom: 1px solid black">speler</th>
								<td align="left" style="border-bottom: 1px solid black">ptn</th>
								<td align="left" style="border-bottom: 1px solid black">brk</th>								
							</tr>
							<c:forEach items="${poule.pouleranking}" var="entry">
								<tr <c:if test="${entry.winner}">class=bold</c:if>>	
									<td align="left"><c:out value="${entry.player}" /></td>
									<td align="left"><c:out value="${entry.framesWon}" /></td>
									<td align="left">
										<c:if test="${entry.brk.value != 0}">
											<c:out value="${entry.brk}" />
										</c:if>						
									</td>
								</tr>								
							</c:forEach>
						</table>
					</div>
				</td>		
			</c:forEach>
		</tr>
	</table>		
</div>

<hr />
	
<!-- show knockout-round -->
<div class="panel">
	<c:if test="${subTournament.knockoutRound != null}">
	
				
		<!-- knockout-table is generated in here using JavaScript -->
		<div id="knockoutTable" style="width: 100%; overflow: visible">							
	
		</div>
			
		<script>								
			generate(<c:out value="${subTournament.knockoutRound.height}"/>, 
     					'1',
					  <json:object>							 
						<json:array name="knockoutMatches" var="knockoutMatch" items="${subTournament.knockoutRound.matches}"> 						
							<json:object>
								<json:property name="position" value="${knockoutMatch.position}"/>
								<json:object name="knockoutMatch">
									<json:property name="playerA" value="${knockoutMatch.playerA.name}"/>
									<json:property name="playerB" value="${knockoutMatch.playerB.name}"/>
									<json:property name="scoreA" value="${knockoutMatch.scoreA}"/>
									<json:property name="scoreB" value="${knockoutMatch.scoreB}"/>
								</json:object>
							</json:object>
						</json:array>				
					  </json:object>, 
					  $("knockoutTable"));
		</script>
	
	</c:if>
</div>				