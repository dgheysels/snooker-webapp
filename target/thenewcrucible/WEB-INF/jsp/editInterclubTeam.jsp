<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="css/stylesheet-new.css" rel="stylesheet" type="text/css" />
</head>
<body>


	<div class="panel" style="width: 100%">
		<form:form method="POST" commandName="team">
			<form:hidden path="id"/>
			<table width="100%">
				<tr>
					<td>name</td>
					<td><form:input path="name" size="40"/></td>
				</tr>
				<tr>
					<td>division</td>
					<td>
						<form:select path="interclubDivision">
							<form:options items="${divisions}" itemLabel="name" />
						</form:select>
					</td>
				</tr>
				<tr>
					<td>captain</td>
					<td>
						<select name="player1">
							<c:forEach items="${players}" var="player">
								<option value="${player.id}" <c:if test="${team.players[0].id == player.id}">selected="selected"</c:if>>
									<c:out value="${player.name}" />
								</option>
							</c:forEach>	
						</select>
					</td>														
				</tr>
				<tr>
					<td>player 2</td>
					<td>
						<select name="player2">
							<c:forEach items="${players}" var="player">
								<option value="${player.id}" <c:if test="${team.players[1].id == player.id}">selected="selected"</c:if>>
									<c:out value="${player.name}" />
								</option>
							</c:forEach>	
						</select>
					</td>														
				</tr>
				<tr>
					<td>player 3</td>
					<td>
						<select name="player3">
							<option value="-1"></option>
							<c:forEach items="${players}" var="player">
								<option value="${player.id}" <c:if test="${team.players[2].id == player.id}">selected="selected"</c:if>>
									<c:out value="${player.name}" />
								</option>
							</c:forEach>	
						</select>
					</td>														
				</tr>								
			</table>
			<input type="submit" value="save" name="saveTeam" />
		</form:form>
	</div> 
	
		

</body>
</html>