<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>



<div class="panel">
	<div style="float: left;">
		<br />
		<a href="http://vsf-oostvlaanderen.be/docs/itckalender.xls">Interclub kalender</a>
		<br /><br />
		<a href="http://vsf-oostvlaanderen.be/rangschikking.asp" target="_blank">Uitslagen & Rangschikking</a>
		<br /><br />
		<a href="http://vsf-oostvlaanderen.be/individueel.asp" target="_blank">Individuele rangschikking</a>
	</div>
	<div style="float: right; margin-right: 50px; width: 128px; height: 128px">
		<img src="images/hemd.gif" width="100%" height="100%" />
	</div>
</div>

<div class="cleaner" />

<hr />

<div class="panel">
	
	<c:forEach var="division" items="${interclubOverview}">
		<c:url var="newTeamURL" value="newInterclubTeam.htm">
			<c:param name="divisionId" value="${division.id}" />
		</c:url>
		<table width="100%" cellspacing="0">
			<tr class="title">				
				<td align="left" colspan="4">
					<b><c:out value="${division.name}"/></b>									
				</td>
										
				<security:authorize ifAnyGranted="ROLE_ADMIN, ROLE_SUPERUSER">
					<td align="right">
						<a href=<c:out value="${newTeamURL}"/>><img src="images/icons/add.png" width="12px" height="12px" /></a>
					</td>
				</security:authorize>
				
			</tr>
			
			<c:forEach var="team" items="${division.teams}">								
				<c:url var="editTeamURL" value="editInterclubTeam.htm">
					<c:param name="teamId" value="${team.id}" />
				</c:url>
				<c:url var="deleteTeamURL" value="deleteInterclubTeam.htm">
					<c:param name="teamId" value="${team.id}" />
				</c:url>
				<tr>
					<td width="20%">
						<b><c:out value="${team.name}"/></b>
						<security:authorize ifAnyGranted="ROLE_ADMIN, ROLE_SUPERUSER">
							<a href=<c:out value="${editTeamURL}"/>><img src="images/icons/edit.png" width="12px" height="12px" /></a>
							<a href=<c:out value="${deleteTeamURL}"/>><img src="images/icons/remove.png" width="12px" height="12px" /></a>			
						</security:authorize>							
					</td>					
					
					<c:forEach var="player" items="${team.players}">														
						<c:url value="getImage.htm" var="imageURL">
							<c:param name="playerId" value="${player.id}" />
						</c:url>
						<c:url var="viewMemberURL" value="viewMember.htm">
							<c:param name="playerId" value="${player.id}" />
						</c:url>
						<td width="22%" align="center">
							<img src=<c:out value="${imageURL}"/> height="64" />
							<br />
							<a href=<c:out value="${viewMemberURL}"/>><c:out value="${player.name}" /></a>								
						</td>
					</c:forEach>
				</tr>
				<tr>
					<td />
					
				</tr>
			</c:forEach>
		</table>
	</c:forEach>
	
</div>
