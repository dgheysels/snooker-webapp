<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="panel">

	<c:if test="${subTournament != null}">
	
		
		<p class="tableheader">
			<b>Overzicht poules</b>
		</p>
		
		<table width="100%">
			<tr>
				<td align="center">
					<c:forEach items="${subTournament.poules}" var="poule">
						<div class="panel" style="width: 20%; margin-right: 15px; float: left;">
							<table cellspacing="0" cellpadding="2" width="100%" style="border: 1px solid black">
								<thead bgcolor="lightgrey">
									<th colspan="3">
										<h1><c:out value="${poule.name}" /></h1>
									</th>
								</thead>
								<tr>
									<td><b>speler</b></td>
									<td><b>ptn</b></td>
									<td><b>break</b></td>
								</tr>
								<c:forEach items="${poule.pouleranking}" var="entry">
									<tr class=<c:if test="${entry.winner}">bold</c:if>>
										<td><c:out value="${entry.player}" /></td>
										<td><c:out value="${entry.framesWon}" /></td>
										<td><c:out value="${entry.brk}" /></td>
									</tr>
								</c:forEach>
							</table>
						</div>
					</c:forEach>
				</td>
			</tr>
		</table>
	
		<div class="cleaner" />
	
		<form method="POST" action=<c:out value="${submitPouleURL}"/>>
						
			<c:url value="/enterResults.htm" var="submitPouleURL">
				<c:param name="tournamentId" value="${tournament.id}" />
				<c:param name="subTournamentId" value="${subTournament.id}" />
			</c:url>
		
			<p class="tableheader">	
				<b>Resultaten ingeven</b>
			</p>
				
			<table>	
				<tr>
					<td><b>resultaat</b></td>
					<td><select name="playerA">
						<c:forEach items="${tournament.subscribers}" var="subscription">
							<option value="${subscription.player.id}"><c:out value="${subscription.player.name}" /></option>
						</c:forEach>
					</select></td>
					<td><input type="text" name="scoreA" size="2" /></td>
					<td>-</td>
					<td><input type="text" name="scoreB" size="2" /></td>
					<td><select name="playerB">
						<c:forEach items="${tournament.subscribers}" var="subscription">
							<option value="${subscription.player.id}"><c:out value="${subscription.player.name}" /></option>
						</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td><b>breaks</b></td>
					<td colspan="2" align="right"><input type="text" name="breaksPlayerA" size="10"/></td>
					<td />
					<td colspan="2" align="left"><input type="text" name="breaksPlayerB" size="10" /></td>
				</tr>
				<tr>
					<td />
					<td />
					<td />
					<td />
					<td />
					<td align="right"><input type="submit" value="Toevoegen" name="addPouleMatch" /></td>
				</tr>
			</table>			
			<br />
			<b>Huidige poule rangschikking</b>
			<br /><br />
			<table>
				<tr>
					<td><b>poulenaam</b></td>
					<td colspan="3"><input type="text" name="poulename" size="20" /></td>
				</tr>
				<tr>
					<td><b>speler</b></td>
					<td><b>ptn</b></td>
					<td><b>break</b></td>
					<td><b>volgende ronde</b></td>
				</tr>
				<c:forEach items="${poule.pouleranking}" var="entry">
					<tr>
						<td><c:out value="${entry.player}" /></td>
						<td><c:out value="${entry.framesWon}" /></td>
						<td><c:out value="${entry.brk}" /></td>
						<td><input type="checkbox" name="winner"
							value="${entry.player.id}" />
					</tr>
				</c:forEach>
			</table>	
					
			<hr />
			<input type="submit" value="Opslaan" name="savePoule" />			
			<input type="submit" value="Genereer knockout-tabel" name="generateKnockoutTable" />
		</form>

	</c:if>
</div>