<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<div id="navigation">
	<ul class="list">
		<li><a href="<c:url value="/main.htm"/>">Home</a></li>
		<li>The New Crucible
			<ul><li><a href="<c:url value="/tnc.htm" />">info</a></li></ul>
			<ul><li><a href="<c:url value="/management.htm" />">bestuur</a></li></ul>
			<ul><li><a href="<c:url value="/listMembers.htm"/>">leden</a></li></ul>
		</li>
		

		<li>Club kampioenschap
			<ul>
				
				<c:url value="openTournament.htm" var="tournamentURL">
					<c:param name="id" value="${activeTournament.id}" />
				</c:url>
				
<!-- 		 	<c:choose>
					<c:when test="${activeTournament != null}">
						<li>
							<a href="<c:out value="${tournamentURL}"/>"><c:out value="${activeTournament.season}" /></a>
						</li>
					</c:when>
					<c:otherwise>
						<security:authorize ifAnyGranted="ROLE_ADMIN, ROLE_SUPERUSER">
							<li>
								<a href="<c:url value="/newTournament.htm"/>">nieuw seizoen...</a>
							</li>
						</security:authorize>				
					</c:otherwise>
				</c:choose> -->
				
				
<!--				<li>-->
<!--					<a href=<c:url value="/openTournamentArchive.htm"/>>archief raadplegen</a>-->
<!--				</li>-->
						<li>
							<a href="<c:out value="${tournamentURL}"/>"><c:out value="${activeTournament.season}" /></a>
						</li>
						<li>
							<a href="<c:url value="/clubChampionship.htm"/>">2010-2011</a>
						</li>
						<li>
							<a href="<c:url value="/clubChampionship20112012.htm"/>">2011-2012</a>
						</li>

			</ul>
		</li>	
		
		<li><a href="<c:url value="/viewInterclub.htm"/>">Interclub</a></li>
		
		<li>Davy's Cup
			<ul>
				<c:url value="davyscupInformation.htm" var="davyscupInformationURL"></c:url>
				<li><a href="<c:out value="${davyscupInformationURL}"/>">concept en algemene info</a></li>
			</ul>
			<ul>
				<c:url value="davyscupPreviousSeasons.htm" var="davyscupPreviousSeasonsURL"></c:url>
				<li><a href="<c:out value="${davyscupPreviousSeasonsURL}"/>">vorige edities</a></li>
			</ul>
			<ul>
				<c:url value="davyscupCurrentSeason.htm" var="davyscupCurrentSeasonURL">
				</c:url>
				<li><a href="<c:out value="${davyscupCurrentURL}"/>">huidig seizoen</a></li>
			</ul>
<!--			<ul>-->
<!--				<c:url value="davyscupPhotos.htm" var="davyscupPhotoURL">-->
<!--				</c:url>-->
<!--				<li><a href="<c:out value="${davyscupPhotoURL}"/>">fotoalbum</a></li>-->
<!--			</ul>-->
		</li>
		<li><a href=<c:url value="/sponsors.htm" />>Sponsors</a></li>
		<li>Links &amp; Multimedia
			<ul>
				<c:url value="links.htm" var="linksURL"></c:url>
				<li><a href="<c:out value="${linksURL}"/>">links</a></li>				
			</ul>
			<ul>
				<c:url value="multimedia.htm" var="multimediaURL"></c:url>
				<li><a href="<c:out value="${multimediaURL}"/>">multimedia</a></li>
			</ul>
			
							
	</ul>
</div>
		
		