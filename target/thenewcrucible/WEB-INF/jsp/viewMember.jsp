<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


<div class="panel">
	<c:url value="getImage.htm" var="imageURL">
		<c:param name="playerId" value="${player.id}" />
	</c:url>
	<table width="100%">
		<tr>
			<td width="25%"><img src=<c:out value="${imageURL}"/> height="128" /></td>
			<td align="left"><h1><c:out value="${player.name}" /></h1></td>					
		</tr>
		<tr></tr>
		<tr class="title">
			<td colspan="2"><p>Contactgegevens</p></td>			
		</tr>
		
		<security:authorize ifAnyGranted="ROLE_PLAYER, ROLE_ADMIN, ROLE_SUPERUSER">
			<tr>
				<td valign="top">Adres</td>
				<td><c:out value="${player.address.street}" /> <c:out value="${player.address.number}"/><br>
				    <c:out value="${player.address.postcode}" /> <c:out value="${player.address.city}" />
				</td>
			</tr>
			<tr>
				<td>Telnr</td>
				<td><c:out value="${player.telnr}" /></td>
			</tr>
			<tr>
				<td>E-mail</td>
				<td><c:out value="${player.email}" /></td>
			</tr>
		</security:authorize>
		<security:authorize ifNotGranted="ROLE_PLAYER, ROLE_ADMIN, ROLE_SUPERUSER">
			<c:url value="sendMessage.htm" var="sendMessageURL">
				<c:param name="playerId" value="${player.id}"></c:param>
			</c:url>
			<tr>
				<td colspan="2">
					Login om de contactgegevens te zien
				</td>
			</tr>
<!--			<tr>-->
<!--				<td colspan="2">-->
<!--					<a href=<c:out value="${sendMessageURL}"/>>Verstuur een E-mail <img src="images/icons/email.png" /></a>-->
<!--				</td>				-->
<!--			</tr>-->
		</security:authorize>
		
		<tr class="title">
			<td colspan="2"><p>Persoonlijke informatie</p></td>
		</tr>
		<tr>
			<td>Verslaafd aan snooker sinds</td>
			<td><c:out value="${player.yearsPlayingSnooker}" /></td>
		</tr>
		<tr>
			<td>Merk en type van je keu</td>
			<td><c:out value="${player.cueType}" /></td>
		</tr>
		<tr>
			<td>Favorite internationale speler</td>
			<td><c:out value="${player.favoriteInternationalPlayer}" /></td>
		</tr>
		<tr>
			<td>Favorite belgische speler</td>
			<td><c:out value="${player.favoriteBelgianPlayer}" /></td>
		</tr>
		<tr>
			<td>Hoogste break tijdens training</td>
			<td><c:out value="${player.hbTraining}" /><td>
		</tr>
		<tr>
			<td>Hoogste break tijdens wedstrijd</td>
			<td><c:out value="${player.hbMatch}" /></td>
		</tr>
		<tr>
			<td valign="top">Opmerkelijke gebeurtenis in jouw snooker carri�re</td>
			<td><c:out value="${player.story}" escapeXml="false" /></td>
		</tr>
				
<!--			<tr>-->
<!--				<td>snookerclub</td>-->
<!--				<td><c:out value="${player.snookerclub.name}" /></td>-->
<!--			</tr>-->
	</table>
<!--	<table width="100%">-->
<!--		<tr class="tableheader">-->
<!--			<td colspan="2"><b>Interclub</b></td>			-->
<!--		</tr>-->
<!--		<tr>-->
<!--			<td><img src="images/icons/underconstruction.gif" width="128px" /></td>-->
<!--		</tr>-->
<!--	</table>-->
	
<!--	<table width="100%">	-->
<!--		<tr class="tableheader">-->
<!--			<td colspan="2"><b>Statistieken &amp; palmares</b></td>						-->
<!--		</tr>-->
<!--		<tr>-->
<!--			<td><img src="images/icons/underconstruction.gif" width="128px" /></td>-->
<!--		</tr>-->
<!--	</table>-->


<!--			<tr>-->
<!--				<td>interclub-team</td>-->
<!--				<td><c:out value="${player.interclubTeam.name}" /></td>-->
<!--			</tr>-->
<!--			<security:authorize ifAnyGranted="ROLE_SUPERUSER">-->
<!--				<tr>-->
<!--					<td>role</td>-->
<!--					<td><c:out value="${player.role.name}" /></td>-->
<!--				</tr>			-->
<!--			</security:authorize>-->

</div>
