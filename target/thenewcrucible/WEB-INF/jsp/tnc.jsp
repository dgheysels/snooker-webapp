<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


<div class="panel">
	
	<div style="width: 40%; float: left; font-size: 12pt">	
		<b>The New Crucible</b><br />
		Leiedam 4B<br>9800 Deinze<br />
		09/380 26 26<br />
		<br />
		Iedere dag open van 11u30 tot ...
	</div>
	
	<div style="width: 60%; float: right; position: relative">
		<iframe width="100%" height="200" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.be/maps?f=q&amp;source=s_q&amp;hl=nl&amp;geocode=&amp;q=the+new+crucible+deinze&amp;sll=50.983315,3.530205&amp;sspn=0.059726,0.01929&amp;gl=be&amp;ie=UTF8&amp;hq=the+new+crucible&amp;hnear=Deinze&amp;ll=50.983884,3.530216&amp;spn=0.005403,0.017166&amp;z=14&amp;output=embed"></iframe><br />
					<small><a href="http://maps.google.be/maps?hl=nl&amp;ie=UTF8&amp;q=the+new+crucible+deinze&amp;fb=1&amp;gl=be&amp;hq=The+New+Crucible&amp;hnear=The+New+Crucible&amp;cid=2893736794265836706&amp;li=lmd&amp;ll=50.983316,3.530195&amp;spn=0.001351,0.004292&amp;z=16&amp;iwloc=A&amp;source=embed" target="_blank">Grotere kaart weergeven</a></small>
	</div>
</div>

<div class="cleaner" />

<hr />

<!--
<div class="panel">	
	
	<div style="border: 1px solid red; float: right; width: 400px; height: 300px;">
		picture 1
	</div>
	
		
	<div style="border: 1px solid red; float: left; top: 40px; width: 250px; height: 300px;">
		picture 2
	</div>		
</div>
-->


