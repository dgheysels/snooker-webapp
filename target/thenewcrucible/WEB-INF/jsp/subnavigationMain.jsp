<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<security:authorize ifAnyGranted="ROLE_SUPERUSER">
	<ul>
		<li>
			<c:url var="newNoticeURL" value="newNotice.htm" />
			<c:choose>
				<c:when test="${param.menu == 0}">
					<h2><img src="images/icons/add.png" width="20px" height="20px" /> Nieuw</h2>
				</c:when>
				<c:otherwise>
					<h2><a href=<c:out value="${newNoticeURL}"/>><img src="images/icons/add.png" width="20px" height="20px" /> Nieuw</a></h2>
				</c:otherwise>
			</c:choose>	
		</li>
	</ul>
</security:authorize>

