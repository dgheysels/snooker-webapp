<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script src="js/nicEdit.js"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

<div class="panel">			
	<form:form method="POST" commandName="notice">
		<form:input path="title" />
		<form:textarea path="contents" rows="10" cssStyle="width: 100%"  />
		<input type="submit" value="save" name="saveNotice" />
	</form:form>
	
</div>		
	
