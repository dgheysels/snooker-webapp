<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<div class="panel">
	<table width="100%">		
		<tr class="title">
			<td colspan="2"><b>Handicaptornooi September</b></td>		
		</tr>
		<tr>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 1 tornooi 1.pdf" target="_blank">tornooi 1</a></b></p>
			</td>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 1 tornooi 2.pdf" target="_blank">tornooi 2</a></b></p>
			</td>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 1 tornooi 3.pdf" target="_blank">tornooi 3</a></b></p>
			</td>
		</tr>
		<tr class="title">
			<td colspan="2"><b>Handicaptornooi Oktober</b></td>		
		</tr>
		<tr>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 2 tornooi 1.pdf" target="_blank">tornooi 1</a></b></p>
			</td>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 2 tornooi 2.pdf" target="_blank">tornooi 2</a></b></p>
			</td>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 2 tornooi 3.pdf" target="_blank">tornooi 3</a></b></p>
			</td>
		</tr>
		<!--
		<tr class="title">
			<td colspan="2"><b>Handicaptornooi November</b></td>		
		</tr>
		<tr>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 3 tornooi 1.pdf" target="_blank">tornooi 1</a></b></p>
			</td>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 3 tornooi 2.pdf" target="_blank">tornooi 2</a></b></p>
			</td>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 3 tornooi 3.pdf" target="_blank">tornooi 3</a></b></p>
			</td>
		</tr>
		<tr class="title">
			<td colspan="2"><b>Handicaptornooi December</b></td>		
		</tr>
		<tr>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 4 tornooi 1.pdf" target="_blank">tornooi 1</a></b></p>
			</td>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 4 tornooi 2.pdf" target="_blank">tornooi 2</a></b></p>
			</td>
			<td width="40%">
				<p><b><a href="http://www.thenewcrucible.be/docs/ck/20102011/Clubcompetitie 2010-2011 maand 4 tornooi 3.pdf" target="_blank">tornooi 3</a></b></p>
			</td>
		</tr>
		-->
	</table>
</div>