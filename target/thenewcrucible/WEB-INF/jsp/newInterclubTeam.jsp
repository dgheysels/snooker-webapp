<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>



<div class="panel" style="width: 100%">
	<form:form method="POST" commandName="team">
		<table width="100%">
			<tr>
				<td>name</td>
				<td><form:input path="name" size="40"/></td>
			</tr>
			<tr>
				<td>division</td>
				<td>
					<form:select path="interclubDivision">
						<form:options items="${divisions}" itemLabel="name" />
					</form:select>
				</td>
			</tr>
			<tr>
				<td>captain</td>
				<td>
					<select name="player1">
						<c:forEach items="${players}" var="player">
							<option value="${player.id}"><c:out value="${player.name}" /></option>
						</c:forEach>	
					</select>
				</td>														
			</tr>
			<tr>
				<td>player 2</td>
				<td>
					<select name="player2">
						<c:forEach items="${players}" var="player">
							<option value="${player.id}"><c:out value="${player.name}" /></option>
						</c:forEach>	
					</select>
				</td>														
			</tr>
			<tr>
				<td>player 3</td>
				<td>
					<select name="player3">
						<option value="-1"></option>
						<c:forEach items="${players}" var="player">
							<option value="${player.id}"><c:out value="${player.name}" /></option>
						</c:forEach>	
					</select>
				</td>														
			</tr>								
		</table>
		<input type="submit" value="save" name="saveTeam" />
	</form:form>
</div> 