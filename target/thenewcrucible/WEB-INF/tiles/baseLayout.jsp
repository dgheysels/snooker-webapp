<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></meta>
		<meta name="description" lang="nl" content="Website van snookerclub The New Crucible in Deinze"></meta>
		<meta name="keywords" lang="nl" content="snooker crucible deinze vsf oost-vlaanderen interclub"></meta>
		<meta name="author" lang="nl" content="Dimitri Gheysels"></meta>
		<title>Snookerclub The New Crucible</title>
		<link href="css/stylesheet-new.css" rel="stylesheet" type="text/css"></link>
		<script language="javascript" src="js/bannerscript.js" type="text/javascript"></script>
	</head>
	
	<body>
		<!-- Start of StatCounter Code -->
		<script type="text/javascript">
			var sc_project=5201127; 
			var sc_invisible=1; 
			var sc_partition=46; 
			var sc_click_stat=1; 
			var sc_security="19c7d610"; 
		</script>
		<script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script>
		<noscript>
			<div class="statcounter">
				<a title="iweb counter" href="http://www.statcounter.com/iweb/" target="_blank">
					<img class="statcounter" src="http://c.statcounter.com/5201127/0/19c7d610/1/" alt="iweb counter" >
				</a>
			</div>
		</noscript>
		<!-- End of StatCounter Code -->
		<img src="<tiles:insertAttribute name="background-url" />" id="bg" />
		<div id="center">	
			<div id="headerLogin">
				<tiles:insertAttribute name="header" />	
			</div>
			<div id="header">				
				<img src="images/header.gif" id="bgheader" width="100%" height="100%" />
			</div>			
			<div class="sponsorbar">
				<tiles:insertAttribute name="sponsor_top" />
			</div>		
			<div id="titlebar">
				<h1>	
				 <tiles:insertAttribute name="title" />
				</h1>
			</div>		
			<div class="cleaner"></div>	
			<div id="content">						
				<div id="leftside">
					<tiles:insertAttribute name="navigation" />
					<tiles:insertAttribute name="calendar" />
				</div>			
				<div id="rightside">			
					<div id="menubar" class="panel">
						<tiles:insertAttribute name="subnavigation" />
					</div>				
					<tiles:insertAttribute name="body"   />					
				</div>			
			</div>
			<div class="cleaner"></div>		
			<div class="sponsorbar">
				<tiles:insertAttribute name="sponsor_bottom" />
			</div>		
		  	<div id="footer">
		    	<tiles:insertAttribute name="footer" />
		    </div>	    
		</div>
	</body>
</html>